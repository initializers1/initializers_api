package com.initializers.services.apiservices.constant;

public class Status {
	public final static String Active = "Active";
	public final static String InActive = "InActive";
	
	public final static String pipelineTriggered = "triggered";
	public final static String pipelineCreated = "created";
	public final static String PipelineSuccess = "success";
	public final static String pipelineFailure = "failed";
	public final static String pipelineRunning = "running";
}
