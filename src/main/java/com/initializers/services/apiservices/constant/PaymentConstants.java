package com.initializers.services.apiservices.constant;

public class PaymentConstants {
	
	//Payment Class Path
	public final static String ClassPath = "com.initializers.services.apiservices.payment.";
	
	//Initial Status 
	public final static char InitStatus = 'P';
	public final static char FinalStatus = 'S';
	
	//Payment Supported Types
	public final static char Card = 'C';
	public final static char UPI = 'U';
	public final static char COD = 'M';
	
	//Payment Standard Date Format
	public final static String STDateFormat = "dd-MM-yyyy HH:mm";
	
//	supported gateway
	public final static String Stripe = "Stripe";
	public final static String StripeClass = "StripeController"; //used while configuring webhook
	
}
