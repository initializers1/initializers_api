package com.initializers.services.apiservices.constant;

public enum Scope {
	CLIENT,
	ADMIN
}
