package com.initializers.services.apiservices.service.impl;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.exception.ActiveBuildExistException;
import com.initializers.services.apiservices.exception.UserNotFoundException;
import com.initializers.services.apiservices.model.AdminAppDetails;
import com.initializers.services.apiservices.model.CloudinaryConfig;
import com.initializers.services.apiservices.model.actionparam.GenerateApp;
import com.initializers.services.apiservices.multitenancy.MultiTenantConfig;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.others.AppIdGenerator;
import com.initializers.services.apiservices.others.PipelineUtil;
import com.initializers.services.apiservices.repo.AdminAppDetailsRepo;
import com.initializers.services.apiservices.service.AdminAppDetailsService;

@Service
public class AdminAppDetailsServiceImpl implements AdminAppDetailsService {

	@Autowired
	AdminAppDetailsRepo adminAppDetailsRepo;
	@Autowired
	MultiTenantConfig multiTenantConfig;
	@Autowired
	CloudinaryConfig cloudinaryConfig;
	@Value("${initializers.app.package}")
	private String packageName;
	@Value("${initializers.gitlab.host}")
	private String gitlabHost;
	@Value("${initializers.gitlab.mobile.projectId}")
	private String mobileAppProjectId;
	@Value("${initializers.gitlab.access_token}")
	private String gitlabAccessToken;
	@Value("${initializers.gitlab.pipeline.access_token}")
	private String pipeLineAccessToken;
	@Value("${initializers.gitlab.maxBuildTime}")
	private int pipelineMaxBuildTime;
	@Value("${initializers.gitlab.maxRefreshTime}")
	private int pipelineMaxRefreshTime;
	@Value("${initializers.expo.display.baseURL}")
	private String expoBaseURL;
	@Value("${initializers.expo.display.baseURL.mobile}")
	private String expoBaseURLMobile;

	private RestTemplate restTemplate;

	public AdminAppDetailsServiceImpl(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@Override
	public AdminAppDetails getAdminAppDetails() {
		AdminAppDetails adminAppDetails = adminAppDetailsRepo.findFirstById(TenantContext.getAppId());
		return checkBuildStatus(adminAppDetails);
	}

	@Override
	public String getAppNameById(String appId) {
		AdminAppDetails adminAppDetails = adminAppDetailsRepo.findFirstAppNameById(appId);
		if (adminAppDetails != null) {
			return adminAppDetails.getAppName();
		} else {
			return null;
		}
	}

	@Override
	public AdminAppDetails triggerBuildPipeline(GenerateApp generateApp, MultipartFile image) {
		AdminAppDetails adminAppDetails = updateAdminAppDetailsFromGenerateApp(generateApp, image);
		triggerBuild(adminAppDetails, TenantContext.getAppId());
		return adminAppDetails;
	}

	@Async
	private void triggerBuild(AdminAppDetails adminAppDetails, String appId) {
		HttpHeaders headers = new HttpHeaders();
		JSONObject jsonPayload;
		Integer pipelineId = 0;
		String status = null;
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("variables[INITIALIZER_APP_NAME]", adminAppDetails.getAppName());
		map.add("variables[INITIALIZER_APP_SLUG]", adminAppDetails.getAppName().replace(' ', '-'));
		map.add("variables[INITIALIZER_APP_PKG]", adminAppDetails.getAppPackage());
		map.add("variables[INITIALIZER_APP_ICON]",
				adminAppDetails.getAppIcon() == null ? "" : adminAppDetails.getAppIcon());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

		String url = generateTriggerPipeLineURL();
		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
			jsonPayload = new JSONObject(response.getBody());
			pipelineId = jsonPayload.getInt("id");
			status = jsonPayload.getString("status");
		} catch (Exception e) {
//			TODO: Log exception
			updatePipelineDetailsOnTrigger(appId, 0, Status.pipelineFailure);
		}
		if (pipelineId > 0 && status != null) {
			updatePipelineDetailsOnTrigger(appId, pipelineId, status);
		} else {
			updatePipelineDetailsOnTrigger(appId, 0, Status.pipelineFailure);
		}
	}

//	similar ref in TransientUserDetailsServiceImpl
	private String generateAppPackage(String appName) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof AnonymousAuthenticationToken) {
			throw new UserNotFoundException();
		} else {
			String userName = authentication.getName();
			String bias = AppIdGenerator.getBias(userName);
			appName = appName.trim().replace(" ", "").toLowerCase();
			return (packageName + "." + appName + bias);
		}
	}

	private String generateTriggerPipeLineURL() {
		return "" + gitlabHost + "/projects/" + mobileAppProjectId + "/trigger/pipeline?token=" + pipeLineAccessToken
				+ "&ref=main";
	}

	private AdminAppDetails updateAdminAppDetailsFromGenerateApp(GenerateApp generateApp, MultipartFile image) {
		AdminAppDetails adminAppDetails = adminAppDetailsRepo
				.findFirstAppIconPipelineStatusById(TenantContext.getAppId());
		String[] notToTriggerStatus = { Status.pipelineTriggered, Status.pipelineCreated, Status.pipelineRunning };
		if (Arrays.asList(notToTriggerStatus).contains(adminAppDetails.getPipelineStatus())) {
			throw new ActiveBuildExistException();
		} else {
			adminAppDetails.setId(TenantContext.getAppId());
			if (generateApp.getAppName() != null) {
				adminAppDetails.setAppName(generateApp.getAppName());
				adminAppDetails.setAppPackage(generateAppPackage(generateApp.getAppName()));
			}
			if (image != null) {
				try {
					String imageUrl = cloudinaryConfig.addImage(image, "png", true);
//					delete old image
					cloudinaryConfig.deleteImage(adminAppDetails.getAppIcon());
					adminAppDetails.setAppIcon(imageUrl);
				} catch (Exception e) {
					// log error
				}
			}
			if (generateApp.getSupportedPlatform() != null) {
				adminAppDetails.setSupportedPlatform(generateApp.getSupportedPlatform());
			}
			if (generateApp.getAppCategory() != null) {
				adminAppDetails.setCategory(generateApp.getAppCategory());
			}
			if (generateApp.getSelectedTheme() != null) {
				try {
					ObjectId themeId = new ObjectId(generateApp.getSelectedTheme());
					adminAppDetails.setTheme(themeId);
				} catch (Exception ex) {
//				log exception
				}
			}
			adminAppDetails.setPipelineStatus(Status.pipelineTriggered);
			return adminAppDetailsRepo.save(adminAppDetails);
		}
	}

	private void updatePipelineDetailsOnTrigger(String appId, Integer pipelineId, String status) {
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(appId));
		Update update = new Update();
		update.set("pipelineId", pipelineId);
		update.set("pipelineStatus", status);
		update.set("triggeredTime", LocalDateTime.now());
		multiTenantConfig.mongoTemplateShared().findAndModify(query, update, AdminAppDetails.class);
	}

	@Override
	public AdminAppDetails checkBuildStatus(AdminAppDetails adminAppDetails) {
		// if build has not reached final state and it has not refreshed recently ==>
		// refresh from pipeline else send data from DB
		if (adminAppDetails != null && adminAppDetails.getPipelineStatus() != null
				&& (!adminAppDetails.getPipelineStatus().equals(Status.PipelineSuccess)
						|| !adminAppDetails.getPipelineStatus().equals(Status.pipelineFailure))
						& shouldRefreshFromPipeline(adminAppDetails)) {
			Map<String, Object> jobDetails = getPipelineJob(adminAppDetails.getPipelineId());
			if (jobDetails != null) {
				String pipelineStatus = (String) jobDetails.get("status");
				adminAppDetails.setPipelineStatus(pipelineStatus);
				adminAppDetails.setRefreshTime(LocalDateTime.now());
				if (pipelineStatus.equals(Status.PipelineSuccess)) {
					adminAppDetails.setExpoBuildUrl(extactExpoUrlFromJobLog((Integer) jobDetails.get("jobId")));
				}
				adminAppDetails = adminAppDetailsRepo.save(adminAppDetails);
			}
		}
		return adminAppDetails;
	}

	private Boolean shouldRefreshFromPipeline(AdminAppDetails adminAppDetails) {
		LocalDateTime triggeredTime = adminAppDetails.getTriggeredTime() != null
				? adminAppDetails.getTriggeredTime().plusMinutes(pipelineMaxBuildTime)
				: LocalDateTime.now();
		LocalDateTime refreshedTime = adminAppDetails.getRefreshTime() != null
				? adminAppDetails.getRefreshTime().plusMinutes(pipelineMaxRefreshTime)
				: LocalDateTime.now();
		LocalDateTime now = LocalDateTime.now();
		if (triggeredTime.compareTo(now) <= 0 && refreshedTime.compareTo(now) <= 0) {
			return true;
		}
		return false;
	}

	private String extactExpoUrlFromJobLog(Integer jobId) {
		String url = "" + gitlabHost + "/projects/" + mobileAppProjectId + "/jobs/" + jobId + "/trace?private_token="
				+ gitlabAccessToken;
		String jobLogs = restTemplate.getForObject(url, String.class);
		return PipelineUtil.extractExpoURL(jobLogs, expoBaseURL, expoBaseURLMobile);
	}

//	will return null if there is network issue or pipeline doesn't exist
	private Map<String, Object> getPipelineJob(Integer pipelineId) {
		Map<String, Object> jobDetails = new HashMap<>();
		String url = "" + gitlabHost + "/projects/" + mobileAppProjectId + "/pipelines/" + pipelineId
				+ "/jobs?private_token=" + gitlabAccessToken;
		try {
			String pipelinePayload = restTemplate.getForObject(url, String.class);
			JSONArray jsonArray = new JSONArray(pipelinePayload);
			if (jsonArray.length() > 0) {
				JSONObject lastBuild = jsonArray.getJSONObject(jsonArray.length() - 1);
				Integer jobId = lastBuild.getInt("id");
				String jobStatus = lastBuild.getString("status");
				JSONObject pipeline = lastBuild.getJSONObject("pipeline");
				Integer receivedPipelineId = pipeline.getInt("id");
				String receivedPipelineStatus = pipeline.getString("status");
				if (pipelineId.equals(receivedPipelineId)) {
					jobDetails.put("status", receivedPipelineStatus);
					jobDetails.put("jobId", jobId);
					jobDetails.put("jobStatus", jobStatus);
					return jobDetails;
				}
			}
		} catch (Exception exception) {
			return null;
		}
		return null;
	}

	@Override
	public AdminAppDetails getAppNameIconById(String appId) {
		return adminAppDetailsRepo.findFirstAppNameAppIconById(appId);
	}

}
