package com.initializers.services.apiservices.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.exception.PayementGatewayNotFoundException;
import com.initializers.services.apiservices.exception.RequiredValueMissingException;
import com.initializers.services.apiservices.model.payment.AdminPayementGatewayKey;
import com.initializers.services.apiservices.model.payment.PaymentGateway;
import com.initializers.services.apiservices.repo.AdminPayementGatewayKeyRepo;
import com.initializers.services.apiservices.repo.PaymentGatewayRepo;
import com.initializers.services.apiservices.service.AdminPayementGatewayKeyService;

@Service
public class AdminPayementGatewayKeyServiceImpl implements AdminPayementGatewayKeyService {

	@Autowired
	private AdminPayementGatewayKeyRepo adminPayementGatewayKeyRepo;
	@Autowired
	private PaymentGatewayRepo paymentGatewayRepo;

	@Override
	public AdminPayementGatewayKey addAdminPayementGatewayKey(AdminPayementGatewayKey adminPayementGatewayKey) {

		PaymentGateway result = paymentGatewayRepo.findFirstById(adminPayementGatewayKey.getGatewayID());

		if (adminPayementGatewayKey.getGatewayID() == null) {
			throw new RequiredValueMissingException();
		} else if (adminPayementGatewayKey.getType() == '\0' && adminPayementGatewayKey.getStatus() == null) {
			throw new RequiredValueMissingException();
		} else if (result == null) {
			throw new PayementGatewayNotFoundException();
		} else if (adminPayementGatewayKey.getSecretKey() == null || adminPayementGatewayKey.getPublicKey() == null) {
			throw new PayementGatewayNotFoundException();
		} else
			return adminPayementGatewayKeyRepo.save(adminPayementGatewayKey);
	}

	@Override
	public Page<AdminPayementGatewayKey> getAdminPayementGatewayKey(Pageable pageable) {

		return adminPayementGatewayKeyRepo.findAll(pageable);
	}

	@Override
	public AdminPayementGatewayKey updateAdminPayementGatewayKey(AdminPayementGatewayKey adminPayementGatewayKey) {

//		TODO : only public and secret key can be editable
		if (adminPayementGatewayKey.getGatewayID() == null) {
			throw new RequiredValueMissingException();
		} else {
			return adminPayementGatewayKeyRepo.save(adminPayementGatewayKey);
		}
	}

	@Override
	public AdminPayementGatewayKey getAdminPayementGatewayKeyById(ObjectId adminPayementGatewayKeyId) {

		return adminPayementGatewayKeyRepo.findFirstById(adminPayementGatewayKeyId);
	}

	@Override
	public Map<String, Object> deleteAdminPayementGatewayKeyById(ObjectId adminPayementGatewayKeyId) {
		Map<String, Object> returnVal = new HashMap<>();
		returnVal.put("Info", "Admin Payment Gateway Key Deleted By Id");
		adminPayementGatewayKeyRepo.deleteById(adminPayementGatewayKeyId);
		return returnVal;
	}
}
