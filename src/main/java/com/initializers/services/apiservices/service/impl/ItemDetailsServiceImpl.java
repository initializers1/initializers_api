package com.initializers.services.apiservices.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.elasticsearch.GlobalSearchFactroy;
import com.initializers.services.apiservices.exception.CategoryNotFoundException;
import com.initializers.services.apiservices.exception.ItemNotFoundException;
import com.initializers.services.apiservices.exception.RequiredValueMissingException;
import com.initializers.services.apiservices.exception.SubCategoryNotFoundException;
import com.initializers.services.apiservices.model.UserWishlist;
import com.initializers.services.apiservices.model.actionResult.ChangeStatusResult;
import com.initializers.services.apiservices.model.actionparam.ChangeStatus;
import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.model.item.ItemAvailability;
import com.initializers.services.apiservices.model.item.ItemDescription;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.model.temp.ItemDetailsList;
import com.initializers.services.apiservices.model.temp.ItemDetailsTemp;
import com.initializers.services.apiservices.others.FilterAction;
import com.initializers.services.apiservices.others.FilterValue;
import com.initializers.services.apiservices.others.QueryString;
import com.initializers.services.apiservices.others.Suggestions;
import com.initializers.services.apiservices.repo.ItemDetailsRepo;
import com.initializers.services.apiservices.service.ItemAvailabilityService;
import com.initializers.services.apiservices.service.ItemCategoryService;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.ItemSubCategoryService;
import com.initializers.services.apiservices.service.UserWishlistService;

@Service
public class ItemDetailsServiceImpl implements ItemDetailsService {

	@Autowired
	private ItemCategoryService itemCategoryService;
	@Autowired
	private ItemSubCategoryService itemSubCategoryService;
	@Autowired
	private ItemDetailsRepo itemDetailsRepo;
	@Autowired
	private ItemAvailabilityService itemAvailabilityService;
	@Autowired
	private UserWishlistService userWishlistService;
	@Autowired
	private GlobalSearchFactroy globalSearchFactory;
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Object getAdminItemDetails(ObjectId id) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		ItemDetailsList itemDetails = new ItemDetailsList();
		Map<String, Object> resultVal = new HashMap<String, Object>();
//		if (itemDetails == null) {
//			throw new ItemNotFoundException();
//		} else {
		list.add(LookupOperation.newLookup().from("itemSubCategory").localField("subCategoryId").foreignField("_id")
				.as("join_subcat"));
		list.add(LookupOperation.newLookup().from("itemCategory").localField("categoryId").foreignField("_id")
				.as("join_cat"));
		list.add(Aggregation.unwind("join_cat", true));
		list.add(Aggregation.unwind("join_subcat", true));
		list.add(Aggregation.match(Criteria.where("_id").is(id)));
		list.add(Aggregation.project(Fields
				.fields("_id", "name", "subCategoryId", "imageLinks", "description", "itemAvailabilities", "status",
						"categoryId")
				.and(Fields.field("subCategoryName", "join_subcat.name"))
				.and(Fields.field("categoryName", "join_cat.name"))));
		Aggregation aggregation = Aggregation.newAggregation(list);
		List<ItemDetailsList> results = mongoTemplate.aggregate(aggregation, "itemDetails", ItemDetailsList.class)
				.getMappedResults();
		if (results.size() > 0) {
			itemDetails = results.get(0);
			List<ItemAvailability> itemAvailability = itemAvailabilityService.getAvailabilityByItemId(id, 'N');
			List<String> images = itemDetails.getImageLinks();
			resultVal.put("id", itemDetails.getId().toHexString());
			resultVal.put("categoryId", itemDetails.getCategoryId());
			resultVal.put("subCategoryId", itemDetails.getSubCategoryId());
			resultVal.put("categoryName", itemDetails.getCategoryName());
			resultVal.put("subCategoryName", itemDetails.getSubCategoryName());
			resultVal.put("name", itemDetails.getName());
			resultVal.put("imageLinks", images);
			resultVal.put("description", itemDetails.getDescription());
			resultVal.put("itemAvailabilities", itemAvailability);
			resultVal.put("status", itemDetails.getStatus());
			resultVal.put("defaultImage", images == null || images.size() <= 0 ? "" : images.get(0));
		}
		return resultVal;
	}

	@Override
	public ItemDetailsTemp getItemDetails(ObjectId id) {
		ItemDetails itemDetails = itemDetailsRepo.findFirstId(id);
		if (itemDetails == null) {
			throw new ItemNotFoundException();
		} else {
			ItemDetailsTemp itemDetailsTemp = new ItemDetailsTemp();
			List<ItemAvailability> itemAvailability = itemAvailabilityService.getAvailabilityByItemId(id);
			itemDetailsTemp.setId(itemDetails.getId());
			itemDetailsTemp.setCategoryId(itemDetails.getCategoryId());
			itemDetailsTemp.setSubCategoryId(itemDetails.getSubCategoryId());
			itemDetailsTemp.setName(itemDetails.getName());
			itemDetailsTemp.setImageLinks(itemDetails.getImageLinks());
			itemDetailsTemp.setDescription(itemDetails.getDescription());
			itemDetailsTemp.setItemAvailabilities(itemAvailability);
			return itemDetailsTemp;
		}
	}

	@Override
	public String getItemNameById(ObjectId id) {
		ItemDetails itemDetails = itemDetailsRepo.findFirstNameByIdAdmin(id);
		return itemDetails == null ? "" : itemDetails.getName();
	}

	@Override
	public Page<Object> getItemDetailsPage(QueryString queryString, Suggestions suggestions, Pageable pageable) {
		Query countQuery = new Query();
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		if (suggestions.getKey() != null && suggestions.getValue() != null) {
			List<Object> suggestionsList = new ArrayList<>();
			list.add(Aggregation.project(Fields.fields().and(Fields.field("key", suggestions.getKey()))
					.and(Fields.field("value", suggestions.getValue()))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			suggestionsList = mongoTemplate.aggregate(aggregation, "itemDetails", Suggestions.class).getMappedResults()
					.stream().collect(Collectors.toList());
			Page<Object> page = new PageImpl<>(suggestionsList, pageable, suggestionsList.size());
			return page;
		} else {
			list.add(LookupOperation.newLookup().from("itemSubCategory").localField("subCategoryId").foreignField("_id")
					.as("join_subcat"));
			list.add(LookupOperation.newLookup().from("itemCategory").localField("categoryId").foreignField("_id")
					.as("join_cat"));
			list.add(Aggregation.unwind("join_cat", true));
			list.add(Aggregation.unwind("join_subcat", true));
			if (queryString != null && queryString.getSearch() != null) {
				String search = queryString.getSearch();
				List<Object> searchIdList = new ArrayList<>();
				List<ItemDetailsES> searchHit = globalSearchFactory.searchItemDetails(search);
				searchHit.stream().forEach((hit) -> {
					searchIdList.add(hit.getId());
				});
				list.add(Aggregation.match(Criteria.where("_id").in(searchIdList)));
				countQuery.addCriteria(Criteria.where("_id").in(searchIdList));
			}
			if (queryString != null && queryString.getFilter() != null) {
				String[] filter = queryString.getFilter();
				List<FilterValue> filterValues = new FilterAction().getFilterValue(filter);
				for (FilterValue filterValue : filterValues) {
					List<Object> alteredFilterValue = new ArrayList<>();
					if (filterValue.getName() != null && filterValue.getOperator() != null
							&& filterValue.getValue() != null) {
						switch (filterValue.getName()) {
						case "subCategoryId":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						case "categoryId":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						case "_id":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						default:
							alteredFilterValue = filterValue.getValue().stream().collect(Collectors.toList());
							break;
						}
						list.add(Aggregation.match(Criteria.where(filterValue.getName()).in(alteredFilterValue)));
						countQuery.addCriteria(Criteria.where(filterValue.getName()).in(alteredFilterValue));
					}
				}
			}
			list.add(Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize()));
			list.add(Aggregation.limit(pageable.getPageSize()));
			list.add(Aggregation.project(Fields.fields("_id", "name", "imageLinks", "status")
					.and(Fields.field("subCategoryId", "join_subcat.name"))
					.and(Fields.field("categoryId", "join_cat.name"))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			List<ItemDetailsList> results = mongoTemplate.aggregate(aggregation, "itemDetails", ItemDetailsList.class)
					.getMappedResults();
			List<Object> resultObject = new ArrayList<>();
			results.stream().forEach((item) -> {
				resultObject.add(item);
			});
			Long totalSize = mongoTemplate.count(countQuery, ItemDetails.class);
			Page<Object> page = new PageImpl<>(resultObject, pageable, totalSize);
			return page;
		}
	}

	@Override
	public List<Object> getItemDetailsList(String[] filter, Suggestions suggestions) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		if (suggestions.getKey() != null && suggestions.getValue() != null) {
			List<Object> suggestionsList = new ArrayList<>();
			list.add(Aggregation.project(Fields.fields().and(Fields.field("key", suggestions.getKey()))
					.and(Fields.field("value", suggestions.getValue()))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			suggestionsList = mongoTemplate.aggregate(aggregation, "itemDetails", Suggestions.class).getMappedResults()
					.stream().collect(Collectors.toList());
			return suggestionsList;
		} else {
			List<Object> itemDetailsList = new ArrayList<>();
			list.add(LookupOperation.newLookup().from("itemSubCategory").localField("subCategoryId").foreignField("_id")
					.as("join_subcat"));
			list.add(LookupOperation.newLookup().from("itemCategory").localField("categoryId").foreignField("_id")
					.as("join_cat"));
			list.add(Aggregation.unwind("join_cat", true));
			list.add(Aggregation.unwind("join_subcat", true));
			if (filter != null) {
				List<FilterValue> filterValues = new FilterAction().getFilterValue(filter);
				for (FilterValue filterValue : filterValues) {
					List<Object> alteredFilterValue = new ArrayList<>();
					if (filterValue.getName() != null && filterValue.getOperator() != null
							&& filterValue.getValue() != null) {
						switch (filterValue.getName()) {
						case "subCategoryId":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						case "categoryId":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						case "_id":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						default:
							alteredFilterValue = filterValue.getValue().stream().collect(Collectors.toList());
							break;
						}
						list.add(Aggregation.match(Criteria.where(filterValue.getName()).in(alteredFilterValue)));
					}
				}
			}
			list.add(Aggregation.project(Fields.fields("_id", "name", "imageLinks", "status")
					.and(Fields.field("subCategoryId", "join_subcat.name"))
					.and(Fields.field("categoryId", "join_cat.name"))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			List<ItemDetailsList> results = mongoTemplate.aggregate(aggregation, "itemDetails", ItemDetailsList.class)
					.getMappedResults();
			for (ItemDetailsList item : results) {
				Map<String, Object> resultVal = new HashMap<String, Object>();
				resultVal.put("id", item.getId().toHexString());
				resultVal.put("name", item.getName());
				resultVal.put("categoryId", item.getCategoryId());
				resultVal.put("subCategoryId", item.getSubCategoryId());
				List<String> images = item.getImageLinks();
				resultVal.put("imageLinks", images != null && images.size() > 0 ? images.get(0) : "");
				String status = item.getStatus();
				resultVal.put("status", status);
				resultVal.put("state", status.equals("Active") ? "Success" : "Error");
				itemDetailsList.add(resultVal);
			}
			return itemDetailsList;
		}
	}

	@Override
	public Object updateItemDetailsAdmin(ItemDetails itemDetails) {
		if (itemDetails.getId() == null) {
			throw new RequiredValueMissingException();
		}
		ItemDetails dbItemDetails = itemDetailsRepo.findFirstId(itemDetails.getId());

		if (dbItemDetails == null) {
			throw new ItemNotFoundException();
		}

		if (itemDetails.getCategoryId() != null) {
			if (!itemCategoryService.checkItemCategory(itemDetails.getCategoryId())) {
				throw new CategoryNotFoundException();
			} else {
				dbItemDetails.setCategoryId(itemDetails.getCategoryId());
			}
		}
		if (itemDetails.getSubCategoryId() != null) {
			if (!itemSubCategoryService.checkItemSubCategory(itemDetails.getSubCategoryId())) {
				throw new SubCategoryNotFoundException();
			} else
				dbItemDetails.setSubCategoryId(itemDetails.getSubCategoryId());
		}

		if (itemDetails.getName() != null && !itemDetails.getName().equals("")) {
			dbItemDetails.setName(itemDetails.getName());
		}
		if (itemDetails.getDescription() != null) {
			ItemDescription itemDescription = itemDetails.getDescription();
			if (itemDetails.getDescription().getDisclaimer() != null
					&& !itemDetails.getDescription().getDisclaimer().equals("")) {
				itemDescription.setDisclaimer(itemDetails.getDescription().getDisclaimer());
			}
			if (itemDetails.getDescription().getItemProperties() != null
					&& !itemDetails.getDescription().getItemProperties().equals("")) {
				itemDescription.setItemProperties(itemDetails.getDescription().getItemProperties());
			}
			if (itemDetails.getDescription().getSellerName() != null
					&& !itemDetails.getDescription().getSellerName().equals("")) {
				itemDescription.setSellerName(itemDetails.getDescription().getSellerName());
			}
			dbItemDetails.setDescription(itemDescription);
		}
		if (itemDetails.getStatus() != null && !itemDetails.getStatus().equals("")) {
			dbItemDetails.setStatus(itemDetails.getStatus());
		}
		itemDetailsRepo.save(dbItemDetails);
		return getAdminItemDetails(dbItemDetails.getId());
	}

	@Override
	public Object addItemDetailsAdmin(ItemDetails itemDetails) {

		if (itemDetails.getCategoryId() != null) {
			if (!itemCategoryService.checkItemCategory(itemDetails.getCategoryId())) {
				throw new CategoryNotFoundException();
			}
		}
		if (itemDetails.getSubCategoryId() != null) {
			if (!itemSubCategoryService.checkItemSubCategory(itemDetails.getSubCategoryId())) {
				throw new SubCategoryNotFoundException();
			}
		}
		if (itemDetails.getStatus() == null || itemDetails.getStatus().equals("")) {
			itemDetails.setStatus(Status.InActive);
		}

		ItemDetails dbItemDetails = itemDetailsRepo.save(itemDetails);
		return getAdminItemDetails(dbItemDetails.getId());
	}

	@Override
	public ItemDetails getItemListById(ObjectId id, ObjectId... availabilityId) {
		ItemDetails itemDetails = new ItemDetails();
		itemDetails = itemDetailsRepo.findListByIdByStatus(id, Status.Active);

		if (itemDetails != null) {
			if (itemDetails.getImageLinks() != null && itemDetails.getImageLinks().size() > 0) {
				itemDetails.setImageLink(itemDetails.getImageLinks().get(0));
			}
			List<ItemAvailability> itemAvailability = itemAvailabilityService.getAvailabilityByItemId(id);

			itemDetails.setItemAvailability(itemAvailability);

			return itemDetails;
		}
		return null;
	}

	@Override
	public List<ItemDetails> getItemDetailsBySubCategory(ObjectId subCategoryID) {
		return itemDetailsRepo.findBySubCategoryId(subCategoryID);
	}

	@Override
	public ItemDetails getItemDetails(ObjectId userId, ObjectId itemId) {
		ItemDetails itemDetails = itemDetailsRepo.getItemDetailsById(itemId);
		if (itemDetails == null) {
			throw new ItemNotFoundException("Item Not found ");
		} else {
			if (userId != null) {
				UserWishlist userWishlist = userWishlistService.getUserWishlist(userId);
				if (userWishlist != null && userWishlist.getItemsId().contains(itemId)) {
					itemDetails.setIsWishlist(true);
				} else {
					itemDetails.setIsWishlist(false);
				}
			}
			return itemDetails;
		}
	}

	@Override
	public Boolean checkItemDetails(ObjectId id) {
		// true - exist , false - doesn't exist
		if (itemDetailsRepo.findFirstNameById(id) != null) {
			return true;
		}
		return false;
	}

	@Override
	public ItemDetails getItemNameImageLinkById(ObjectId id) {
		return itemDetailsRepo.findFirstNameImageLinksById(id);
	}

	@Override
	public Page<ItemDetails> getItemDetailsByType(ObjectId categoryID, Pageable pageable, String type) {
		return itemDetailsRepo.getItemDetails(categoryID, pageable, type);
	}

	@Override
	public List<ChangeStatusResult> changeStatus(ChangeStatus changeStatus) {
		// TODO Auto-generated method stub
		Iterable<ObjectId> ids = Arrays.asList(changeStatus.getId());
		List<ChangeStatusResult> result = new ArrayList<>();
		Iterable<ItemDetails> items = itemDetailsRepo.findAllById(ids);
		items.forEach((item) -> {
			item.setStatus(changeStatus.getStatus());
			result.add(new ChangeStatusResult(item.getId().toHexString(), item.getStatus()));
		});
		itemDetailsRepo.saveAll(items);
		return result;
	}
}
