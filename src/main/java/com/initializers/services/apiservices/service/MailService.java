package com.initializers.services.apiservices.service;

import java.io.IOException;

import javax.mail.MessagingException;

import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.constant.MailTemplete;
import com.initializers.services.apiservices.constant.Scope;

@Service
public interface MailService {
	public void sendEmail(String userName, String emailId, int OTP, String appName, Scope scope, MailTemplete templete) throws MessagingException, IOException;
	public void notifyAdmin(String subject, String body);
}
