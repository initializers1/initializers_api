package com.initializers.services.apiservices.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.Theme;
import com.initializers.services.apiservices.repo.ThemeRepo;
import com.initializers.services.apiservices.service.ThemeService;

@Service
public class ThemeServiceImpl implements ThemeService {

	@Autowired
	ThemeRepo themeRepo;

	@Override
	public List<Theme> getAllThemes() {
		return themeRepo.findAll();
	}

	@Override
	public Theme addNewTheme(Theme theme) {
		return themeRepo.save(theme);
	}

}
