package com.initializers.services.apiservices.service;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.TransientUserDetails;

@Service
public interface TransientUserDetailsService {

	ObjectId addUser(TransientUserDetails transientUserDetails);
	
	TransientUserDetails getUser(ObjectId userId);
	
	void deleteUser(ObjectId userId);
}
