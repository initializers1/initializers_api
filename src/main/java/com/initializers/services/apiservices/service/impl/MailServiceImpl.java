package com.initializers.services.apiservices.service.impl;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.constant.MailTemplete;
import com.initializers.services.apiservices.constant.Scope;
import com.initializers.services.apiservices.others.MailerUtil;
import com.initializers.services.apiservices.service.MailService;

@Service
public class MailServiceImpl implements MailService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Override
	@Async
	public void sendEmail(String userName, String emailId, int OTP, String appName, Scope scope, MailTemplete templete)
			throws MessagingException, IOException {
		String helperMessage = scope == Scope.CLIENT ? "Happy shopping with us!!!"
				: "Get your online store in minuites!!!";
		MimeMessage msg = javaMailSender.createMimeMessage();

		// true = multi-part message
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);

		helper.setTo(emailId);

		helper.setSubject(appName);

		switch (templete) {
		case RegenerateOTP:
			helper.setText(MailerUtil.regenerateOtpTemplete(userName, appName, String.valueOf(OTP), helperMessage),
					true);
			break;
		case RegisterUser:
			helper.setText(MailerUtil.registerUserTemplete(userName, appName, String.valueOf(OTP), helperMessage),
					true);
			break;
		default:
			break;
		}

		javaMailSender.send(msg);
	}

	@Override
	@Async
	public void notifyAdmin(String subject, String body) {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo("karthikhanumanthaiah@gmail.com", "initializers.demo.01@gmail.com");
		if (subject == null) {
			msg.setSubject("Initializers---Admin message");
		} else {
			msg.setSubject(subject);
		}
		msg.setText(body);
		javaMailSender.send(msg);
	}

}
