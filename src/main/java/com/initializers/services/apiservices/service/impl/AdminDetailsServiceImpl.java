package com.initializers.services.apiservices.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.AdminDetails;
import com.initializers.services.apiservices.repo.AdminDetailsRepo;
import com.initializers.services.apiservices.service.AdminDetailsService;

@Service
public class AdminDetailsServiceImpl implements AdminDetailsService {

	@Autowired
	AdminDetailsRepo adminDetailsRepo;
	
	@Override
	public AdminDetails addAdminDetails(AdminDetails adminDetails) {
		return adminDetailsRepo.save(adminDetails);
	}

	@Override
	public AdminDetails getAdminDetailsById(ObjectId id) {
		// TODO Auto-generated method stub
		return null;
	}

}
