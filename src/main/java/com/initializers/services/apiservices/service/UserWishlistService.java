package com.initializers.services.apiservices.service;

import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.UserWishlist;

@Service
public interface UserWishlistService {

	UserWishlist addUserWishlist(ObjectId userID, ObjectId itemID);
	
	Map<String, Object> getUserWishlist(ObjectId userID, Pageable page);
	
	UserWishlist getUserWishlist(ObjectId userID);
	
	Object deleteUserWishlist(ObjectId userID, ObjectId itemID);
	
}
