package com.initializers.services.apiservices.service;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import com.initializers.services.apiservices.model.payment.UserPayment;

@Service
public interface UserPaymentService {
	
	List<UserPayment> getUserPayment();
	
	UserPayment getUserPaymentByTransactionID(ObjectId orderId);

	UserPayment addUserPayment(UserPayment userPayment);
	
	UserPayment upadateUserPayment(UserPayment userPayment);
	
	void updateUserPaymentStatus(String externalOrderId, String gatewayName, String status, String failureReason);

}
