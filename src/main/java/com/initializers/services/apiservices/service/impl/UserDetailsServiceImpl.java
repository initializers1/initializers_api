package com.initializers.services.apiservices.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.exception.UserNotFoundException;
import com.initializers.services.apiservices.model.UserDetails;
import com.initializers.services.apiservices.repo.UserDetailsRepo;
import com.initializers.services.apiservices.service.TransientUserDetailsService;
import com.initializers.services.apiservices.service.UserDetailsService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
	@Autowired
	private UserDetailsRepo userDetailsRepo;
	@Autowired
	private TransientUserDetailsService transientUserService;
	
	public List<UserDetails> getAllUsers() {
		List<UserDetails> userDetails = new ArrayList<>();
		userDetailsRepo.findAll().forEach(userDetails::add);
		return userDetails;
	}

	@Override
	public ObjectId addUser(UserDetails userDetails) {
		userDetails.setStatus(Status.Active);
		UserDetails userSavedData = userDetailsRepo.save(userDetails);
		return userSavedData.getId();
	}

	@Override
	public UserDetails getUser(ObjectId userId) {
		UserDetails userDetails = userDetailsRepo.findFirstById(userId);
		if(userDetails == null) throw new UserNotFoundException();
		return userDetails;
	}

	@Override
	public Object deleteUser(ObjectId userId) {
		Map<String,Object> returnVal = new HashMap<>();
		returnVal.put("Info", "User deleted");
		if(getUser(userId) != null) {
			UserDetails userDetails = getUser(userId);
			userDetails.setStatus(Status.InActive);
			userDetailsRepo.save(userDetails);
		}else if(transientUserService.getUser(userId) != null) {
			transientUserService.deleteUser(userId);
		}else {
			throw new UserNotFoundException();
		}
		returnVal.put("Info", "user deleted successfuly");
		return returnVal;
	}

	@Override
	public UserDetails updateUserDetail(UserDetails userDetails) {
		if(userDetails == null) {
//			throw new RequiredValueMissingException();
		}else {
			UserDetails dbUserDetails = userDetailsRepo.findFirstById(userDetails.getId());
			if(dbUserDetails == null) {
//				throw new UserNotFoundException();
			}else {
//				final String email = userDetails.getEmail();
				final String firstName = userDetails.getFirstName();
				final String lastName = userDetails.getLastName();
//				final String password = userDetails.getPassword();
				
//				TODO need to validate with otp before update
//				if(email != dbUserDetails.getEmail() && email != null) {
//					dbUserDetails.setEmail(email);
//				}
				if(firstName != dbUserDetails.getFirstName() && firstName != null) {
					dbUserDetails.setFirstName(firstName);
				}
				if(lastName != dbUserDetails.getLastName() && lastName != null) {
					dbUserDetails.setLastName(lastName);
				}
//				if(password != dbUserDetails.getPassword() && password != null) {
//					dbUserDetails.setPassword(password);
//				}
				UserDetails userDetailsAfterSave = userDetailsRepo.save(dbUserDetails);
//				userDetailsAfterSave.setPassword(null);
				userDetailsAfterSave.setStatus(null);
				return userDetailsAfterSave;
			}
		}
		return null;
	}

	@Override
	public String getNameById(ObjectId userId) {
		UserDetails userDetails = userDetailsRepo.findNameById(userId);
		if(userDetails != null) {
			return userDetails.getFirstName() + " " + userDetails.getLastName();
		}
		return null;
	}
	
	@Override
	public Boolean checkUser(ObjectId userId) {
		if(userDetailsRepo.findNameById(userId) != null) {
			return true;
		}
		return false;
	}


	
}
