package com.initializers.services.apiservices.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.initializers.services.apiservices.exception.ImageUploadException;
import com.initializers.services.apiservices.exception.RequiredValueMissingException;
import com.initializers.services.apiservices.model.CloudinaryConfig;
import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.model.item.ItemSubCategory;
import com.initializers.services.apiservices.repo.ItemCategoryRepo;
import com.initializers.services.apiservices.repo.ItemDetailsRepo;
import com.initializers.services.apiservices.repo.ItemSubCategoryRepo;
import com.initializers.services.apiservices.service.ImageService;

@Service
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ItemDetailsRepo itemDetailsRepo;
	@Autowired
	private ItemCategoryRepo itemCategoryRepo;
	@Autowired
	private ItemSubCategoryRepo itemSubCategoryRepo;
	@Autowired
	private CloudinaryConfig cloudinaryConfig;

	@Override
	public Object addNewImage(List<MultipartFile> images, String type, String id) {
		List<String> imageLinks = new ArrayList<>();

		switch (type) {
		case "itemDetails":
			ItemDetails itemDetails = itemDetailsRepo.findFirstId(new ObjectId(id));
			imageLinks = itemDetails.getImageLinks() == null ? new ArrayList<>() : itemDetails.getImageLinks();
			for (MultipartFile image : images) {
				try {
					String imageUrl = cloudinaryConfig.addImage(image, null, false);
					if (imageUrl != null) {
						imageLinks.add(imageUrl);
					}
				} catch (IOException e1) {
					throw new ImageUploadException();
				}
			}
			itemDetails.setImageLinks(imageLinks);
			itemDetailsRepo.save(itemDetails);
			break;
		case "itemCategory":
			ItemCategory itemCategory = itemCategoryRepo.findFirstById(new ObjectId(id));
			for (MultipartFile image : images) {
				try {
					String imageUrl = cloudinaryConfig.addImage(image, null, false);
					if (imageUrl != null) {
						itemCategory.setImageLink(imageUrl);
					}
				} catch (IOException e1) {
					throw new ImageUploadException();
				}
				itemCategoryRepo.save(itemCategory);
			}
			break;
		case "itemSubcategory":
			ItemSubCategory itemSubCategory = itemSubCategoryRepo.findFirstById(new ObjectId(id));
			for (MultipartFile image : images) {
				try {
					String imageUrl = cloudinaryConfig.addImage(image, null, false);
					if (imageUrl != null) {
						itemSubCategory.setImageLink(imageUrl);
					}
				} catch (IOException e1) {
					throw new ImageUploadException();
				}
				itemSubCategoryRepo.save(itemSubCategory);
			}
			break;
		default:
			break;
		}
		return "1";
	}

	@Override
	public Object updateImage(String images, String type, String id) {
		if (images == null) {
			throw new RequiredValueMissingException();
		}
		switch (type) {
		case "itemDetails":
			ItemDetails itemDetails = itemDetailsRepo.findFirstId(new ObjectId(id));
			List<String> updatedImageLink = new ArrayList<>();
			String imageToDelete = images;
			try {
				cloudinaryConfig.deleteImage(images);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new RequiredValueMissingException();
			}
			if (itemDetails.getImageLinks() != null) {
				for (String image : itemDetails.getImageLinks()) {
					if (!image.equals(imageToDelete)) {
						updatedImageLink.add(image);
					}
				}
			}
			itemDetails.setImageLinks(updatedImageLink);
			itemDetailsRepo.save(itemDetails);
			break;
		case "itemCategory":
			ItemCategory itemCategory = itemCategoryRepo.findFirstById(new ObjectId(id));
			itemCategory.setImageLink(null);
			itemCategoryRepo.save(itemCategory);
			try {
				cloudinaryConfig.deleteImage(images);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new RequiredValueMissingException();
			}
			break;
		case "itemSubcategory":
			ItemSubCategory itemSubCategory = itemSubCategoryRepo.findFirstById(new ObjectId(id));
			itemSubCategory.setImageLink(null);
			itemSubCategoryRepo.save(itemSubCategory);
			try {
				cloudinaryConfig.deleteImage(images);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new RequiredValueMissingException();
			}
			break;
		default:
			break;
		}
		return null;
	}

}
