package com.initializers.services.apiservices.service;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.TransientUserDetails;

@Service
public interface AdminClientDetailsService {
	public AdminClientDetails getTenantForAdmin(String emailId);
	public AdminClientDetails getTenantForClient(String emailId, String appId);
	public AdminClientDetails getLoggingInUser(String loggedInUser);
	
	public AdminClientDetails createTenantDetails(TransientUserDetails transientUserDetails);
	public String getClientEmail(ObjectId clientId);
	public AdminClientDetails getUserDetails(String userName);
}
