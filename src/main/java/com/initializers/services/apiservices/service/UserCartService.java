package com.initializers.services.apiservices.service;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.UserCart;
import com.initializers.services.apiservices.model.UserCartList;
import com.initializers.services.apiservices.model.input.CreateOrderInput;
import com.initializers.services.apiservices.model.output.CreateOrderOutput;

@Service
public interface UserCartService {

	UserCart addUserCart(UserCart userCart);
	
	Page<UserCartList> getUserCartByuserId(ObjectId userId, Pageable pageable);
	
	CreateOrderOutput createOrder(CreateOrderInput createOrderInput);
}
