package com.initializers.services.apiservices.service;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.Address;

@Service
public interface AddressService {
	
	Address addAddress(Address address);
	
	Address getAddressById(ObjectId addressId);
	
	Address getAddressByIdByUserId(ObjectId addressId, ObjectId userID);
	
	List<Address> getAddressByUserId(ObjectId userId);
	
	Address updateAddress(Address address);

	Page<Address> getAddressByUserId(Integer first, Integer after, ObjectId userId);
	
	Boolean deleteAddressById(ObjectId addressId);
}
