package com.initializers.services.apiservices.service;

import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.payment.PaymentGateway;

@Service
public interface PaymentGatewayService {

	List<PaymentGateway> getPaymentGateway();
	
	PaymentGateway getPaymentGatewayById(ObjectId gatewayId);

	PaymentGateway addPaymentGateway(PaymentGateway paymentGateway);
	
	Map<String, Object> deletePaymentGatewayById(ObjectId gatewayId);
	

}
