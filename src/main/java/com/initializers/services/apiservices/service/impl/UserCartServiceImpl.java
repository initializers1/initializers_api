package com.initializers.services.apiservices.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.exception.AddressNotFoundException;
import com.initializers.services.apiservices.exception.CartItemNotFoundException;
import com.initializers.services.apiservices.exception.ItemAvailabilityException;
import com.initializers.services.apiservices.exception.ItemNotFoundException;
import com.initializers.services.apiservices.exception.UserNotFoundException;
import com.initializers.services.apiservices.model.Address;
import com.initializers.services.apiservices.model.OrderStatus;
import com.initializers.services.apiservices.model.UserCart;
import com.initializers.services.apiservices.model.UserCart.CompositeKeyCart;
import com.initializers.services.apiservices.model.UserCartList;
import com.initializers.services.apiservices.model.UserOrder;
import com.initializers.services.apiservices.model.UserOrderSet;
import com.initializers.services.apiservices.model.input.CreateOrderInput;
import com.initializers.services.apiservices.model.output.CreateOrderOutput;
import com.initializers.services.apiservices.others.ApplicationProperties;
import com.initializers.services.apiservices.repo.UserCartRepo;
import com.initializers.services.apiservices.repo.UserOrderSetRepo;
import com.initializers.services.apiservices.service.AddressService;
import com.initializers.services.apiservices.service.ItemAvailabilityService;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.OfferConfigurationDBService;
import com.initializers.services.apiservices.service.UserCartService;
import com.initializers.services.apiservices.service.UserDetailsService;

@Service
public class UserCartServiceImpl implements UserCartService {

	@Autowired
	private UserCartRepo userCartRepo;
	@Autowired
	private ItemDetailsService itemDetailsService;
	@Autowired
	private ItemAvailabilityService itemAvailabilityService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private UserOrderSetRepo userOrderSetRepo;
	@Autowired
	private OfferConfigurationDBService offerConfigurationDBService;
	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	public UserCart addUserCart(UserCart userCart) {
		if (userCart.getQuantity() <= 0) {
			userCartRepo.deleteById(userCart.getId());
			return userCart;
		}
		if (userDetailsService.getUser(userCart.getId().getUserId()) == null) {
			throw new UserNotFoundException();
		}
		if (itemDetailsService.getItemDetails(userCart.getId().getItemId()) == null) {
			throw new ItemNotFoundException();
		}
		if (itemAvailabilityService.getAvailabilityById(userCart.getId().getAvailabilityId(),
				userCart.getId().getItemId()) == null) {
			throw new ItemAvailabilityException();
		}
		return userCartRepo.save(userCart);
	}

	@Override
	public Page<UserCartList> getUserCartByuserId(ObjectId userId, Pageable pageable) {
		return userCartRepo.getUserCart(userId, pageable);
	}

	@Override
	public CreateOrderOutput createOrder(CreateOrderInput createOrderInput) {
		Float totalAmount = 0F;
		CreateOrderOutput createOrderOutput = new CreateOrderOutput();
		List<UserOrder> userOrderList = new ArrayList<>();
		UserOrderSet userOrderSet = new UserOrderSet();
		List<UserCart.CompositeKeyCart> cartIdforDeletion = new ArrayList<>();

		Address address = addressService.getAddressById(createOrderInput.getAddressId());
		if (address == null || !address.getUserId().equals(createOrderInput.getUserId())) {
			throw new AddressNotFoundException();
		}

		List<UserCartList> userCartList = userCartRepo.getUserCart(createOrderInput.getUserId());

		Iterator<UserCartList> userCartItr = userCartList.iterator();
		while (userCartItr.hasNext()) {
			Float amount = 0F;
			UserOrder userOrder = new UserOrder();
			UserCartList userCart = userCartItr.next();
//			TODO: If any items are missed user should be informed 
			if (userCart.getItemId() == null || userCart.getAvailabilityId() == null)
				continue;
			userOrder.setItemId(userCart.getItemId());
			userOrder.setAvailabilityId(userCart.getAvailabilityId());
			userOrder.setQuantity(userCart.getQuantity());
			amount += (userCart.getQuantity() * userCart.getDiscountPrice());
			userOrder.setAmount(userCart.getQuantity() * userCart.getDiscountPrice());
			totalAmount += amount;
			userOrderList.add(userOrder);
			UserCart.CompositeKeyCart key = new CompositeKeyCart();
			key.setAvailabilityId(userCart.getAvailabilityId());
			key.setItemId(userCart.getItemId());
			key.setUserId(userCart.getUserId());
			cartIdforDeletion.add(key);
		}
		if (userOrderList.size() > 0) {

			userOrderSet.setTotalAmount(totalAmount);
			userOrderSet.setOrderList(userOrderList);
			userOrderSet.setAddressId(createOrderInput.getAddressId());
			userOrderSet.setOrderAt(LocalDateTime.now());
			userOrderSet.setStatus(new OrderStatus());
			userOrderSet.setCoupenCode(createOrderInput.getCoupenCode());
			userOrderSet.setUserId(createOrderInput.getUserId());
			for (String type : ApplicationProperties.orderConfigType) {
				Object value = offerConfigurationDBService.configureOrderBeforeSend(userOrderSet, type);
				if (type.equals("DLRCRG") && value != null) {
					userOrderSet.setDeliveryCharge(Float.parseFloat(value.toString()));
				}
				if (type.equals("COUPEN") && value != null) {
					userOrderSet.setCoupenDiscount(Float.parseFloat(value.toString()));
				}
			}

			userOrderSet = userOrderSetRepo.save(userOrderSet);
			// set output value
			createOrderOutput.setOrderId(userOrderSet.getId());
			createOrderOutput.setDeliveredBy(userOrderSet.getDeliveredBy());
			createOrderOutput.setTotalAmount(userOrderSet.getTotalAmount());

			userCartRepo.findAndRemove(cartIdforDeletion);
		} else {
			throw new CartItemNotFoundException("Cart items not available");
		}
		return createOrderOutput;
	}
}
