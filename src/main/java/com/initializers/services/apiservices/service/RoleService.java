package com.initializers.services.apiservices.service;

import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.Role;

@Service
public interface RoleService {
	void initializeDefaultRoles();
	Role addNewRole(Role role);
	Role addNewAccessToRole(String roleId, String[] accessId);
	Role removeAccessFromRole(String roleId, String accessId);
	void deleteRole(String roleId);
}
