package com.initializers.services.apiservices.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.elasticsearch.GlobalSearchFactroy;
import com.initializers.services.apiservices.exception.CategoryNotFoundException;
import com.initializers.services.apiservices.exception.SubCategoryNotFoundException;
import com.initializers.services.apiservices.model.elasticsearch.ItemSubCategoryES;
import com.initializers.services.apiservices.model.item.ItemSubCategory;
import com.initializers.services.apiservices.model.temp.ItemDetailsTemp;
import com.initializers.services.apiservices.model.temp.ItemSubCategoryList;
import com.initializers.services.apiservices.others.FilterAction;
import com.initializers.services.apiservices.others.FilterValue;
import com.initializers.services.apiservices.others.QueryString;
import com.initializers.services.apiservices.others.Suggestions;
import com.initializers.services.apiservices.repo.ItemCategoryRepo;
import com.initializers.services.apiservices.repo.ItemSubCategoryRepo;
import com.initializers.services.apiservices.service.ItemCategoryService;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.ItemSubCategoryService;

@Service
public class ItemSubCategoryServiceImpl implements ItemSubCategoryService {

	@Autowired
	private ItemSubCategoryRepo itemSubCategoryRepo;
	@Autowired
	private ItemCategoryRepo itemCategoryRepo;
	@Autowired
	private ItemCategoryService itemCategoryService;
	@Autowired
	private ItemDetailsService itemDetailsService;
	@Autowired
	private GlobalSearchFactroy globalSearchFactory;
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Object> getItemSubCategory(String[] filter, Suggestions suggestions) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		if (suggestions.getKey() != null && suggestions.getValue() != null) {
			List<Object> suggestionsList = new ArrayList<>();
			list.add(Aggregation.project(Fields.fields().and(Fields.field("key", suggestions.getKey()))
					.and(Fields.field("value", suggestions.getValue()))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			suggestionsList = mongoTemplate.aggregate(aggregation, "itemSubCategory", Suggestions.class)
					.getMappedResults().stream().collect(Collectors.toList());
			return suggestionsList;
		} else {
			list.add(LookupOperation.newLookup().from("itemCategory").localField("categoryId").foreignField("_id")
					.as("join_cat"));
			list.add(Aggregation.unwind("join_cat"));
			if (filter != null) {
				List<FilterValue> filterValues = new FilterAction().getFilterValue(filter);
				for (FilterValue filterValue : filterValues) {
					List<Object> alteredFilterValue = new ArrayList<>();
					if (filterValue.getName() != null && filterValue.getOperator() != null
							&& filterValue.getValue() != null) {
						switch (filterValue.getName()) {
						case "categoryId":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						case "_id":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						default:
							alteredFilterValue = filterValue.getValue().stream().collect(Collectors.toList());
							break;
						}
						list.add(Aggregation.match(Criteria.where(filterValue.getName()).in(alteredFilterValue)));
					}
				}
			}
			list.add(Aggregation.project(Fields.fields("_id", "description", "name", "imageLink", "offer")
					.and(Fields.field("categoryId", "join_cat.name"))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			List<ItemSubCategoryList> itemSubCategory = mongoTemplate
					.aggregate(aggregation, "itemSubCategory", ItemSubCategoryList.class).getMappedResults();
			return itemSubCategory.stream().collect(Collectors.toList());
		}
	}

	@Override
	public Object getItemSubCategoryPage(QueryString queryString, Suggestions suggestions, Pageable pageable) {
		Query countQuery = new Query();
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		if (suggestions.getKey() != null && suggestions.getValue() != null) {
			List<Object> suggestionsList = new ArrayList<>();
			list.add(Aggregation.project(Fields.fields().and(Fields.field("key", suggestions.getKey()))
					.and(Fields.field("value", suggestions.getValue()))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			suggestionsList = mongoTemplate.aggregate(aggregation, "itemSubCategory", Suggestions.class)
					.getMappedResults().stream().collect(Collectors.toList());
			return suggestionsList;
		} else {
			list.add(LookupOperation.newLookup().from("itemCategory").localField("categoryId").foreignField("_id")
					.as("join_cat"));
			list.add(Aggregation.unwind("join_cat"));
//			Search
			if(queryString != null && queryString.getSearch() != null) {
				String search = queryString.getSearch();
				List<Object> searchIdList = new ArrayList<>();
				List<ItemSubCategoryES> searchHit = globalSearchFactory.searchItemSubCategory(search);
				searchHit.stream().forEach((hit) -> {
					searchIdList.add(hit.getId());	
				});
				list.add(Aggregation.match(Criteria.where("_id").
						in(searchIdList)));
				countQuery.addCriteria(Criteria.where("_id").
						in(searchIdList));
			}
//			Filter
			if (queryString != null && queryString.getFilter() != null) {
				String[] filter = queryString.getFilter();
				List<FilterValue> filterValues = new FilterAction().getFilterValue(filter);
				for (FilterValue filterValue : filterValues) {
					List<Object> alteredFilterValue = new ArrayList<>();
					if (filterValue.getName() != null && filterValue.getOperator() != null
							&& filterValue.getValue() != null) {
						switch (filterValue.getName()) {
						case "categoryId":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						case "_id":
							alteredFilterValue = filterValue.getValue().stream().map(ObjectId::new)
									.collect(Collectors.toList());
							break;
						default:
							alteredFilterValue = filterValue.getValue().stream().collect(Collectors.toList());
							break;
						}
						list.add(Aggregation.match(Criteria.where(filterValue.getName()).in(alteredFilterValue)));
						countQuery.addCriteria(Criteria.where(filterValue.getName()).in(alteredFilterValue));
					}
				}
			}
			list.add(Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize()));
			list.add(Aggregation.limit(pageable.getPageSize()));
			list.add(Aggregation.project(Fields.fields("_id", "description", "name", "imageLink", "offer")
					.and(Fields.field("categoryId", "join_cat.name"))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			List<ItemSubCategoryList> itemSubCategory = mongoTemplate
					.aggregate(aggregation, "itemSubCategory", ItemSubCategoryList.class).getMappedResults();
			List<Object> resultObject = new ArrayList<>();
			itemSubCategory.stream().forEach((item) -> {
				resultObject.add(item);
			});
			Long totalSize = mongoTemplate.count(countQuery, ItemSubCategory.class);
			Page<Object> page = new PageImpl<>(resultObject, pageable, totalSize);
			return page;
		}
	}

	@Override
	public ItemSubCategory getItemSubCategoryById(ObjectId id) {
		return itemSubCategoryRepo.findFirstById(id);
	}

	@Override
	public Map<String, Object> deleteItemSubCategoryByCategoryId(ObjectId categoryId) {
		Map<String, Object> returnVal = new HashMap<>();
		returnVal.put("Info", "Sub category got deleted by category id");
		itemSubCategoryRepo.deleteByCategoryId(categoryId);
		return returnVal;
	}

	@Override
	public Map<String, Object> deleteItemSubCategoryById(ObjectId subCategoryId) {
		// TODO Auto-generated method stub
		Map<String, Object> returnVal = new HashMap<>();
		returnVal.put("Info", "Sub category deleted by id");
		itemSubCategoryRepo.deleteById(subCategoryId);
		return returnVal;
	}

	@Override
	public boolean checkItemSubCategory(ObjectId itemSubCategoryId) {
		if (itemSubCategoryRepo.findFirstById(itemSubCategoryId) != null) {
			return true;
		}
		return false;
	}

	@Override
	public String getItemSubCategoryName(ObjectId id) {
		ItemSubCategory itemSubCategory = itemSubCategoryRepo.findFirstNameById(id);

		return itemSubCategory == null ? "" : itemSubCategory.getName();
	}

	@Override
	public ItemSubCategory addItemSubCategoryAdmin(ItemSubCategory itemSubCategory) {
		if (itemCategoryRepo.findFirstById(itemSubCategory.getCategoryId()) == null) {
			throw new SubCategoryNotFoundException();
		} else {
			if (!itemCategoryService.checkItemCategory(itemSubCategory.getCategoryId())) {
				throw new CategoryNotFoundException();
			}
			return itemSubCategoryRepo.save(itemSubCategory);
		}
	}

	@Override
	public ItemSubCategory updateSubCategoryAdmin(ItemSubCategory itemSubCategory) {
		ItemSubCategory dbItemSubCategory = itemSubCategoryRepo.findFirstById(itemSubCategory.getId());
		if (dbItemSubCategory == null) {
			throw new SubCategoryNotFoundException();
		} else {
			final String name = itemSubCategory.getName();
			final String description = itemSubCategory.getDescription();
			final ObjectId categoryId = itemSubCategory.getCategoryId();

			if (name != null && !name.equals("")) {
				dbItemSubCategory.setName(name);
			}
			if (description != null && !description.equals("")) {
				dbItemSubCategory.setDescription(description);
			}
			if (categoryId != null && categoryId != dbItemSubCategory.getCategoryId()) {
				if (!itemCategoryService.checkItemCategory(categoryId)) {
					throw new CategoryNotFoundException();
				}
				dbItemSubCategory.setCategoryId(categoryId);
			}
			return itemSubCategoryRepo.save(dbItemSubCategory);
		}
	}

	@Override
	public void updateItemSubCategoryOffer(ObjectId itemId, Long offer) {
		ItemDetailsTemp itemDetails = itemDetailsService.getItemDetails(itemId);
		ItemSubCategory itemSubCategory = getItemSubCategoryById(itemDetails.getSubCategoryId());

		if (itemSubCategory != null) {
			if (itemSubCategory.getOffer() == null) {
				itemSubCategory.setOffer(offer);
			} else {
				if (itemSubCategory.getOffer() < offer) {
					itemSubCategory.setOffer(offer);
				}
			}
			itemSubCategoryRepo.save(itemSubCategory);
		}
//		ong maxOffer = 0L;
//		List<ItemDetails> itemDetails = itemDetailsService.getItemDetailsBySubCategory(subCategoryID);
//		for(ItemDetails item : itemDetails) {
//			ItemAvailability itemAvailability = itemAvailabilityService.getMaxAvailabilityByItemId(item.
//					getId());
//			if(itemAvailability != null) {
//				if(itemAvailability.getDiscount() > maxOffer) {
//					maxOffer = itemAvailability.getDiscount();
//				}
//			}
//		}
//		itemSubCategory.setOffer(maxOffer);
	}

	@Override
	public List<ItemSubCategory> getItemSubCategoryByCategoryId(ObjectId categoryId) {
		return itemSubCategoryRepo.findByCategoryId(categoryId);
	}

	@Override
	public Page<ItemSubCategory> getItemSubCategoryByCategoryId(ObjectId categoryId, Pageable pageable) {
		return itemSubCategoryRepo.findByCategoryId(categoryId, pageable);
	}
}
