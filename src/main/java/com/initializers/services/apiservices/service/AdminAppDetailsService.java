package com.initializers.services.apiservices.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.initializers.services.apiservices.model.AdminAppDetails;
import com.initializers.services.apiservices.model.actionparam.GenerateApp;

@Service
public interface AdminAppDetailsService {
	AdminAppDetails getAdminAppDetails();
	
	String getAppNameById(String appId);
	
	AdminAppDetails getAppNameIconById(String appId);
	
	AdminAppDetails triggerBuildPipeline(GenerateApp generateApp, MultipartFile image);
	
	AdminAppDetails checkBuildStatus(AdminAppDetails adminAppDetails);
}
