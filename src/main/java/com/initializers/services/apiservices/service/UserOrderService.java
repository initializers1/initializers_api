package com.initializers.services.apiservices.service;

import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.UserOrderSet;

@Service
public interface UserOrderService {
	
	Object getAllUserOrderAdmin(String[] filter);
	
	Object getAllUserOrderPageAdmin(String[] filter, Pageable pageable);
	
	Object getUserOrderAdmin(ObjectId id);
	
	Map<String, Object> updateUserOrderSet(UserOrderSet userOrderSet);
	
//	Client
	Page<UserOrderSet> getUserOrderList(ObjectId userId, Pageable pageable);
	
	UserOrderSet getUserOrder(ObjectId id);
}
