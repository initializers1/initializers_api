package com.initializers.services.apiservices.service.impl;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.HomePage;
import com.initializers.services.apiservices.repo.HomePageRepo;
import com.initializers.services.apiservices.service.HomePageService;

@Service
public class HomePageServiceImpl implements HomePageService {
	
	@Autowired
	HomePageRepo homePageRepo;
	
	@Override
	public Page<HomePage> getHomePage(Integer first, Integer after) {
		Pageable page = PageRequest.of(after, first, Sort.by(Sort.Direction.ASC, "_id"));
		return homePageRepo.findAll(page);
	}

	@Override
	public List<HomePage> getHomePage() {
		return homePageRepo.findAll();
	}
	@Override
	public HomePage addHomePage(HomePage homePage) {
		return homePageRepo.save(homePage);
	}

	@Override
	public HomePage updateHomePage(HomePage homePage) {
		HomePage dbHomePage = homePageRepo.findFirstById(homePage.getId());
		if(homePage.getItemType() != null && !homePage.getItemType().equals("")) {
			dbHomePage.setItemType(homePage.getItemType());
		}
		if(homePage.getWidget() != null && !homePage.getWidget().equals("")) {
			dbHomePage.setWidget(homePage.getWidget());
		}
		if(homePage.getName() != null && !homePage.getName().equals("")) {
			dbHomePage.setName(homePage.getName());
		}
		if(homePage.getTypeId() != null) {
			dbHomePage.setTypeId(homePage.getTypeId());
		}
		return homePageRepo.save(dbHomePage);
	}

	@Override
	public HomePage getHomePageById(ObjectId id) {
		return homePageRepo.findFirstById(id);
	}

	@Override
	public void deleteHomePage(ObjectId id) {
		homePageRepo.deleteById(id);
	}

}
