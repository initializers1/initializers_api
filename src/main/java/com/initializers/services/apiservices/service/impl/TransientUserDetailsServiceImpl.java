package com.initializers.services.apiservices.service.impl;

import java.io.IOException;

import javax.mail.MessagingException;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.constant.MailTemplete;
import com.initializers.services.apiservices.constant.Scope;
import com.initializers.services.apiservices.exception.DuplicateAppException;
import com.initializers.services.apiservices.exception.EmailExistException;
import com.initializers.services.apiservices.exception.EmailIDException;
import com.initializers.services.apiservices.exception.RequiredValueMissingException;
import com.initializers.services.apiservices.exception.TenantNotFoundException;
import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.TransientUserDetails;
import com.initializers.services.apiservices.model.UserOTP;
import com.initializers.services.apiservices.others.AppIdGenerator;
import com.initializers.services.apiservices.others.ApplicationProperties;
import com.initializers.services.apiservices.repo.AdminClientDetailsRepo;
import com.initializers.services.apiservices.repo.TransientUserDetailsRepo;
import com.initializers.services.apiservices.security.CheckInputValue;
import com.initializers.services.apiservices.service.AdminAppDetailsService;
import com.initializers.services.apiservices.service.MailService;
import com.initializers.services.apiservices.service.TransientUserDetailsService;
import com.initializers.services.apiservices.service.UserOTPService;

@Service
public class TransientUserDetailsServiceImpl implements TransientUserDetailsService {

	@Autowired
	private TransientUserDetailsRepo transientUserDetailsRepo;
	@Autowired
	private UserOTPService userOTPService;
	@Autowired
	private AdminClientDetailsRepo adminClientDetailsRepo;
	@Autowired
	private AdminAppDetailsService adminAppDetailsService;
	@Autowired
	private MailService mailservice;
	@Value("${initializers.app.package}")
	private String packageName;
	Logger logger = LoggerFactory.getLogger(TransientUserDetailsServiceImpl.class);

	@Override
	public ObjectId addUser(TransientUserDetails transientUserDetails) {
		Scope scope = Scope.ADMIN;
		String appName = ApplicationProperties.app_name;

//		Input Validation 
		CheckInputValue.checkPassword(transientUserDetails.getPassword());
		CheckInputValue.checkEmail(transientUserDetails.getEmail());
		transientUserDetails.setAppId(CheckInputValue.checkFreeTextFields(transientUserDetails.getAppId()));
		transientUserDetails.setAppName(CheckInputValue.checkFreeTextFields(transientUserDetails.getAppName()));
		transientUserDetails.setFirstName(CheckInputValue.checkFreeTextFields(transientUserDetails.getFirstName()));
		transientUserDetails.setLastName(CheckInputValue.checkFreeTextFields(transientUserDetails.getLastName()));

		try {
//			always store email in lower case
			transientUserDetails.setEmail(transientUserDetails.getEmail().toLowerCase());
//			TODO : check mandatory info
			if (transientUserDetails.getAppId() == null) {
				transientUserDetails
						.setAppId(generateAppId(transientUserDetails.getAppName(), transientUserDetails.getEmail()));
			} else {
				scope = Scope.CLIENT;
				appName = adminAppDetailsService.getAppNameById(transientUserDetails.getAppId());
				if (appName == null)
					throw new TenantNotFoundException();
				transientUserDetails.setAppName(appName);
			}
			boolean noSuchEmail = checkEmail(transientUserDetails.getEmail(), transientUserDetails.getAppId());
			if (noSuchEmail) {
				transientUserDetails.setScope(scope);
				TransientUserDetails userSavedData = transientUserDetailsRepo.save(transientUserDetails);
				UserOTP userOTP = new UserOTP();
				userOTP.setId(transientUserDetails.getId());
				userOTPService.addUserOTP(userOTP);
				try {
					mailservice.sendEmail("User", transientUserDetails.getEmail(), userOTP.getOtp(), appName, scope,
							MailTemplete.RegisterUser);
				} catch (MessagingException | IOException e) {
					logger.error(e + "unable to send email to " + transientUserDetails.getEmail());
					throw new EmailIDException();
				}
				return userSavedData.getId();
			} else {
				throw new EmailExistException();
			}

		} catch (IllegalArgumentException e) {
			logger.error(e + "transient user details data is incorrect" + transientUserDetails.getEmail());
			throw new RequiredValueMissingException();
		}
	}

	@Override
	public TransientUserDetails getUser(ObjectId userId) {
		return transientUserDetailsRepo.findFirstById(userId);
	}

	@Override
	public void deleteUser(ObjectId userId) {
		transientUserDetailsRepo.deleteById(userId);
	}

	// check if email exist in persistent user details
	public boolean checkEmail(String email, String appId) {
		AdminClientDetails.CompositeKeyAdminClient key = new AdminClientDetails.CompositeKeyAdminClient();
		key.setAppId(appId);
		key.setEmail(email);
		if (adminClientDetailsRepo.findFirstById(key) != null) {
			return false;
		} else {
			return true;
		}
		
	}

//	similar ref in AdminAppDetailsServiceImpl
	public String generateAppId(String appName, String userName) {
		if (appName == null)
			throw new RequiredValueMissingException();
//		String bias = RandomStringUtils.random(5, true, true).toLowerCase();
		String bias = AppIdGenerator.getBias(userName);
		appName = appName.trim().replace(" ", "").toLowerCase();
		String appId = packageName + "." + appName + bias;
		if ((transientUserDetailsRepo.findFirstIdByAppId(appName) != null)
				|| (adminClientDetailsRepo.findFirstByIdAppId(appId) != null)) {
			throw new DuplicateAppException();
		}
		return appId;
	}
}
