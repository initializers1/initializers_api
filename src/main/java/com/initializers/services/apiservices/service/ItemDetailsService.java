package com.initializers.services.apiservices.service;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.actionResult.ChangeStatusResult;
import com.initializers.services.apiservices.model.actionparam.ChangeStatus;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.model.temp.ItemDetailsTemp;
import com.initializers.services.apiservices.others.QueryString;
import com.initializers.services.apiservices.others.Suggestions;

@Service
public interface ItemDetailsService {
	
	Object addItemDetailsAdmin(ItemDetails itemDetails);
	
	ItemDetailsTemp getItemDetails(ObjectId id);
	
	Object getAdminItemDetails(ObjectId id);
	
	List<Object> getItemDetailsList(String[] filter, Suggestions suggestions);
	
	Page<Object> getItemDetailsPage(QueryString queryString, Suggestions suggestions, Pageable pageable);
	
	Object updateItemDetailsAdmin(ItemDetails itemDetails);
	
	String getItemNameById(ObjectId id);

	ItemDetails getItemListById(ObjectId itemId, ObjectId... availabilityId);
	
	List<ItemDetails> getItemDetailsBySubCategory(ObjectId subCategoryID);
	
	List<ChangeStatusResult> changeStatus(ChangeStatus changeStatus);
	
//	Client
	
	ItemDetails getItemDetails(ObjectId userId, ObjectId itemId);
	
	Page<ItemDetails> getItemDetailsByType(ObjectId categoryID, Pageable pageable, String type);
	
	Boolean checkItemDetails(ObjectId id);
	
	ItemDetails getItemNameImageLinkById(ObjectId id);
	
}
