package com.initializers.services.apiservices.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.exception.ItemNotFoundException;
import com.initializers.services.apiservices.exception.UserNotFoundException;
import com.initializers.services.apiservices.model.UserWishlist;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.repo.UserWishlistRepo;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.UserDetailsService;
import com.initializers.services.apiservices.service.UserWishlistService;

@Service
public class UserWishlistServiceImpl implements UserWishlistService {

	@Autowired
	private UserWishlistRepo userWishListRepo;
	@Autowired
	private ItemDetailsService itemDetailsService;
	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	public UserWishlist addUserWishlist(ObjectId userID, ObjectId itemID) {
		UserWishlist dbUserWishlist = userWishListRepo.findFirstByUserID(userID);

		if (dbUserWishlist == null) {
			dbUserWishlist = new UserWishlist();
//			if (!userDetailsService.checkUser(userID)) {
//				throw new UserNotFoundException("User Not found");
//			}
			dbUserWishlist.setUserID(userID);
		}
			dbUserWishlist.setItemsId(itemID);			
//		if (!itemDetailsService.checkItemDetails(itemID)) {
//			throw new ItemNotFoundException("Item not found");
//		}
		userWishListRepo.save(dbUserWishlist);
		return dbUserWishlist;
	}

	@Override
	public Map<String, Object> getUserWishlist(ObjectId userID, Pageable page) {
		Map<String, Object> returnVal = new HashMap<>();
		UserWishlist userWishlist = userWishListRepo.findFirstByUserID(userID);
		
		int after = page.getPageNumber();
		int pageSize = page.getPageSize();
		int start = after;

		int count = 0;
		if (userWishlist == null) {
			throw new UserNotFoundException("User Not found");
		}
		Set<ObjectId> itemsId = userWishlist.getItemsId();
		List<ObjectId> itemsIdList = new ArrayList<>(itemsId);
		List<ItemDetails> itemDetailsList = new ArrayList<ItemDetails>();
		System.out.println("size" + itemsIdList.size());
		//should fetch given pagesize and check if 
		while((start < itemsIdList.size()) && (count < pageSize)) {
			ItemDetails itemDetails = itemDetailsService.getItemListById(itemsIdList.get(start));
			start++;
			if (itemDetails != null) {
				itemDetails.setIsWishlist(true);
				itemDetailsList.add(itemDetails);
				count++;
			}
		}
		returnVal.put("endCursor", start);
		if(start >= itemsIdList.size()) {
			returnVal.put("hasNextPage", false);
		} else {
			returnVal.put("hasNextPage", true);
		}
		returnVal.put("page", new PageImpl<>(itemDetailsList, page, itemsId.size()));
		return returnVal;
	}

	@Override
	public Object deleteUserWishlist(ObjectId userID, ObjectId itemID) {
		UserWishlist userWishlist = userWishListRepo.findFirstByUserID(userID);
		if (userWishlist != null) {
			userWishlist.getItemsId().remove(itemID);
			userWishListRepo.save(userWishlist);
			return itemID;
		}
		return null;
	}

	@Override
	public UserWishlist getUserWishlist(ObjectId userID) {
		return userWishListRepo.findFirstByUserID(userID);
	}
}
