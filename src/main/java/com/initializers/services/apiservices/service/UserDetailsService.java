package com.initializers.services.apiservices.service;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.UserDetails;

@Service
public interface UserDetailsService {
	

	List<UserDetails> getAllUsers();
	
	ObjectId addUser(UserDetails userDetails);
	
	UserDetails getUser(ObjectId userId);
	
	Object deleteUser(ObjectId userId);
	
	UserDetails updateUserDetail(UserDetails userDetails);
	
	String getNameById(ObjectId userId);
	
	Boolean checkUser(ObjectId userId);
	
}
