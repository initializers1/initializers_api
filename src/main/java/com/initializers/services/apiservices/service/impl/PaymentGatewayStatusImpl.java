package com.initializers.services.apiservices.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.repo.PaymentGatewayStatusRepo;
import com.initializers.services.apiservices.service.PaymentGatewayStatusService;
import com.initializers.services.apiservices.model.payment.PaymentGatewayStatus;

@Service
public class PaymentGatewayStatusImpl implements PaymentGatewayStatusService {

	@Autowired
	private PaymentGatewayStatusRepo PaymentGatewayStatusRepo;

	@Override
	public PaymentGatewayStatus getStatusByGateway(ObjectId gatewayID, String gatewayStatus) {
		return PaymentGatewayStatusRepo.findByGatewayIDAndGatewayStatus(gatewayID, gatewayStatus);
	}

}
