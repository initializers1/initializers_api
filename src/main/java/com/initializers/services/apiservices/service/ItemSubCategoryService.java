package com.initializers.services.apiservices.service;

import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.item.ItemSubCategory;
import com.initializers.services.apiservices.others.QueryString;
import com.initializers.services.apiservices.others.Suggestions;

@Service
public interface ItemSubCategoryService {

	
	ItemSubCategory addItemSubCategoryAdmin(ItemSubCategory itemSubCategory);
	
	List<Object> getItemSubCategory(String[] filter, Suggestions suggestions);
	
	Object getItemSubCategoryPage(QueryString queryString, Suggestions suggestions, Pageable pageable);
	
	ItemSubCategory getItemSubCategoryById(ObjectId id);
	
	String getItemSubCategoryName(ObjectId id);
	
	Map<String, Object> deleteItemSubCategoryByCategoryId(ObjectId categoryId);
	
	Map<String, Object> deleteItemSubCategoryById(ObjectId subCategoryId);
	
	ItemSubCategory updateSubCategoryAdmin(ItemSubCategory itemSubCategory);
		
	boolean checkItemSubCategory(ObjectId itemSubCategoryId);
	
	void updateItemSubCategoryOffer(ObjectId itemId, Long offer);
	
	List<ItemSubCategory> getItemSubCategoryByCategoryId(ObjectId categoryId);
	
//	client 
	Page<ItemSubCategory> getItemSubCategoryByCategoryId(ObjectId categoryId, Pageable pageable);
	
}
