package com.initializers.services.apiservices.service;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.HomePage;

@Service
public interface HomePageService {
	
	Page<HomePage> getHomePage(Integer first, Integer after);
	
	List<HomePage> getHomePage();
	
	HomePage addHomePage(HomePage homePage);
	
	HomePage updateHomePage(HomePage homePage);
	
	HomePage getHomePageById(ObjectId id);
	
	void deleteHomePage(ObjectId id);
}
