package com.initializers.services.apiservices.service;

import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.payment.AdminPayementGatewayKey;

@Service
public interface AdminPayementGatewayKeyService {
	
	Page<AdminPayementGatewayKey> getAdminPayementGatewayKey(Pageable pageable);
	
	AdminPayementGatewayKey getAdminPayementGatewayKeyById(ObjectId adminPayementGatewayKeyId);
	
	AdminPayementGatewayKey addAdminPayementGatewayKey(AdminPayementGatewayKey adminPayementGatewayKey);

	AdminPayementGatewayKey updateAdminPayementGatewayKey(AdminPayementGatewayKey adminPayementGatewayKey);
	
	Map<String, Object> deleteAdminPayementGatewayKeyById(ObjectId adminPayementGatewayKeyId);
	
}
