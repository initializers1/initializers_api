package com.initializers.services.apiservices.service.impl;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.initializers.services.apiservices.exception.RequiredValueMissingException;
import com.initializers.services.apiservices.model.payment.UserPayment;
import com.initializers.services.apiservices.service.UserPaymentService;
import com.initializers.services.apiservices.repo.PaymentGatewayStatusRepo;
import com.initializers.services.apiservices.repo.UserPaymentRepo;

@Service
public class UserPaymentServiceImpl implements UserPaymentService {

	@Autowired
	private UserPaymentRepo userPaymentRepo;
	@Autowired
	private PaymentGatewayStatusRepo paymentGatewayStatusRepo;

	@Override
	public List<UserPayment> getUserPayment() {
		return userPaymentRepo.findAll();
	}

	@Override
	public UserPayment getUserPaymentByTransactionID(ObjectId orderId) {
		if (orderId == null) {
			throw new RequiredValueMissingException();
		} else {
			return userPaymentRepo.findById(orderId);
		}
	}

	@Override
	public UserPayment upadateUserPayment(UserPayment userPayment) {
		if (userPayment.getAmount() == null) {
			throw new RequiredValueMissingException();
		} else {
			return userPaymentRepo.save(userPayment);
		}
	}

	@Override
	public UserPayment addUserPayment(UserPayment userPayment) {
		if (userPayment.getAmount() == null) {
			throw new RequiredValueMissingException();
		}   else {
			return userPaymentRepo.save(userPayment);
		}
	}

	@Override
	public void updateUserPaymentStatus(String externalOrderId, String gatewayName, String status, String failureReason) {
		List<UserPayment> userPaymentList = userPaymentRepo.findListByexternalOrderId(externalOrderId);
		if(userPaymentList.size() == 1) {
			UserPayment userPayment = userPaymentList.get(0);
			char internalStatus = paymentGatewayStatusRepo.getInternalStatusByGatewayName(gatewayName, status);
			userPayment.setStatus(internalStatus);
			if(failureReason != null) {
				userPayment.setFailureMessage(failureReason);
			}
			userPaymentRepo.save(userPayment);
		} else {
//			throw exception for duplicate external id, this should never happen 
		}
	}

}