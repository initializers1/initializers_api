package com.initializers.services.apiservices.service;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public interface AuthenticationFacade {
	Authentication getAuthentication();
}
