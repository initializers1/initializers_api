package com.initializers.services.apiservices.service;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.AdminDetails;

@Service
public interface AdminDetailsService {
	
	AdminDetails addAdminDetails(AdminDetails adminDetails);
	
	AdminDetails getAdminDetailsById(ObjectId id);
}
