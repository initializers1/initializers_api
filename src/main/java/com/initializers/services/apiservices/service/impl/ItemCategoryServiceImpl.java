package com.initializers.services.apiservices.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.elasticsearch.GlobalSearchFactroy;
import com.initializers.services.apiservices.exception.CategoryNotFoundException;
import com.initializers.services.apiservices.model.elasticsearch.ItemCategoryES;
import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.model.temp.ItemDetailsTemp;
import com.initializers.services.apiservices.others.FilterAction;
import com.initializers.services.apiservices.others.FilterValue;
import com.initializers.services.apiservices.others.QueryString;
import com.initializers.services.apiservices.others.Suggestions;
import com.initializers.services.apiservices.repo.ItemCategoryRepo;
import com.initializers.services.apiservices.service.ItemCategoryService;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.ItemSubCategoryService;

@Service
public class ItemCategoryServiceImpl implements ItemCategoryService {

	@Autowired 
	private ItemCategoryRepo itemCategoryRepo;
	@Autowired
	private ItemSubCategoryService itemSubCategoryService;
	@Autowired
	private ItemDetailsService itemDetailsService;
	@Autowired
	private GlobalSearchFactroy globalSearchFactory;
	@Autowired
	private MongoTemplate mongoTemplate;
	
	
	@Override
	public ItemCategory getItemCategoryById(ObjectId id) {
		ItemCategory itemCategory = itemCategoryRepo.findFirstById(id);
		if(itemCategory == null) {
			throw new CategoryNotFoundException();
		}else {
			return itemCategory;
		}
	}

	@Override
	public Map<String, Object> deleteItemCategoryById(ObjectId categoryId) {
		Map<String, Object> returnVal = new HashMap<>();
		returnVal.put("Info", "Category deleted by id");
		itemCategoryRepo.deleteById(categoryId);
		itemSubCategoryService.deleteItemSubCategoryByCategoryId(categoryId);
		return returnVal;
	}

	@Override
	public boolean checkItemCategory(ObjectId itemCategoryId) {
		if(itemCategoryRepo.findFirstById(itemCategoryId) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void updateItemCategoryOffer(ObjectId itemId,Long offer) {
		ItemDetailsTemp itemDetails = itemDetailsService.getItemDetails(itemId);
		ItemCategory itemCategory = getItemCategoryById(itemDetails.getCategoryId());
		 
		if(itemCategory != null) {
			if(itemCategory.getOffer() == null) {
				itemCategory.setOffer(offer);
			}else {
				if(itemCategory.getOffer() < offer) {
					itemCategory.setOffer(offer);
				}
			}
			itemCategoryRepo.save(itemCategory);
		}
	}

	@Override
	public Object getItemCategoryPage(QueryString queryString, Suggestions suggestions, Pageable pageable) {
		Query countQuery = new Query();
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		if(suggestions.getKey() != null && suggestions.getValue() != null) {
			List<Object> suggestionsList = new ArrayList<>();
			list.add(Aggregation.project(Fields.fields().
					and(Fields.field("key", suggestions.getKey())).
					and(Fields.field("value", suggestions.getValue()))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			suggestionsList = mongoTemplate.aggregate(aggregation, "itemCategory",
					Suggestions.class)
					.getMappedResults().stream().collect(Collectors.toList());
			return suggestionsList;
		}else {
			if(queryString != null && queryString.getSearch() != null) {
				String search = queryString.getSearch();
				List<Object> searchIdList = new ArrayList<>();
				List<ItemCategoryES> searchHit = globalSearchFactory.searchItemCategory(search);
				searchHit.stream().forEach((hit) -> {
					searchIdList.add(hit.getId());	
				});
				list.add(Aggregation.match(Criteria.where("_id").
						in(searchIdList)));
				countQuery.addCriteria(Criteria.where("_id").
						in(searchIdList));
			}
			if(queryString != null && queryString.getFilter() != null) {
				String[] filter = queryString.getFilter();
				List<FilterValue> filterValues =  new FilterAction().getFilterValue(filter);
				for(FilterValue filterValue : filterValues) {
					List<Object> alteredFilterValue = new ArrayList<>();
					if(filterValue.getName() != null && filterValue.getOperator() != null 
							&& filterValue.getValue() != null) {
						switch (filterValue.getName()) {
						case "_id":
							alteredFilterValue = filterValue.getValue().stream().map(Long::parseLong).
							collect(Collectors.toList());
							break;
						default:
							alteredFilterValue = filterValue.getValue().stream().
							collect(Collectors.toList());
							break;
						}
						list.add(Aggregation.match( Criteria.where(filterValue.getName()).
								in(alteredFilterValue)));
						countQuery.addCriteria(Criteria.where(filterValue.getName()).
								in(alteredFilterValue));
					}
				}
			}
			list.add(Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize()));
			list.add(Aggregation.limit(pageable.getPageSize()));
			list.add(Aggregation.project(Fields.fields("_id","name","description","imageLink")));
			Aggregation aggregation = Aggregation.newAggregation(list);
			List<ItemCategory> itemCategory = mongoTemplate.aggregate(aggregation, "itemCategory", ItemCategory.class)
					.getMappedResults();
			List<Object> resultObject = new ArrayList<>();
			itemCategory.stream().forEach((item) -> {
				resultObject.add(item);
			});
			Long totalSize = mongoTemplate.count(countQuery, ItemCategory.class);
			Page<Object> page = new PageImpl<>(resultObject, pageable, totalSize);
			return page;
		}
	}
	
	@Override
	public Object getItemCategory(String[] filter, Suggestions suggestions) {
		List<Object> itemCategoryList = new ArrayList<>();
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		if(suggestions.getKey() != null && suggestions.getValue() != null) {
			List<Object> suggestionsList = new ArrayList<>();
			list.add(Aggregation.project(Fields.fields().
					and(Fields.field("key", suggestions.getKey())).
					and(Fields.field("value", suggestions.getValue()))));
			Aggregation aggregation = Aggregation.newAggregation(list);
			suggestionsList = mongoTemplate.aggregate(aggregation, "itemCategory",
					Suggestions.class)
					.getMappedResults().stream().collect(Collectors.toList());
			return suggestionsList;
		}else {
			if(filter != null) {
				List<FilterValue> filterValues =  new FilterAction().getFilterValue(filter);
				for(FilterValue filterValue : filterValues) {
					List<Object> alteredFilterValue = new ArrayList<>();
					if(filterValue.getName() != null && filterValue.getOperator() != null 
							&& filterValue.getValue() != null) {
						switch (filterValue.getName()) {
						case "_id":
							alteredFilterValue = filterValue.getValue().stream().map(Long::parseLong).
							collect(Collectors.toList());
							break;
						default:
							alteredFilterValue = filterValue.getValue().stream().
							collect(Collectors.toList());
							break;
						}
						list.add(Aggregation.match( Criteria.where(filterValue.getName()).
								in(alteredFilterValue)));
					}
				}
			}
			list.add(Aggregation.project(Fields.fields("_id","name","description","imageLink")));
			Aggregation aggregation = Aggregation.newAggregation(list);
			List<ItemCategory> itemCategory = mongoTemplate.aggregate(aggregation, "itemCategory", ItemCategory.class)
					.getMappedResults();
			for(ItemCategory item : itemCategory) {
				Map<String, Object> returnVal = new HashMap<>();
				returnVal.put("id", item.getId().toHexString());
				returnVal.put("name", item.getName());
				returnVal.put("description", item.getDescription());
				returnVal.put("imageLink", item.getImageLink());
				itemCategoryList.add(returnVal);
			};
			return itemCategoryList;
		}
	}

	@Override
	public String getItemCategoryName(ObjectId id) {
		ItemCategory itemCategory = itemCategoryRepo.findFirstNameById(id);
		
		return itemCategory == null?
				"":
					itemCategory.getName();
	}

	@Override
	public ItemCategory updateCategoryAdmin(ItemCategory itemCategory) {
		ItemCategory dbItemCategory = itemCategoryRepo.findFirstById(itemCategory.getId());
		if(dbItemCategory == null) {
			throw new CategoryNotFoundException();
		}else {
			final String name = itemCategory.getName();
			final String description = itemCategory.getDescription();
			
			if(name != null && !name.equals("")) {
				dbItemCategory.setName(name);
			}
			if(description != null && !description.equals("")) {
				dbItemCategory.setDescription(description);
			}
			return itemCategoryRepo.save(dbItemCategory);
		}
	}

	@Override
	public ItemCategory addItemCategoryAdmin(ItemCategory itemCategory) {
		itemCategory.setId(null);
		return itemCategoryRepo.save(itemCategory);
	}
	@Override
	public List<ItemCategory> getItemCategory() {
		return itemCategoryRepo.findAll();
	}
	@Override
	public Page<ItemCategory> getItemCategory(Integer first, Integer after) {
		Pageable page = PageRequest.of(after, first);
		return itemCategoryRepo.findAll(page);
	}
	@Override
	public List<ItemCategory> getItemCategoryContentById(ObjectId CategoryId) {
		return itemCategoryRepo.findAll();
	}
}
