package com.initializers.services.apiservices.service;

import org.springframework.stereotype.Service;

@Service
public interface AdminClientExternalIdRefSevice {
	public void addExternalIdRef(String externalOrderId);
	public void deleteExternalIdRef(String externalOrderId);
	public String getAppIdForExternalIdRef(String externalOrderId);
}
