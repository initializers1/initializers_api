package com.initializers.services.apiservices.service;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.item.ItemAvailability;

@Service
public interface ItemAvailabilityService {
	
	List<ItemAvailability> getAvailabilityByItemId(ObjectId itemId, char... availability);
	
	ItemAvailability addAvailability(ItemAvailability itemAvailability);
	
	ItemAvailability getAvailabilityById(ObjectId id, ObjectId itemId);
	
	ItemAvailability getAvailabilityByIdAdmin(ObjectId id);
	
	ItemAvailability updateAvailability(ItemAvailability itemAvailability);
	
	Float getPriceByAvailabilityId(ObjectId id);
	
	StringBuilder getValueUnitByAvailabilityId(ObjectId id);
}
