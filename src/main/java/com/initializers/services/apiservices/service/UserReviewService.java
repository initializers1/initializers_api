package com.initializers.services.apiservices.service;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.UserReview;
import com.initializers.services.apiservices.model.output.UserReviewOutput;
import com.initializers.services.apiservices.model.temp.UserReviewTemp;

@Service
public interface UserReviewService {
	
	UserReview addUserReview(UserReviewTemp userReviewTemp);
	
	Page<UserReviewOutput> findReviewByItemId(ObjectId itemId, Pageable pageable); 
}
