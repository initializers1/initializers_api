package com.initializers.services.apiservices.service;

import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.others.QueryString;
import com.initializers.services.apiservices.others.Suggestions;

@Service
public interface ItemCategoryService {
	
	ItemCategory addItemCategoryAdmin(ItemCategory itemCategory);
	
	Object getItemCategory(String[] filter, Suggestions suggestions);
	
	Object getItemCategoryPage(QueryString queryString, Suggestions suggestions, Pageable pageable);
	
	String getItemCategoryName(ObjectId id);
	
	ItemCategory getItemCategoryById(ObjectId id);
	
	ItemCategory updateCategoryAdmin(ItemCategory itemCategory);
	
	Map<String, Object> deleteItemCategoryById(ObjectId categoryId);
	
	boolean checkItemCategory(ObjectId itemCategoryId);
	
	void updateItemCategoryOffer(ObjectId itemId, Long offer);
	
//	client
	
	List<ItemCategory> getItemCategory();
	
	Page<ItemCategory> getItemCategory(Integer first, Integer after);
	
	List<ItemCategory> getItemCategoryContentById(ObjectId CategoryId);
	
}
