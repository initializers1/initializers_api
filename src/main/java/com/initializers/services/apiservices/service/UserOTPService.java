package com.initializers.services.apiservices.service;

import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.UserOTP;

@Service
public interface UserOTPService {
	
	void addUserOTP(UserOTP userOTP);
	
	AdminClientDetails validateUserOTP(UserOTP userOTP);

	AdminClientDetails pushUserDetails(ObjectId userId);
	
	void deleteUserOTP(ObjectId userId);
	
	Map<String,Object> regenerateUserOTP(ObjectId userId);
}
