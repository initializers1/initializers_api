package com.initializers.services.apiservices.service;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.payment.PaymentGatewayStatus;

@Service
public interface PaymentGatewayStatusService {
	
	PaymentGatewayStatus getStatusByGateway(ObjectId gateWayID, String gatewayStatus);

}
