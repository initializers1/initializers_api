package com.initializers.services.apiservices.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.constant.MailTemplete;
import com.initializers.services.apiservices.constant.Scope;
import com.initializers.services.apiservices.exception.InternalIssueException;
import com.initializers.services.apiservices.exception.OTPExpiredException;
import com.initializers.services.apiservices.exception.OTPNotExpiredException;
import com.initializers.services.apiservices.exception.OTPNotValidException;
import com.initializers.services.apiservices.exception.UserNotFoundException;
import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.TransientUserDetails;
import com.initializers.services.apiservices.model.UserOTP;
import com.initializers.services.apiservices.others.ApplicationProperties;
import com.initializers.services.apiservices.repo.UserOTPRepo;
import com.initializers.services.apiservices.service.AdminClientDetailsService;
import com.initializers.services.apiservices.service.MailService;
import com.initializers.services.apiservices.service.TransientUserDetailsService;
import com.initializers.services.apiservices.service.UserOTPService;

@Service
public class UserOTPServiceImpl implements UserOTPService {

	@Autowired
	private UserOTPRepo userOTPRepo;
	@Autowired
	private TransientUserDetailsService transientUserDetailsService;
	@Autowired
	private AdminClientDetailsService adminClientDetailsService;
	@Autowired
	private MailService mailservice;

	Logger logger = LoggerFactory.getLogger(UserOTPServiceImpl.class);

	@Override
	public void addUserOTP(UserOTP userOTP) {
		// TODO Auto-generated method stub
		userOTPRepo.save(userOTP);
	}

	@Override
	public AdminClientDetails validateUserOTP(UserOTP paramUserOTP) {
		UserOTP dbUserOTP = userOTPRepo.findFirstById(paramUserOTP.getId());
		Date currentTime = new Date();
		if (dbUserOTP == null) {
			throw new UserNotFoundException();
		} else {
			if (currentTime.after(dbUserOTP.getExpiryTime())) {
				regenerateUserOTP(dbUserOTP.getId());
				throw new OTPExpiredException();
			} else {
				if (dbUserOTP.getOtp().equals(paramUserOTP.getOtp())) {
					AdminClientDetails adminClientDetails = pushUserDetails(paramUserOTP.getId());
					transientUserDetailsService.deleteUser(paramUserOTP.getId());
					deleteUserOTP(paramUserOTP.getId());
					return adminClientDetails;
				} else {
					throw new OTPNotValidException();
				}
			}
		}
	}

	@Override
	public AdminClientDetails pushUserDetails(ObjectId userId) {
		TransientUserDetails transientUserDetails = transientUserDetailsService.getUser(userId);
		if (transientUserDetails == null) {
			throw new UserNotFoundException();
		} else {
			AdminClientDetails adminClientDetails = adminClientDetailsService.createTenantDetails(transientUserDetails);
			if (adminClientDetails == null) {
				throw new InternalIssueException();
			}
			return adminClientDetails;
		}
	}

	@Override
	public void deleteUserOTP(ObjectId userId) {
		userOTPRepo.deleteById(userId);
	}

	@Override
	public Map<String, Object> regenerateUserOTP(ObjectId userId) {
		Map<String, Object> returnVal = new HashMap<String, Object>();
		UserOTP oldUserOTP = userOTPRepo.findFirstById(userId);
		if (oldUserOTP != null && new Date().before(oldUserOTP.getExpiryTime())) {
			throw new OTPNotExpiredException();
		}
		UserOTP userOTP = new UserOTP();
		TransientUserDetails transientUserDetails = transientUserDetailsService.getUser(userId);
		if (transientUserDetails == null) {
			throw new UserNotFoundException();
		} else {
			String appName = transientUserDetails.getScope() == Scope.CLIENT ? transientUserDetails.getAppName()
					: ApplicationProperties.app_name;
			userOTP.setId(userId);
			addUserOTP(userOTP);
			try {
				mailservice.sendEmail(transientUserDetails.getFirstName() + " " + transientUserDetails.getLastName(),
						transientUserDetails.getEmail(), userOTP.getOtp(), appName, transientUserDetails.getScope(),
						MailTemplete.RegenerateOTP);
			} catch (MessagingException | IOException e) {
				e.printStackTrace();
			}
			returnVal.put("Info", "user OTP resent");
			return returnVal;
		}
	}
}
