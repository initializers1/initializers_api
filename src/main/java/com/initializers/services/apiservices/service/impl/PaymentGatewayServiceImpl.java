package com.initializers.services.apiservices.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.constant.PaymentConstants;
import com.initializers.services.apiservices.exception.PayementGatewayNotFoundException;
import com.initializers.services.apiservices.exception.RequiredValueMissingException;
import com.initializers.services.apiservices.model.payment.PaymentGateway;
import com.initializers.services.apiservices.payment.PaymentAbstract;
import com.initializers.services.apiservices.payment.PaymentGatewayFactory;
import com.initializers.services.apiservices.repo.PaymentGatewayRepo;
import com.initializers.services.apiservices.service.PaymentGatewayService;

@Service
public class PaymentGatewayServiceImpl implements PaymentGatewayService {

	@Autowired
	private PaymentGatewayRepo paymentGatewayRepo;
	@Autowired
	private PaymentGatewayFactory paymentGatewayFactory;

	@Override
	public List<PaymentGateway> getPaymentGateway() {

		if (paymentGatewayRepo.findAll().size() < 0) {
			throw new PayementGatewayNotFoundException();
		} else {
			return paymentGatewayRepo.findAll();
		}
	}

	@Override
	public PaymentGateway addPaymentGateway(PaymentGateway paymentGateway) {
		if (paymentGateway.getName() == null) {
			throw new RequiredValueMissingException();
		} else if (paymentGateway.getClassName() == null) {
			throw new RequiredValueMissingException();
		} else {
			/*
			 * Configure web-hook
			 */
			PaymentAbstract paymentAbstract = paymentGatewayFactory
					.getPaymentAbstractInstance(PaymentConstants.StripeClass, null, null);
			paymentAbstract.configureWebHook();

			return paymentGatewayRepo.save(paymentGateway);
		}

	}

	@Override
	public PaymentGateway getPaymentGatewayById(ObjectId gatewayId) {
		return paymentGatewayRepo.findFirstById(gatewayId);
	}

	@Override
	public Map<String, Object> deletePaymentGatewayById(ObjectId gatewayId) {
		Map<String, Object> returnVal = new HashMap<>();
		returnVal.put("Info", "Payment Gateway Deleted By Id");
		paymentGatewayRepo.deleteById(gatewayId);
		return returnVal;
	}

}
