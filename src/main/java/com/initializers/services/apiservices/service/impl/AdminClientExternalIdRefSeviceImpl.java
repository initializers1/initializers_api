package com.initializers.services.apiservices.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.AdminClientExternalIdRef;
import com.initializers.services.apiservices.model.AdminClientExternalIdRef.CompositeKeyExtrnalId;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.repo.AdminClientExternalIdRefRepo;
import com.initializers.services.apiservices.service.AdminClientExternalIdRefSevice;

@Service
public class AdminClientExternalIdRefSeviceImpl implements AdminClientExternalIdRefSevice {
	
	@Autowired
	AdminClientExternalIdRefRepo adminClientExternalIdRefRepo;
	
	@Override
	public void addExternalIdRef(String externalOrderId) {
		AdminClientExternalIdRef adminClientExternalIdRef = new AdminClientExternalIdRef();
		AdminClientExternalIdRef.CompositeKeyExtrnalId key = new CompositeKeyExtrnalId();
		key.setAppId(TenantContext.getAppId());
		key.setExternalId(externalOrderId);
		adminClientExternalIdRef.setId(key);
		adminClientExternalIdRefRepo.save(adminClientExternalIdRef);
	}

	@Override
	public void deleteExternalIdRef(String externalOrderId) {
		AdminClientExternalIdRef.CompositeKeyExtrnalId key = new CompositeKeyExtrnalId();
		key.setAppId(TenantContext.getAppId());
		key.setExternalId(externalOrderId);
		adminClientExternalIdRefRepo.deleteById(key);
	}

	@Override
	public String getAppIdForExternalIdRef(String externalOrderId) {
		List<AdminClientExternalIdRef> adminClientExternalIdRef = adminClientExternalIdRefRepo.findByIdExternalId(externalOrderId);
		if(adminClientExternalIdRef.size() > 0) {
//			Assuming all external id are unique, if this cause any issue, we need to consider userID as key
			return adminClientExternalIdRef.get(0).getId().getAppId();
		}
		return null;
	}

}
