package com.initializers.services.apiservices.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.Theme;

@Service
public interface ThemeService {
	List<Theme> getAllThemes();
	Theme addNewTheme(Theme theme);
}
