package com.initializers.services.apiservices.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.initializers.services.apiservices.constant.Scope;
import com.initializers.services.apiservices.elasticsearch.ElasticSearchAliasService;
import com.initializers.services.apiservices.exception.TenantNotFoundException;
import com.initializers.services.apiservices.model.AdminAppDetails;
import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.AdminClientDetails.CompositeKeyAdminClient;
import com.initializers.services.apiservices.model.AdminDetails;
import com.initializers.services.apiservices.model.Role;
import com.initializers.services.apiservices.model.TransientUserDetails;
import com.initializers.services.apiservices.model.UserDetails;
import com.initializers.services.apiservices.model.temp.LoggedInUser;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.repo.AdminAppDetailsRepo;
import com.initializers.services.apiservices.repo.AdminClientDetailsRepo;
import com.initializers.services.apiservices.repo.AdminDetailsRepo;
import com.initializers.services.apiservices.service.AdminClientDetailsService;
import com.initializers.services.apiservices.service.AdminDetailsService;
import com.initializers.services.apiservices.service.UserDetailsService;

@Service
public class AdminClientDetailsServiceImpl
		implements AdminClientDetailsService, org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	AdminClientDetailsRepo adminClientDetailsRepo;
	@Autowired
	AdminDetailsRepo adminDetailsRepo;
	@Autowired
	UserDetailsService userDetailsService;
	@Autowired
	AdminDetailsService adminDetailsService;
	@Autowired
	AdminAppDetailsRepo adminAppDetailsRepo;
	@Autowired
	ElasticSearchAliasService elasticSearchAliasService;
	@Autowired
	private ObjectMapper objectMapper;

	@Value("${initializers.role.default.user}")
	private String defaultUserRole;
	@Value("${initializers.role.default.admin}")
	private String defaultAdminRole;
	@Value("${initializers.app.package}")
	private String appPackage;

	@Override
	public AdminClientDetails getTenantForAdmin(String emailId) {
		return null;
	}

	@Override
	public AdminClientDetails getTenantForClient(String emailId, String appId) {
		return null;
	}

	@Override
	public AdminClientDetails createTenantDetails(TransientUserDetails transientUserDetails) {
		Set<Role> authorities = new HashSet<>();
		AdminClientDetails adminClientDetails = new AdminClientDetails();
		if (transientUserDetails.getScope() == Scope.CLIENT) {
//			Register Client
			adminClientDetails = adminClientDetailsRepo.findFirstByIdAppId(transientUserDetails.getAppId());
			if (adminClientDetails == null) {
				throw new TenantNotFoundException();
			}
//			set tenant Context
			TenantContext.setTenantId(transientUserDetails.getAppId().replace(".", "_"));

			UserDetails userDetails = new UserDetails(transientUserDetails.getFirstName(),
					transientUserDetails.getLastName());

			AdminClientDetails clientDetails = new AdminClientDetails();
			AdminClientDetails.CompositeKeyAdminClient key = new AdminClientDetails.CompositeKeyAdminClient();
			key.setAppId(adminClientDetails.getId().getAppId());
			key.setEmail(transientUserDetails.getEmail());
			clientDetails.setId(key);
			clientDetails.setUserId(userDetailsService.addUser(userDetails));
			clientDetails.setPassword(transientUserDetails.getPassword());
			clientDetails.setScope(Scope.CLIENT);
			authorities.add(new Role(Role.CLIENT));
			clientDetails.setAuthorities(authorities);
			adminClientDetails = clientDetails;
		} else {
//			Register Admin
//			create new Admin Master Data
			AdminDetails adminDetails = new AdminDetails(transientUserDetails.getFirstName(),
					transientUserDetails.getLastName(), transientUserDetails.getEmail());
			adminDetails = adminDetailsService.addAdminDetails(adminDetails);
//			set tenant Context as AppId
			TenantContext.setTenantId(transientUserDetails.getAppId().replace(".", "_"));
//			create new client data for admin
			UserDetails userDetails = new UserDetails(transientUserDetails.getFirstName(),
					transientUserDetails.getLastName());
			userDetails.setId(adminDetails.getId());
//			create app details
			AdminAppDetails adminAppDetails = new AdminAppDetails();
			adminAppDetails.setId(transientUserDetails.getAppId());
			adminAppDetails.setAppName(transientUserDetails.getAppName());
			adminAppDetails.setAppPackage(transientUserDetails.getAppId());
			AdminClientDetails.CompositeKeyAdminClient key = new AdminClientDetails.CompositeKeyAdminClient();
			key.setEmail(transientUserDetails.getEmail());
			key.setAppId(transientUserDetails.getAppId());
			adminClientDetails.setId(key);
			adminClientDetails.setPassword(transientUserDetails.getPassword());
			adminClientDetails.setScope(Scope.ADMIN);
			for (int i = 0; i < Role.ADMIN_ROLES.length; i++) {
				authorities.add(new Role(Role.ADMIN_ROLES[i]));
			}
			adminClientDetails.setAuthorities(authorities);
//			update app details
			adminAppDetailsRepo.save(adminAppDetails);
//			create client for admin
			userDetailsService.addUser(userDetails);
//			create alias for elastic search
			elasticSearchAliasService.createIndexAliasForTenant(TenantContext.getTenantId());
		}
		return adminClientDetailsRepo.save(adminClientDetails);
	}

	@Override
	public String getClientEmail(ObjectId clientId) {
		List<AdminClientDetails> keys = adminClientDetailsRepo.findIdByClientId(clientId);
		if (keys.size() > 0) {
			return keys.get(0).getId().getEmail();
		}
		return null;
	}

	@Override
	public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		return null;
	}

	@Override
	public AdminClientDetails getUserDetails(String userName) {
		return null;
	}

	@Override
	public AdminClientDetails getLoggingInUser(String loggedInUserStr) {
		AdminClientDetails adminClientDetails;
//		Object obj = String.valueOf(loggedInUserStr);
//		Class cls = obj.getClass();
//		try {
//			Method mtd = cls.getDeclaredMethod("getUsername");
//			String resultObject = (String) mtd.invoke(obj);
//		} catch (Exception e1) {
//			return null;
//		}
		try {
			LoggedInUser loggedInUser = objectMapper.readValue(loggedInUserStr, LoggedInUser.class);
			AdminClientDetails.CompositeKeyAdminClient key = new CompositeKeyAdminClient();
			if (loggedInUser.getAppId() != null) {
				key.setAppId(loggedInUser.getAppId());
				key.setEmail(loggedInUser.getUsername().toLowerCase());
				adminClientDetails = adminClientDetailsRepo.findFirstById(key);
			} else {
				AdminClientDetails.CompositeKeyAdminClient key1 = new CompositeKeyAdminClient();
				key1.setAppId("com.initializers.fantasticfouraic7z");
				key1.setEmail("karthikhanumanthaiah@gmail.com");
				adminClientDetails = adminClientDetailsRepo.findFirstByIdEmailAndScope(loggedInUser.getUsername(),
						Scope.ADMIN);
			}
			return adminClientDetails;
		} catch (JsonMappingException e) {
			return null;
		} catch (JsonProcessingException e) {
			return null;
		}

	}

}
