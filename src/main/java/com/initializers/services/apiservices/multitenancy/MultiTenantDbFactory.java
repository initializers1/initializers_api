package com.initializers.services.apiservices.multitenancy;

import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MultiTenantDbFactory extends SimpleMongoClientDatabaseFactory 
{
	private final String defaultDatabaseName;
	
	public MultiTenantDbFactory(MongoClient mongoClient, String databaseName) {
		super(mongoClient, databaseName);
		this.defaultDatabaseName = databaseName;
	}
	
    @Override
    protected MongoDatabase doGetMongoDatabase(String dbName) {
    	if(TenantContext.getTenantId() == null) {
    		return getMongoClient().getDatabase(defaultDatabaseName);
    	}else {
    		return getMongoClient().getDatabase(TenantContext.getTenantId());
    	}
    }
}