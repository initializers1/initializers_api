package com.initializers.services.apiservices.multitenancy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.repo.PaymentGatewayStatusCustomRepo;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
@EnableMongoRepositories(basePackages = {"com"}, excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = MasterData.class),
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {PaymentGatewayStatusCustomRepo.class})})
public class MultiTenantConfig extends AbstractMongoClientConfiguration {
	
	@Value("${spring.data.mongodb.database}")
	private String dataBaseName;
	@Value("${spring.data.mongodb.uri}")
	private String connectionString;
	
	@Autowired
	MongoDatabaseFactory mongoDbFactory;
    
	@Override
	public MongoClient mongoClient() {
		return MongoClients.create(connectionString);
	}
	
	@Override
	protected String getDatabaseName() {
		return dataBaseName;
	}
	
    @Override
    @Primary
    @Bean
    public MongoDatabaseFactory mongoDbFactory() {
    	return new MultiTenantDbFactory(mongoClient(), dataBaseName);
    }
    
    @Override
    @Bean
    @Primary
    public MongoTemplate mongoTemplate(MongoDatabaseFactory databaseFactory, MappingMongoConverter converter) {
    	System.out.println("In mongoTemplate()"+mongoDbFactory);
        return new MongoTemplate(mongoDbFactory);
    }

    @Bean
    public MongoTemplate mongoTemplateShared() {
    	MongoDatabaseFactory mongoDbFactory = new SimpleMongoClientDatabaseFactory(mongoClient(), dataBaseName);
        return new MongoTemplate(mongoDbFactory);
    }

}
