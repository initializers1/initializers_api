package com.initializers.services.apiservices.multitenancy;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.initializers.services.apiservices.annotation.MasterData;

@Configuration
@EnableMongoRepositories(basePackages = {"com"},
        includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = MasterData.class),
        mongoTemplateRef = "mongoTemplateShared"
)
public class MasterDataConfig {

}
