package com.initializers.services.apiservices.multitenancy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("TenantContext")
public class TenantContext {
	private static final ThreadLocal<String> Tenant_Id = new ThreadLocal<>();
	private static final ThreadLocal<String> App_Id = new ThreadLocal<>();
	
	public static String getTenantIdForAppId(String appId) {
		String tenantId = appId.replace(".", "_");
		return tenantId;
	}
	public static void setTenantId(String tenantId ) {
		Tenant_Id.set(tenantId);
	}
	@Bean
	public static String getTenantId() {
		return Tenant_Id.get();
	}
	public static void clearTenant() {
		Tenant_Id.remove();
	}
	public static String getAppId() {
		return App_Id.get();
	}
	public static void setAppId(String appId) {
		App_Id.set(appId);
	}
}
