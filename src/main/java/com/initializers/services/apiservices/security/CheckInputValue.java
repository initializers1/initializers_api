package com.initializers.services.apiservices.security;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.initializers.services.apiservices.exception.EmailIDException;
import com.initializers.services.apiservices.exception.PasswordShouldMatchCrieteriaException;
import com.initializers.services.apiservices.exception.SpecialCharacterNotAllowedException;

public class CheckInputValue {

	public static String checkFreeTextFields(String str) {
		if(str != null) {
			str = str.trim();
			Pattern special = Pattern.compile ("[!#$%&*()+=|<>?{}\\[\\]~-]");
			Matcher hasSpecial = special.matcher(str);
			if(hasSpecial.find()) {
				throw new SpecialCharacterNotAllowedException();
			}		
		}
		return str;
	}
	
	public static void checkPassword(String password) {
		Pattern letter = Pattern.compile("[a-zA-z]");
        Pattern digit = Pattern.compile("[0-9]");
        
        Matcher hasLetter = letter.matcher(password);
        Matcher hasDigit = digit.matcher(password);
        
        if(password == null || password.length() < 8 || !hasLetter.find() || !hasDigit.find()) {
        	throw new PasswordShouldMatchCrieteriaException();
        }
	}
	public static void checkEmail(String email) {
		if(!email.contains("@")) {
			throw new EmailIDException();
		}
	}
	
}
