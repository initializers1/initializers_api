package com.initializers.services.apiservices.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.model.AdminClientDetails;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenUtil {

	private final String jwtSecret = "zdtlD3JK56m6wTTgsNFhqzjqP";
	private final String jwtIssuer = "initializers.auth";

//    private final Logger logger;

	public String generateAccessToken(AdminClientDetails user) {
		Map<String, Object> role = new HashMap<>();
		role.put("role", user.getAuthorities());
		return Jwts.builder().setSubject(user.getId().getAppId()).setId(user.getId().getEmail()).setIssuer(jwtIssuer)
				.setIssuedAt(new Date()).setExpiration(DateUtils.addDays(new Date(), 1)) // 1 day
				.signWith(SignatureAlgorithm.HS512, jwtSecret).claim("role", user.getAuthorities())
				.claim("userId", user.getUserId().toHexString()).compact();
	}

	public String getUserId(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		return claims.getOrDefault("userId", "").toString();
	}

	public String getId(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		return claims.getId();
	}

	public String getTenantId(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		return claims.getSubject();
	}

	public Date getExpirationDate(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();

		return claims.getExpiration();
	}

	public Object getRoles(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();

		return claims.getOrDefault("role", null);
	}

	public boolean validate(String token) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
			return true;
		} catch (MalformedJwtException ex) {
//            logger.error("Invalid JWT token - {}", ex.getMessage());
		} catch (ExpiredJwtException ex) {
//            logger.error("Expired JWT token - {}", ex.getMessage());
		} catch (UnsupportedJwtException ex) {
//            logger.error("Unsupported JWT token - {}", ex.getMessage());
		} catch (IllegalArgumentException ex) {
//            logger.error("JWT claims string is empty - {}", ex.getMessage());
		}
		return false;
	}

}