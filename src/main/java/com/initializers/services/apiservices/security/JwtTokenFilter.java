package com.initializers.services.apiservices.security;

import static java.util.Optional.ofNullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.elasticsearch.common.collect.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.AdminClientDetails.CompositeKeyAdminClient;
import com.initializers.services.apiservices.model.Role;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.repo.AdminClientDetailsRepo;

@Configuration
public class JwtTokenFilter extends OncePerRequestFilter {
	private final JwtTokenUtil jwtTokenUtil;

	@Autowired
	AdminClientDetailsRepo adminClientDetailRepo;

	public JwtTokenFilter(JwtTokenUtil jwtTokenUtil) {
		this.jwtTokenUtil = jwtTokenUtil;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		// Get authorization header and validate
		final String header = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (header == null || !header.startsWith("Bearer ")) {
			chain.doFilter(request, response);
			return;
		}

		// Get jet token and validate
		final String token = header.split(" ")[1].trim();
		if (!jwtTokenUtil.validate(token)) {
			chain.doFilter(request, response);
			return;
		}

		String appId = jwtTokenUtil.getTenantId(token);
		String email = jwtTokenUtil.getId(token);
		String userId = jwtTokenUtil.getUserId(token);
		ArrayList<LinkedHashMap<String, String>> extractedRole = (ArrayList<LinkedHashMap<String, String>>) jwtTokenUtil
				.getRoles(token);
		Set<Role> roles = new HashSet<>();

		extractedRole.forEach(e -> {
			Role newRole = new Role();
			newRole.setAuthority(e.get("authority"));
			roles.add(newRole);
		});
		TenantContext.setAppId(appId);
		TenantContext.setTenantId(TenantContext.getTenantIdForAppId(appId));
		// Set is authenticated to true
		AdminClientDetails adminClientDetails = new AdminClientDetails();
		AdminClientDetails.CompositeKeyAdminClient key = new CompositeKeyAdminClient();
		key.setAppId(appId);
		key.setEmail(email);
		adminClientDetails.setId(key);
		try {
			adminClientDetails.setUserId(new ObjectId(userId));
		} catch (Exception e) {
//			log exception
		}
		adminClientDetails.setAuthorities(roles);
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(adminClientDetails,
				null, ofNullable(adminClientDetails).map(AdminClientDetails::getAuthorities).orElse(List.of()));
		authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(request, response);
	}
}
