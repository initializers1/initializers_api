package com.initializers.services.apiservices.elasticsearch;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

@Configuration
public class ElasticSearchConfig  extends AbstractElasticsearchConfiguration  {
	
    @Value("${elasticsearch.host}")
    private String EsHost;
    @Value("${elasticsearch.port}")
    private int EsPort;
    @Value("${elasticsearch.clustername}")
    private String EsClusterName;

    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {
    	RestHighLevelClient client;
    	try {
    		final ClientConfiguration clientConfiguration = ClientConfiguration.builder()  
    				.connectedTo(EsHost+":"+EsPort)
    				.build();
    		
    		client = RestClients.create(clientConfiguration).rest();                             		
    	}catch(Exception ex) {
    		return null;
    	}
    	return client;
    }

}
