package com.initializers.services.apiservices.elasticsearch;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.elasticsearch.ItemCategoryES;
import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.model.elasticsearch.ItemSubCategoryES;
import com.initializers.services.apiservices.others.ApplicationProperties;
import com.initializers.services.apiservices.repo.SearchRepo;

@Service
public class GlobalSearchFactroy {
	@Autowired
	@Qualifier("withES")
	SearchRepo searchRepowithES;

	@Autowired
	@Qualifier("withoutES")
	SearchRepo searchRepoWithoutES;

	public List<ItemDetailsES> searchItemDetails(String searchTerm) {
		List<ItemDetailsES> itemDetailsES;
		if (ApplicationProperties.enable_elasticSearch) {
			itemDetailsES = searchRepowithES.searchItemDetails(searchTerm);
		} else {
			itemDetailsES = searchRepoWithoutES.searchItemDetails(searchTerm);
		}
		return itemDetailsES;
	}
	
	public List<ItemCategoryES> searchItemCategory(String searchTerm) {
		List<ItemCategoryES> itemCategoryES;
		if (ApplicationProperties.enable_elasticSearch) {
			itemCategoryES = searchRepowithES.searchItemCategory(searchTerm);
		} else {
			itemCategoryES = searchRepoWithoutES.searchItemCategory(searchTerm);
		}
		return itemCategoryES;
	}
	
	public List<ItemSubCategoryES> searchItemSubCategory(String searchTerm) {
		List<ItemSubCategoryES> itemSubCategoryES;
		if (ApplicationProperties.enable_elasticSearch) {
			itemSubCategoryES = searchRepowithES.searchItemSubCategory(searchTerm);
		} else {
			itemSubCategoryES = searchRepoWithoutES.searchItemSubCategory(searchTerm);
		}
		return itemSubCategoryES;
	}
}
