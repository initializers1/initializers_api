package com.initializers.services.apiservices.elasticsearch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest.AliasActions;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.elasticsearch.ItemCategoryES;
import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.model.elasticsearch.ItemSubCategoryES;
import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.model.item.ItemSubCategory;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.repo.AdminClientDetailsRepo;
import com.initializers.services.apiservices.repo.ItemCategoryRepo;
import com.initializers.services.apiservices.repo.ItemDetailsRepo;
import com.initializers.services.apiservices.repo.ItemSubCategoryRepo;

// super Admin Task only
@Component
public class ElasticSearchReIndexService {

	@Autowired
	ElasticsearchOperations elasticSearchOperation;
	@Autowired
	RestHighLevelClient restHighLevelClient;
	@Autowired
	AdminClientDetailsRepo adminClientDetailsRepo;
	@Autowired
	ItemDetailsRepo itemDetailsRepo;
	@Autowired
	ItemCategoryRepo itemCategoryRepo;
	@Autowired
	ItemSubCategoryRepo itemSubCategoryRepo;

	public void bulkIndexForAllTenants() {
		List<AdminClientDetails> adminClientDetails = adminClientDetailsRepo.findAll();
		createIndexIfNotExist();
		createIndexAlias(adminClientDetails);
		bulkIndex(adminClientDetails, getIndexNames());
	}

	public void bulkIndexForTenantId(String appId) {
		List<AdminClientDetails> adminClientDetailsList = constructListForTenantId(appId);

		createIndexAlias(adminClientDetailsList);
		bulkIndex(adminClientDetailsList, getIndexNames());
	}

	public void bulkIndexForAllTenantForIndex(String indexName) {
		List<AdminClientDetails> adminClientDetails = adminClientDetailsRepo.findAll();
		Set<String> index = new HashSet<>();
		index.add(indexName);

		createIndexAlias(adminClientDetails);
		bulkIndex(adminClientDetails, index);
	}

	private void createIndexIfNotExist() {
		Set<String> indexNames = getIndexNames();
		indexNames.forEach(index -> {
			GetIndexRequest getIndexRequest = new GetIndexRequest(index);
			try {
				boolean exists = restHighLevelClient.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
				if (!exists) {
					CreateIndexRequest createIndexRequest = new CreateIndexRequest(index);
					StringBuffer settings = getSettings(index);
					StringBuffer mapping = getMapping(index);
					if (settings != null && mapping != null) {
						createIndexRequest.mapping(mapping.toString(), XContentType.JSON);
						createIndexRequest.settings(settings.toString(), XContentType.JSON);
					}
					restHighLevelClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	private StringBuffer getMapping(String index) {
		InputStream inputStream;
		try {
			inputStream = new ClassPathResource("/ElasticSearch/mapping/"+index+".json").getInputStream();
			InputStreamReader isReader = new InputStreamReader(inputStream);
			BufferedReader reader = new BufferedReader(isReader);
			StringBuffer itemDetailsMapping = new StringBuffer();
			String str;
			while ((str = reader.readLine()) != null) {
				itemDetailsMapping.append(str);
			}
			return itemDetailsMapping;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private StringBuffer getSettings(String index) {
		InputStream inputStream;
		try {
			inputStream = new ClassPathResource("/ElasticSearch/settings/"+index+".json").getInputStream();
			InputStreamReader isReader = new InputStreamReader(inputStream);
			BufferedReader reader = new BufferedReader(isReader);
			StringBuffer itemDetailsSettings = new StringBuffer();
			String str;
			while ((str = reader.readLine()) != null) {
				itemDetailsSettings.append(str);
			}
			return itemDetailsSettings;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<AdminClientDetails> constructListForTenantId(String tenantId) {
		List<AdminClientDetails> adminClientDetailsList = new ArrayList<>();
		AdminClientDetails.CompositeKeyAdminClient key = new AdminClientDetails.CompositeKeyAdminClient(null, tenantId);
		AdminClientDetails adminClientDetails = new AdminClientDetails();
		adminClientDetails.setId(key);
		adminClientDetailsList.add(adminClientDetails);
		return adminClientDetailsList;
	}

	private void createIndexAlias(List<AdminClientDetails> adminClientDetails) {

		IndicesAliasesRequest request = new IndicesAliasesRequest();
		Set<String> indexNames = getIndexNames();
		
		adminClientDetails.forEach(adminClient -> {
			String alias = TenantContext.getTenantIdForAppId(adminClient.getId().getAppId());
//			AliasActions aliasAction = new AliasActions(AliasActions.Type.ADD).indices(indexNames.toArray(new String[0])).alias(alias)
//					.filter("{\"term\":{\"appId\":\"" + alias + "\"}}");
//			request.addAliasAction(aliasAction);
			indexNames.forEach(index -> {
				AliasActions aliasAction = new AliasActions(AliasActions.Type.ADD).index(index).alias(index+""+alias)
						.filter("{\"term\":{\"appId\":\"" + alias + "\"}}");
				request.addAliasAction(aliasAction);
			});
		});

		try {
			restHighLevelClient.indices().updateAliases(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void bulkIndex(List<AdminClientDetails> adminClientDetails, Set<String> indexNames) {

		adminClientDetails.forEach(adminClientDetail -> {
			String alias = TenantContext.getTenantIdForAppId(adminClientDetail.getId().getAppId());
			TenantContext.clearTenant();
			TenantContext.setTenantId(alias);
			indexNames.forEach(index -> {
				List<IndexQuery> indexQuery = new ArrayList<>();
				switch (index) {
				case ItemDetailsES.indexName:
					List<ItemDetails> itemDetails = itemDetailsRepo.findAll();
					itemDetails.forEach(item -> {
						ItemDetailsES itemDetailsEs = new ItemDetailsES();
						BeanUtils.copyProperties(item, itemDetailsEs);
						itemDetailsEs.setAppId(alias);
						indexQuery.add(new IndexQueryBuilder().withId(itemDetailsEs.getId().toString())
								.withObject(itemDetailsEs).build());
					});
					if(indexQuery.size() > 0) {
						elasticSearchOperation.bulkIndex(indexQuery, ItemDetailsES.class);
					}
					break;
				case ItemCategoryES.indexName: 
					List<ItemCategory> itemCategories = itemCategoryRepo.findAll();
					itemCategories.forEach(category -> {
						ItemCategoryES itemCategoryES = new ItemCategoryES();
						BeanUtils.copyProperties(category, itemCategoryES);
						itemCategoryES.setAppId(alias);
						indexQuery.add(new IndexQueryBuilder().withId(itemCategoryES.getId().toString())
								.withObject(itemCategoryES).build());
					});
					if(indexQuery.size() > 0) {
						elasticSearchOperation.bulkIndex(indexQuery, ItemCategoryES.class);
					}
					break;
				case ItemSubCategoryES.indexName: 
					List<ItemSubCategory> itemSubCategories = itemSubCategoryRepo.findAll();
					itemSubCategories.forEach(subcategory -> {
						ItemSubCategoryES itemSubCategoryES = new ItemSubCategoryES();
						BeanUtils.copyProperties(subcategory, itemSubCategoryES);
						itemSubCategoryES.setAppId(alias);
						indexQuery.add(new IndexQueryBuilder().withId(itemSubCategoryES.getId().toString())
								.withObject(itemSubCategoryES).build());
					});
					if(indexQuery.size() > 0) {
						elasticSearchOperation.bulkIndex(indexQuery, ItemSubCategoryES.class);
					}
					break;
				default:
					break;
				}
			});
		});

	}

	private Set<String> getIndexNames() {
		Set<String> indexNames = new HashSet<>();
		indexNames.add(ItemDetailsES.indexName);
		indexNames.add(ItemCategoryES.indexName);
		indexNames.add(ItemSubCategoryES.indexName);
		return indexNames;
	}
}
