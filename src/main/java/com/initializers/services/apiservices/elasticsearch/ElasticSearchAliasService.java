package com.initializers.services.apiservices.elasticsearch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.repo.elasticsearch.ItemDetailsElasticSearchCustomRepo;
import com.initializers.services.apiservices.repo.elasticsearch.ItemDetailsElasticSearchRepo;

@Component
public class ElasticSearchAliasService {

	@Autowired
	ItemDetailsElasticSearchCustomRepo itemDetailsElasticSearchCustomRepo;
	
	@Async
	public void createIndexAliasForTenant(String tenantId) {
		itemDetailsElasticSearchCustomRepo.createIndexAlias(tenantId);
	}
}
