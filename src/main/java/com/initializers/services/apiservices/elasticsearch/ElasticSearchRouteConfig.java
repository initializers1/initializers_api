package com.initializers.services.apiservices.elasticsearch;

import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.repository.query.AbstractElasticsearchRepositoryQuery;
import org.springframework.data.elasticsearch.repository.query.ElasticsearchQueryMethod;

public class ElasticSearchRouteConfig extends AbstractElasticsearchRepositoryQuery{

	public ElasticSearchRouteConfig(ElasticsearchQueryMethod queryMethod,
			ElasticsearchOperations elasticsearchOperations) {
		super(queryMethod, elasticsearchOperations);
		System.out.println("ElasticSearchRepo constructor");
	}

	@Override
	public Object execute(Object[] parameters) {
		System.out.println("execute.....");
		return null;
	}

	@Override
	public boolean isCountQuery() {
		System.out.println("countquery.....");
		return false;
	}

}
