package com.initializers.services.apiservices.controller;

import java.util.HashMap;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.initializers.services.apiservices.constant.Scope;
import com.initializers.services.apiservices.model.AdminAppDetails;
import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.TransientUserDetails;
import com.initializers.services.apiservices.model.UserOTP;
import com.initializers.services.apiservices.model.temp.LoggedInUser;
import com.initializers.services.apiservices.model.temp.LoggedInUserResponse;
import com.initializers.services.apiservices.others.ApplicationProperties;
import com.initializers.services.apiservices.security.JwtTokenUtil;
import com.initializers.services.apiservices.service.AdminAppDetailsService;
import com.initializers.services.apiservices.service.AdminClientDetailsService;
import com.initializers.services.apiservices.service.TransientUserDetailsService;
import com.initializers.services.apiservices.service.UserOTPService;

@RestController()
@RequestMapping("/user")
@CrossOrigin
public class UserRegistration {

	@Autowired
	JwtTokenUtil jwtTokenUtil;
	@Autowired
	AdminClientDetailsService adminClientDetailsService;
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	AdminAppDetailsService adminAppDetailsService;

	@Autowired
	TransientUserDetailsService transientUserDetailsService;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private UserOTPService userOTPService;

	@PostMapping("/register")
	public Object registerUser(@RequestBody TransientUserDetails transientUserDetails) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("userId", transientUserDetailsService.addUser(transientUserDetails).toString());
		result.put("otpExpiryTime", ApplicationProperties.otp_expireTime);
		return result;
//		return transientUserDetailsService.addUser(transientUserDetails).toString();
	}

	@PostMapping("/login")
	public Object loginUser(@RequestBody LoggedInUser loggedInUser) {
		try {
			String principle = objectMapper.writeValueAsString(loggedInUser);
			Authentication authenticate = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(principle, loggedInUser.getPassword()));
			LoggedInUserResponse loggedInUserResponse = mapUserDetails(
					(AdminClientDetails) authenticate.getPrincipal());
			return ResponseEntity.ok().body(loggedInUserResponse);
		} catch (JsonProcessingException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		} catch (BadCredentialsException ex) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}

	@PostMapping("/otp")
	public Object validateUserOTP(@RequestBody UserOTP userOTP) {
		AdminClientDetails adminClientDetails = userOTPService.validateUserOTP(userOTP);
		LoggedInUserResponse loggedInUserResponse = mapUserDetails(adminClientDetails);
		return ResponseEntity.ok().body(loggedInUserResponse);
	}

	@GetMapping("/otp/{userId}")
	public Map<String, Object> regenerateOTP(@PathVariable ObjectId userId) {
		return userOTPService.regenerateUserOTP(userId);
	}

//	Util

	private LoggedInUserResponse mapUserDetails(AdminClientDetails adminClientDetails) {
		LoggedInUserResponse loggedInUserResponse = new LoggedInUserResponse();

//		add app details for admin
		if (adminClientDetails.getScope().equals(Scope.ADMIN)) {
			AdminAppDetails adminAppDetails = adminAppDetailsService
					.getAppNameIconById(adminClientDetails.getId().getAppId());
			if (adminAppDetails != null) {
				loggedInUserResponse.setAppName(adminAppDetails.getAppName());
				loggedInUserResponse.setAppIcon(adminAppDetails.getAppIcon());
			}
		}

		loggedInUserResponse.setId(adminClientDetails.getUserId());
		loggedInUserResponse.setUserName(adminClientDetails.getUsername());
		loggedInUserResponse.setAccessToken(jwtTokenUtil.generateAccessToken(adminClientDetails));
		return loggedInUserResponse;
	}
}