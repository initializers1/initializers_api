package com.initializers.services.apiservices.controller;

import org.cloudinary.json.JSONArray;
import org.cloudinary.json.JSONException;
import org.cloudinary.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonSyntaxException;
import com.initializers.services.apiservices.model.AdminAppDetails;
import com.initializers.services.apiservices.multitenancy.MultiTenantConfig;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.others.PipelineUtil;
import com.initializers.services.apiservices.repo.AdminAppDetailsRepo;
import com.initializers.services.apiservices.service.AdminClientExternalIdRefSevice;
import com.initializers.services.apiservices.service.UserPaymentService;
import com.stripe.model.Event;
import com.stripe.model.EventDataObjectDeserializer;
import com.stripe.model.PaymentIntent;
import com.stripe.model.StripeObject;
import com.stripe.net.ApiResource;

@RestController()
@RequestMapping("/webhooks")
@CrossOrigin
public class WebHooksController {
	@Autowired
	AdminClientExternalIdRefSevice adminClientExternalIdRefSevice;
	@Autowired
	UserPaymentService userPaymentService;
	@Autowired
	AdminAppDetailsRepo adminAppDetailsRepo;
	private RestTemplate restTemplate;

	@Value("${initializers.gitlab.host}")
	private String gitlabHost;
	@Value("${initializers.gitlab.access_token}")
	private String gitlabAccessToken;
	@Value("${initializers.gitlab.mobile.projectId}")
	private String gitlabMobileAppProjectId;
	@Value("${initializers.expo.display.baseURL}")
	private String expoBaseURL;
	@Value("${initializers.expo.display.baseURL.mobile}")
	private String expoBaseURLMobile;
	@Autowired
	MultiTenantConfig multiTenantConfig;

	WebHooksController(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@PostMapping("/")
	public String stripeWebhook(@RequestBody String payload) {
		Event event = null;
		try {
			event = ApiResource.GSON.fromJson(payload, Event.class);
		} catch (JsonSyntaxException e) {
			return "";
		}
		EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
		StripeObject stripeObject = null;
		if (dataObjectDeserializer.getObject().isPresent()) {
			stripeObject = dataObjectDeserializer.getObject().get();
		}
		switch (event.getType()) {
		case "payment_intent.succeeded":
			PaymentIntent paymentIntent = (PaymentIntent) stripeObject;
			String appId = adminClientExternalIdRefSevice.getAppIdForExternalIdRef(paymentIntent.getId());
			TenantContext.setTenantId(appId.replace(".", "_"));
			userPaymentService.updateUserPaymentStatus(paymentIntent.getClientSecret(), "Stripe",
					paymentIntent.getStatus(), null);
			adminClientExternalIdRefSevice.deleteExternalIdRef(paymentIntent.getId());
			break;
		case "payment_intent.payment_failed":
			PaymentIntent paymentIntentFailed = (PaymentIntent) stripeObject;
			String appIdFailed = adminClientExternalIdRefSevice.getAppIdForExternalIdRef(paymentIntentFailed.getId());
			TenantContext.setTenantId(appIdFailed.replace(".", "_"));
			userPaymentService.updateUserPaymentStatus(paymentIntentFailed.getClientSecret(), "Stripe",
					paymentIntentFailed.getStatus(), paymentIntentFailed.getCancellationReason());
			break;
		default:
			return "";
		}
		return "Ok";
	}

	@PostMapping("/gitlab")
	public String gitlabPipelineEvent(@RequestBody String payload) {
		try {
			JSONObject jsonPayload = new JSONObject(payload);
			JSONObject objectAttribute = jsonPayload.getJSONObject("object_attributes");
			Integer piplineId = objectAttribute.getInt("id");
			String status = objectAttribute.getString("status");
			String expoAppURL = "";
			JSONArray builds = jsonPayload.getJSONArray("builds");
			if (builds.length() > 0) {
				JSONObject jobs = builds.getJSONObject(0);
				Integer jobId = jobs.getInt("id");
				String jobLogs = restTemplate.getForObject(constructJobUrl(jobId), String.class);
				expoAppURL = PipelineUtil.extractExpoURL(jobLogs, expoBaseURL, expoBaseURLMobile);
			}
			updatePipelineDetails(piplineId, status, expoAppURL);
		} catch (JSONException exp) {
		}
		return "ok";
	}

	@PostMapping("/test")
	public String addId(@RequestParam String id) {
		adminClientExternalIdRefSevice.addExternalIdRef(id);
		return "ok";
	}

	private String constructJobUrl(Integer jobId) {
		return "" + gitlabHost + "/projects/" + gitlabMobileAppProjectId + "/jobs/" + jobId + "/trace?private_token="
				+ gitlabAccessToken;
	}

	private void updatePipelineDetails(Integer piplineId, String status, String expoBuildUrl) {
		Query query = new Query();
		query.addCriteria(Criteria.where("pipelineId").is(piplineId));
		Update update = new Update();
		update.set("pipelineStatus", status);
		update.set("expoBuildUrl", expoBuildUrl);
		multiTenantConfig.mongoTemplateShared().findAndModify(query, update, AdminAppDetails.class);
	}
}
