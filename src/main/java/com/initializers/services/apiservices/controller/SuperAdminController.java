package com.initializers.services.apiservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.initializers.services.apiservices.elasticsearch.ElasticSearchReIndexService;
import com.initializers.services.apiservices.service.RoleService;

@RestController()
@RequestMapping("/superadmin")
@CrossOrigin
public class SuperAdminController {
	
	@Autowired
	ElasticSearchReIndexService elasticSearchReIndexService;
	
	
////	Setup default customization in the database
//	@GetMapping("/initializeDatabase")
//	public void initializeDatabase() {
////	Role - Default user and admin role
//		roleService.initializeDefaultRoles();
//	}
	@GetMapping("/reIndex")
	public void elasticSearchReindex() {
		elasticSearchReIndexService.bulkIndexForAllTenants();
	}
	@GetMapping("/reIndex/{appId}")
	public void elasticSearchReIndexByTenantId(@PathVariable String appId) {
		elasticSearchReIndexService.bulkIndexForTenantId(appId);
	}
	@GetMapping("/reIndex/{indexName}")
	public void elasticSearchReIndexByIndexName(@PathVariable String indexName) {
		elasticSearchReIndexService.bulkIndexForAllTenantForIndex(indexName);
	}
}
