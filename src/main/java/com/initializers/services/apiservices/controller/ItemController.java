package com.initializers.services.apiservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.initializers.services.apiservices.model.TopDeals;
import com.initializers.services.apiservices.service.TopDealsService;

@RestController()
@RequestMapping("/item")
@CrossOrigin
public class ItemController {
	@Autowired
	private TopDealsService topDealsService;

	//*************************************************************************
	//top deals
	//*************************************************************************
	@GetMapping("/top-deals")
	public List<TopDeals> getTopDeals() {
		return topDealsService.getTopDeals();
	}
	@PostMapping("/top-deals")
	public TopDeals addTopDeals(@RequestBody TopDeals topDeals) {
		return topDealsService.addNewTopDeals(topDeals);
	}
}
