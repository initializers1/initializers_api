package com.initializers.services.apiservices.controller;

import java.util.List;
import java.util.Optional;

import javax.annotation.security.RolesAllowed;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.initializers.services.apiservices.model.AdminAppDetails;
import com.initializers.services.apiservices.model.HomePage;
import com.initializers.services.apiservices.model.Role;
import com.initializers.services.apiservices.model.Theme;
import com.initializers.services.apiservices.model.UserOrderSet;
import com.initializers.services.apiservices.model.actionparam.ChangeStatus;
import com.initializers.services.apiservices.model.actionparam.GenerateApp;
import com.initializers.services.apiservices.model.input.CreateOrderInput;
import com.initializers.services.apiservices.model.item.ItemAvailability;
import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.model.item.ItemSubCategory;
import com.initializers.services.apiservices.model.payment.AdminPayementGatewayKey;
import com.initializers.services.apiservices.model.payment.PaymentGateway;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;
import com.initializers.services.apiservices.model.payment.PaymentProcess;
import com.initializers.services.apiservices.others.QueryString;
import com.initializers.services.apiservices.others.Suggestions;
import com.initializers.services.apiservices.payment.PaymentProcessServices;
import com.initializers.services.apiservices.service.AdminAppDetailsService;
import com.initializers.services.apiservices.service.AdminPayementGatewayKeyService;
import com.initializers.services.apiservices.service.HomePageService;
import com.initializers.services.apiservices.service.ImageService;
import com.initializers.services.apiservices.service.ItemAvailabilityService;
import com.initializers.services.apiservices.service.ItemCategoryService;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.ItemSubCategoryService;
import com.initializers.services.apiservices.service.PaymentGatewayService;
import com.initializers.services.apiservices.service.ThemeService;
import com.initializers.services.apiservices.service.UserOrderService;

@RestController()
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private ItemDetailsService itemDetailsService;
	@Autowired
	private ItemCategoryService itemCategoryService;
	@Autowired
	private ItemSubCategoryService itemSubCategoryService;
	@Autowired
	private ItemAvailabilityService itemAvailabilityService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private UserOrderService userOrderService;
	@Autowired
	private HomePageService homePageService;
	@Autowired
	private AdminPayementGatewayKeyService adminPayementGatewayKeyService;
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	@Autowired
	private PaymentProcessServices paymentProcessServices;
	@Autowired
	private AdminAppDetailsService adminAppDetailsService;
	@Autowired
	private ThemeService themeService;

//***********************************************************************************
//	Item Details 
//***********************************************************************************
	@GetMapping("/itemDetails")
	public Page<Object> getItemDetailsPage(@RequestParam(required = false) String[] filter, String search,
			Suggestions suggestions, int offset, int count) {
		QueryString queryString = new QueryString();
		queryString.setFilter(filter);
		queryString.setSearch(search);
		return itemDetailsService.getItemDetailsPage(queryString, suggestions, PageRequest.of(offset, count));
	}

	@RolesAllowed(value = { Role.ADMIN, Role.READ_ITEM })
	@GetMapping("/itemDetails/{itemId}")
	public Object getItemDetailsById(@PathVariable ObjectId itemId) {
		return itemDetailsService.getAdminItemDetails(itemId);
	}

	@PostMapping("/itemDetails")
	public Object addItemDetails(@RequestBody ItemDetails itemDetails) {
		return itemDetailsService.addItemDetailsAdmin(itemDetails);
	}

	@PutMapping("/itemDetails")
	public Object updateItemDetails(@RequestBody ItemDetails itemDetails) {
		return itemDetailsService.updateItemDetailsAdmin(itemDetails);
	}

	@PostMapping("/itemDetails/changeStatus")
	public Object changeStatus(@RequestBody ChangeStatus changeStatus) {
		return itemDetailsService.changeStatus(changeStatus);
	}

//***********************************************************************************
//	Item Availability 	
//***********************************************************************************
	@GetMapping("/itemAvailability/{availabilityId}")
	public Object getItemAvailability(@PathVariable ObjectId availabilityId) {
		return itemAvailabilityService.getAvailabilityByIdAdmin(availabilityId);
	}

	@PutMapping("/itemAvailability")
	public Object addItemAvailability(@RequestBody ItemAvailability itemAvailability) {
		return itemAvailabilityService.updateAvailability(itemAvailability);
	}

	@PostMapping("/itemAvailability")
	public Object updateItemAvailability(@RequestBody ItemAvailability itemAvailability) {
		return itemAvailabilityService.addAvailability(itemAvailability);
	}

//***********************************************************************************
//	Item Category	
//***********************************************************************************
	@GetMapping("/itemCategory")
	public Object getItemCategory(@RequestParam(required = false) String[] filter, String search,
			Suggestions suggestions, Integer offset, Integer count) {
		QueryString queryString = new QueryString();
		queryString.setFilter(filter);
		queryString.setSearch(search);
		if (offset == null || count == null) {
			return itemCategoryService.getItemCategoryPage(queryString, suggestions, null);
		} else {
			return itemCategoryService.getItemCategoryPage(queryString, suggestions, PageRequest.of(offset, count));
		}
	}

	@GetMapping("/itemCategory/{categoryId}")
	public Object getItemCategoryById(@PathVariable ObjectId categoryId) {
		return itemCategoryService.getItemCategoryById(categoryId);
	}

	@PostMapping("/itemCategory")
	public Object addItemCategory(@RequestBody ItemCategory itemCategory) {
		return itemCategoryService.addItemCategoryAdmin(itemCategory);
	}

	@PutMapping("/itemCategory")
	public Object updateItemCategory(@RequestBody ItemCategory itemCategory) {
		return itemCategoryService.updateCategoryAdmin(itemCategory);
	}

	@DeleteMapping("/itemCategory/{categoryId}")
	public Object deleteCategoryById(@PathVariable ObjectId categoryId) {
		return itemCategoryService.deleteItemCategoryById(categoryId);
	}

//***********************************************************************************
//	Item Sub Category	
//***********************************************************************************
	@GetMapping("/itemSubcategory")
	public Object getItemSubcategory(@RequestParam(required = false) String[] filter, String search,
			Suggestions suggestions, Integer offset, Integer count) {
		QueryString queryString = new QueryString();
		queryString.setFilter(filter);
		queryString.setSearch(search);
		if (offset == null || count == null) {
			return itemSubCategoryService.getItemSubCategoryPage(queryString, suggestions, null);
		} else {
			return itemSubCategoryService.getItemSubCategoryPage(queryString, suggestions,
					PageRequest.of(offset, count));
		}
	}

	@GetMapping("/itemSubcategory/{subCategoryId}")
	public Object getItemSubCategoryById(@PathVariable ObjectId subCategoryId) {
		return itemSubCategoryService.getItemSubCategoryById(subCategoryId);
	}

	@PostMapping("/itemSubcategory")
	public Object addItemSubCategory(@RequestBody ItemSubCategory itemSubcategory) {
		return itemSubCategoryService.addItemSubCategoryAdmin(itemSubcategory);
	}

	@PutMapping("/itemSubcategory")
	public Object updateItemSubCategory(@RequestBody ItemSubCategory itemSubcategory) {
		return itemSubCategoryService.updateSubCategoryAdmin(itemSubcategory);
	}

	@DeleteMapping("/itemSubcategory/{subCategoryId}")
	public Object deleteSubCategoryById(@PathVariable ObjectId subCategoryId) {
		return itemSubCategoryService.deleteItemSubCategoryById(subCategoryId);
	}

//***********************************************************************************
//	Image Service	
//***********************************************************************************
	@PostMapping("/imageUpload")
	public Object uploadImage(@RequestPart("images") List<MultipartFile> images, @RequestPart("type") String type,
			@RequestPart("id") String id) {
		return imageService.addNewImage(images, type, id);
	}

	@PutMapping("/imageUpload")
	public Object updateImage(@RequestPart("images") String images, @RequestPart("type") String type,
			@RequestPart("id") String id) {
		// images should be the one that need to be deleted
		return imageService.updateImage(images, type, id);
	}

//***********************************************************************************
//	UserOrder
//***********************************************************************************
	@GetMapping("/userOrder")
	public Object getUserOrder(@RequestParam(required = false) String[] filter, Integer offset, Integer count) {
		if (offset == null || count == null) {
			return userOrderService.getAllUserOrderPageAdmin(filter, null);
		} else {
			return userOrderService.getAllUserOrderPageAdmin(filter, PageRequest.of(offset, count));
		}
	}

	@GetMapping("/userOrder/{userOrderId}")
	public Object getUserOrderById(@PathVariable ObjectId userOrderId) {
		return userOrderService.getUserOrderAdmin(userOrderId);
	}

	@PutMapping("/userOrder")
	public Object updateItemSubCategory(@RequestBody UserOrderSet userOrder) {
		return userOrderService.updateUserOrderSet(userOrder);
	}

//***********************************************************************************
//	HomePage
//***********************************************************************************
	@PostMapping("/homePage")
	public HomePage addHomePage(@RequestBody HomePage homePage) {
		return homePageService.addHomePage(homePage);
	}

	@PutMapping("/homePage")
	public HomePage updateHomePage(@RequestBody HomePage homePage) {
		return homePageService.updateHomePage(homePage);
	}

	@GetMapping("/homePage")
	public Object getAllHomePage(Integer offset, Integer count) {
		if (offset == null || count == null) {
			return homePageService.getHomePage();
		} else {
			return homePageService.getHomePage(count, offset);
		}
	}

	@GetMapping("/homePage/{id}")
	public HomePage getHomePageById(@PathVariable ObjectId id) {
		return homePageService.getHomePageById(id);
	}

	@DeleteMapping("/homePage/{id}")
	public void deleteHomePageById(@PathVariable ObjectId id) {
		homePageService.deleteHomePage(id);
	}

//***********************************************************************************
//	AdminPayementGatewayKey Service	
//***********************************************************************************	

	@GetMapping("/adminPayementGatewayKey")
	public Page<AdminPayementGatewayKey> getAdminPayementGatewayKey(Integer offset, Integer count) {
		if (offset == null || count == null) {
			return adminPayementGatewayKeyService.getAdminPayementGatewayKey(null);
		} else {
			return adminPayementGatewayKeyService.getAdminPayementGatewayKey(PageRequest.of(offset, count));
		}
	}

	@GetMapping("/adminPayementGatewayKey/{adminPayementGatewayKeyId}")
	public AdminPayementGatewayKey getAdminPayementGatewayKeyById(@PathVariable ObjectId adminPayementGatewayKeyId) {
		return adminPayementGatewayKeyService.getAdminPayementGatewayKeyById(adminPayementGatewayKeyId);
	}

	@PostMapping("/adminPayementGatewayKey")
	public AdminPayementGatewayKey addAdminPayementGatewayKey(
			@RequestBody AdminPayementGatewayKey adminPayementGatewayKey) {
		return adminPayementGatewayKeyService.addAdminPayementGatewayKey(adminPayementGatewayKey);
	}

	@PutMapping("/adminPayementGatewayKey")
	public AdminPayementGatewayKey updateAdminPayementGatewayKeyById(
			@RequestBody AdminPayementGatewayKey adminPayementGatewayKey) {
		return adminPayementGatewayKeyService.updateAdminPayementGatewayKey(adminPayementGatewayKey);
	}

	@DeleteMapping("/adminPayementGatewayKey/{adminPayementGatewayKeyId}")
	public Object deleteAdminPayementGatewayKeyById(@PathVariable ObjectId adminPayementGatewayKeyId) {
		return adminPayementGatewayKeyService.deleteAdminPayementGatewayKeyById(adminPayementGatewayKeyId);
	}

//***********************************************************************************
//PaymentGateway Service	
//***********************************************************************************	

	@GetMapping("/paymentGateway")
	public List<PaymentGateway> getPaymentGateway() {
		return paymentGatewayService.getPaymentGateway();
	}

	@GetMapping("/paymentGateway/{gatewayId}")
	public PaymentGateway getPaymentGatewayById(@PathVariable ObjectId gatewayId) {
		return paymentGatewayService.getPaymentGatewayById(gatewayId);
	}

	@PostMapping("/paymentGateway")
	public PaymentGateway addPaymentGateway(@RequestBody PaymentGateway paymentGateway) {
		return paymentGatewayService.addPaymentGateway(paymentGateway);
	}

	@DeleteMapping("/paymentGateway/{gatewayId}")
	public Object deletePaymentGatewayById(@PathVariable ObjectId gatewayId) {
		return paymentGatewayService.deletePaymentGatewayById(gatewayId);
	}

// ***********************************************************************************
// PaymentMetadata Service
// ***********************************************************************************

	@GetMapping("/paymentSupportTypes")
	public List<PaymentProcess> getPaymentSupportTypes() {
		return paymentProcessServices.getPaymentSupportTypes();
	}

// ***********************************************************************************
// PaymentMetadata Service
// ***********************************************************************************

	@GetMapping("/paymentGatewayMetadata")
	public PaymentMetadata getPaymentMetaData(@RequestBody CreateOrderInput createOrderInput) {
		return paymentProcessServices.getPaymentMetadata(createOrderInput);
	}

// ***********************************************************************************
// GenerateApp Service
// ***********************************************************************************
	@PostMapping("/generateApp")
	public AdminAppDetails generateApp(@RequestPart("image") Optional<MultipartFile> image,
			@RequestPart("appDetails") GenerateApp generateApp) {
//		if(image.orElse(null) != null) {
//			MultipartFile file = image.get();
//		}
		return adminAppDetailsService.triggerBuildPipeline(generateApp, image.orElse(null));
	}

//	@GetMapping("/checkStatus")
//	public BuildStatus checkBuildStatus() {
//		return adminAppDetailsService.checkBuildStatus();
//	}

// ***********************************************************************************
// Admin App Details 
// ***********************************************************************************
	@GetMapping("/appDetails")
	public AdminAppDetails getAdminAppDetails() {
		return adminAppDetailsService.getAdminAppDetails();
	}

	@GetMapping("/themes")
	public List<Theme> getAvailableTheme() {
		return themeService.getAllThemes();
	}

}
