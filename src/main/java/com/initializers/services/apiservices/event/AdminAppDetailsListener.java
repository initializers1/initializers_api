package com.initializers.services.apiservices.event;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.AdminAppDetails;

@Component
public class AdminAppDetailsListener extends AbstractMongoEventListener<AdminAppDetails> {
	
	@Override
    public void onBeforeConvert(BeforeConvertEvent<AdminAppDetails> event) {
		if(event.getSource().getPipelineStatus() == Status.pipelineTriggered) {
			event.getSource().setTriggeredTime(LocalDateTime.now());
//			reset to initial value
			event.getSource().setExpoBuildUrl(null);
			event.getSource().setPipelineId(0);
			
		}
		event.getSource().setRefreshTime(LocalDateTime.now());
    }
}
