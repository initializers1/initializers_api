package com.initializers.services.apiservices.event;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.exception.RequiredValueMissingException;
import com.initializers.services.apiservices.model.AdminClientDetails;

@Component
public class AdminClientDetailsListener extends AbstractMongoEventListener<AdminClientDetails>{
	
	private PasswordEncoder passwordEncoder;
	
	public AdminClientDetailsListener(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
	
	@Override
    public void onBeforeConvert(BeforeConvertEvent<AdminClientDetails> event) {
		if(event.getSource().getPassword() != null) {
			try {
				event.getSource().setPassword(passwordEncoder.encode(event.getSource().getPassword()));
				event.getSource().setEnabled(true);
				event.getSource().setAccountExpired(false);
				event.getSource().setAccountLocked(false);
				event.getSource().setCredentialsExpired(false);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RequiredValueMissingException();
			}
		}
    }
	
}
