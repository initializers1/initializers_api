package com.initializers.services.apiservices.event;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;

import com.initializers.services.apiservices.model.elasticsearch.ItemCategoryES;
import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.repo.elasticsearch.ItemCategoryElasticSearchRepo;

public class ItemCategoryModelListener extends AbstractMongoEventListener<ItemCategory> {
	@Autowired
	ItemCategoryElasticSearchRepo itemCategoryElasticSearchRepo;
	
	
	@Override
	public void onAfterSave(AfterSaveEvent<ItemCategory> event) {
		if(event.getSource() != null) {
//			Insert item to elastic search
			ItemCategoryES itemCategoryEs = new ItemCategoryES();
			BeanUtils.copyProperties(event.getSource(), itemCategoryEs);
			itemCategoryElasticSearchRepo.insertItemCategoryForAlias(itemCategoryEs, TenantContext.getTenantId());
		}
	}
}
