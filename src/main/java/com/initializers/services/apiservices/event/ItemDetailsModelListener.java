package com.initializers.services.apiservices.event;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.repo.elasticsearch.ItemDetailsElasticSearchCustomRepo;

@Component
public class ItemDetailsModelListener  extends AbstractMongoEventListener<ItemDetails>{
	
	@Autowired
	ItemDetailsElasticSearchCustomRepo itemDetailsElasticSearchCustomRepo;
	
	
	@Override
	public void onAfterSave(AfterSaveEvent<ItemDetails> event) {
		if(event.getSource() != null) {
//			Insert item to elastic search
			ItemDetailsES itemDetailsEs = new ItemDetailsES();
			BeanUtils.copyProperties(event.getSource(), itemDetailsEs);
			itemDetailsElasticSearchCustomRepo.insertItemDetailsForAlias(itemDetailsEs, TenantContext.getTenantId());
		}
	}
}
