package com.initializers.services.apiservices.event;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterLoadEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.crypto.EncryptorAesGcm;
import com.initializers.services.apiservices.exception.RequiredValueMissingException;
import com.initializers.services.apiservices.model.TransientUserDetails;

@Component
public class TransientUserDetailsListener extends AbstractMongoEventListener<TransientUserDetails>{

	@Override
    public void onBeforeConvert(BeforeConvertEvent<TransientUserDetails> event) {
		if(event.getSource().getPassword() != null) {
			try {
				String cryptText = EncryptorAesGcm.encrypt(event.getSource().getPassword().getBytes());
				event.getSource().setPassword(cryptText);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RequiredValueMissingException();
			}
		}
    }
	
	@Override
	public void onAfterLoad(AfterLoadEvent<TransientUserDetails> event) {
		try {
			String plainText = EncryptorAesGcm.decrypt(event.getSource().get("password").toString());
			event.getSource().put("password", plainText);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RequiredValueMissingException();
		}
	}
}
