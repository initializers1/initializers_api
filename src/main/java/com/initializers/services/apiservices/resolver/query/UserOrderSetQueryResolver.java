package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.exception.UserNotFoundException;
import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.UserOrderSet;
import com.initializers.services.apiservices.model.item.ItemAvailability;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.service.AuthenticationFacade;
import com.initializers.services.apiservices.service.UserOrderService;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class UserOrderSetQueryResolver implements GraphQLQueryResolver {

	@Autowired
	private UserOrderService userOrderService;
	@Autowired
	private AuthenticationFacade authenticationFacade;

	public Connection<UserOrderSet> getUserOrderSet(Integer first, String after, ObjectId userId) {
		Integer afterInt = Integer.parseInt(after);
		Pageable page = PageRequest.of(afterInt, first);
		AdminClientDetails adminClientDetails = (AdminClientDetails) authenticationFacade.getAuthentication().getPrincipal();
		if(adminClientDetails.getUserId() == null) {
			throw new UserNotFoundException();
		}
		Page<UserOrderSet> userOrderPage = userOrderService.getUserOrderList(adminClientDetails.getUserId(), page);
		Connection<UserOrderSet> con = new Connection<UserOrderSet>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return userOrderPage.hasPrevious();
					}

					@Override
					public boolean isHasNextPage() {
						return userOrderPage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(String.valueOf(userOrderPage.getNumber() + 1));
					}
				};
				return page;
			}

			@Override
			public List<Edge<UserOrderSet>> getEdges() {
				List<Edge<UserOrderSet>> userOrderEdgeList = new ArrayList<Edge<UserOrderSet>>();
				for (UserOrderSet userOrder : userOrderPage.getContent()) {
					Edge<UserOrderSet> itemCategoryEdge = new Edge<UserOrderSet>() {

						@Override
						public UserOrderSet getNode() {
//							String itemName = null;
							userOrder.setOrderDetails(StringUtils.join(userOrder.getItemNames(), ","));
							userOrder.getStatus().setPayment(userOrder.getPaymentStatus());
//							for (UserOrder order : userOrder.getOrderList()) {
//								ItemDetails itemDetails = itemDetailsService
//										.getItemNameImageLinkById(order.getItemId());
//								String getName = StringUtils.capitalize(itemDetails.getName());
//								if (itemName == null) {
//									itemName = getName;
//								} else {
//									itemName = itemName + ", " + getName;
//								}
//							}
//							userOrder.setOrderDetails(itemName);
							return userOrder;
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					userOrderEdgeList.add(itemCategoryEdge);
				}
				return userOrderEdgeList;
			}

		};
		return con;
	}

	public UserOrderSet getUserOrderById(ObjectId orderId) {
		Float netAmount;
		UserOrderSet userOrderSet = userOrderService.getUserOrder(orderId);
		userOrderSet.getOrderList().forEach(userOrder -> {
			if(userOrderSet.getItemAvailability() != null) {
				ItemAvailability itemAvailability = userOrderSet.getItemAvailability().stream().
						filter(availability -> userOrder.getAvailabilityId().equals(availability.getId())).
						findFirst().orElse(null);
				if(itemAvailability != null) {
					userOrder.setValue(itemAvailability.getValue());
					userOrder.setUnit(itemAvailability.getUnit());
				}				
			}
			if(userOrderSet.getItemDetails() != null) {
				ItemDetails itemDetails = userOrderSet.getItemDetails().stream().
						filter(item -> userOrder.getItemId().equals(item.getId())).
						findFirst().orElse(null);
				if(itemDetails != null) {
					userOrder.setItemName(itemDetails.getName());
					if(itemDetails.getImageLinks() != null && itemDetails.getImageLinks().size() > 0) {
						userOrder.setImageLink(itemDetails.getImageLinks().get(0));						
					}
				}
			}
		});
		userOrderSet.getStatus().setPayment(userOrderSet.getPaymentStatus());
		netAmount = userOrderSet.getTotalAmount();
		if(userOrderSet.getDeliveryCharge() != null) {
			netAmount -= userOrderSet.getDeliveryCharge();
		}
		if(userOrderSet.getCoupenDiscount() != null) {
			netAmount -= userOrderSet.getCoupenDiscount();
		}
		userOrderSet.setNetAmount(netAmount);
		return userOrderSet;
	}
}
