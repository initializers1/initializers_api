package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.model.UserWishlist;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.UserWishlistService;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class ItemDetailsQueryResolver implements GraphQLQueryResolver {

	@Autowired
	ItemDetailsService itemDetailsService;
	@Autowired
	UserWishlistService userWishlistService;

	public Connection<ItemDetails> getItemDetails(Integer first, String after, String itemType, ObjectId typeId,
			ObjectId userId) {
		Integer afterInt = Integer.parseInt(after);
		Pageable page = PageRequest.of(afterInt, first);

		Page<ItemDetails> itemDetailsPage;
		itemDetailsPage = itemDetailsService.getItemDetailsByType(typeId, page, itemType);

		return createItemDetailsConnection(itemDetailsPage, userId);
	}

	public ItemDetails getItemDetailsById(ObjectId itemId, ObjectId userId) {
		ItemDetails itemDetails = itemDetailsService.getItemDetails(userId, itemId);
		return itemDetails;
	}

	public Connection<ItemDetails> createItemDetailsConnection(Page<ItemDetails> itemDetailsPage, ObjectId userId) {
		UserWishlist userWishlist = userWishlistService.getUserWishlist(userId);
		Connection<ItemDetails> con = new Connection<ItemDetails>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return false;
					}

					@Override
					public boolean isHasNextPage() {
						return itemDetailsPage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(String.valueOf(itemDetailsPage.getNumber() + 1));
					}
				};
				return page;
			}

			@Override
			public List<Edge<ItemDetails>> getEdges() {
				List<Edge<ItemDetails>> itemDetailsEdgeList = new ArrayList<Edge<ItemDetails>>();
				for (ItemDetails itemDetails : itemDetailsPage.getContent()) {
					Edge<ItemDetails> itemDetailsEdge = new Edge<ItemDetails>() {

						@Override
						public ItemDetails getNode() {
							// set user wishlist
							if (userWishlist != null && userWishlist.getItemsId().contains(itemDetails.getId())) {
								itemDetails.setIsWishlist(true);
							} else {
								itemDetails.setIsWishlist(false);
							}
							// set id hash
//							itemDetails.setItemAvailability(itemAvailabilityService.
//							getAvailabilityByItemId(itemDetails.getId(), 'N'));
							if (itemDetails.getImageLinks() != null && itemDetails.getImageLinks().size() > 0) {
								itemDetails.setImageLink(itemDetails.getImageLinks().get(0));
							}
							return itemDetails;
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					itemDetailsEdgeList.add(itemDetailsEdge);
				}
				return itemDetailsEdgeList;
			}
		};
		return con;
	}

}
