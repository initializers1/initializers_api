package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.model.output.UserReviewOutput;
import com.initializers.services.apiservices.service.UserDetailsService;
import com.initializers.services.apiservices.service.UserReviewService;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class UserReviewQueryResolver  implements GraphQLQueryResolver{
	@Autowired
	private UserReviewService userReviewService;
	@Autowired
	private UserDetailsService userDetailsService;
	
	public Connection<UserReviewOutput> getItemReview(Integer first, String after, ObjectId itemId) {
		Pageable page = PageRequest.of(Integer.parseInt(after), first);
		Page<UserReviewOutput> userReviewPage = userReviewService.findReviewByItemId(itemId, page);
		 
	Connection<UserReviewOutput> con = new Connection<UserReviewOutput>() {
			
			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {
					
					@Override
					public boolean isHasPreviousPage() {
						return userReviewPage.hasPrevious();
					}
					
					@Override
					public boolean isHasNextPage() {
						return userReviewPage.hasNext();
					}
					
					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}
					
					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(
								String.valueOf(userReviewPage.getNumber() + 1)
								);
					} 
				};
				return page;
			}
			
			@Override
			public List<Edge<UserReviewOutput>> getEdges() {
				List<Edge<UserReviewOutput>> userReviewEdgeList = new ArrayList<Edge<UserReviewOutput>>();
				for(UserReviewOutput userReview : userReviewPage.getContent()) {
					Edge<UserReviewOutput> userReviewEdge = new Edge<UserReviewOutput>() {
						
						@Override
						public UserReviewOutput getNode() {
							userReview.setId("userReview===="+userReview.getUserId()+userReview.getItemId());
							return userReview;
						}
						
						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {
								
								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					userReviewEdgeList.add(userReviewEdge);
				}
				return userReviewEdgeList;
			}
			
	};
			return con;
		
	}
}
