package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.model.item.ItemSubCategory;
import com.initializers.services.apiservices.service.ItemSubCategoryService;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class ItemSubCategoryQueryResolver implements GraphQLQueryResolver {

	@Autowired
	private ItemSubCategoryService itemSubCategoryService;

	public Connection<ItemSubCategory> getItemSubCategoryByCategoryId(Integer first, String after, ObjectId categoryId) {
		Pageable page = PageRequest.of(Integer.parseInt(after), first);
		Page<ItemSubCategory> itemSubCategoryPage = itemSubCategoryService.getItemSubCategoryByCategoryId(categoryId,
				page);
		Connection<ItemSubCategory> con = new Connection<ItemSubCategory>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return itemSubCategoryPage.hasPrevious();
					}

					@Override
					public boolean isHasNextPage() {
						return itemSubCategoryPage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(String.valueOf(itemSubCategoryPage.getNumber() + 1));
					}
				};
				return page;
			}

			@Override
			public List<Edge<ItemSubCategory>> getEdges() {
				List<Edge<ItemSubCategory>> itemSubCategoryEdgeList = new ArrayList<Edge<ItemSubCategory>>();
				for (ItemSubCategory ItemSubCategory : itemSubCategoryPage.getContent()) {
					Edge<ItemSubCategory> itemSubCategoryEdge = new Edge<ItemSubCategory>() {

						@Override
						public ItemSubCategory getNode() {
							return ItemSubCategory;
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					itemSubCategoryEdgeList.add(itemSubCategoryEdge);
				}
				return itemSubCategoryEdgeList;
			}

		};
		return con;
	}
}
