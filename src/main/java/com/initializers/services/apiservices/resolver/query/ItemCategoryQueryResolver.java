package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.service.ItemCategoryService;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class ItemCategoryQueryResolver implements GraphQLQueryResolver {

	@Autowired
	ItemCategoryService itemCategoryService;

	public Connection<ItemCategory> getItemCategory(Integer first, String after) {
		DefaultConnectionCursor cursor = new DefaultConnectionCursor(after);
		System.out.println(cursor.toString());
		Integer afterInt = Integer.parseInt(after);
		Page<ItemCategory> itemCategoryPage = itemCategoryService.getItemCategory(first, afterInt);
		Connection<ItemCategory> con = new Connection<ItemCategory>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return itemCategoryPage.hasPrevious();
					}

					@Override
					public boolean isHasNextPage() {
						return itemCategoryPage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(String.valueOf(itemCategoryPage.getNumber() + 1));
					}
				};
				return page;
			}

			@Override
			public List<Edge<ItemCategory>> getEdges() {
				List<Edge<ItemCategory>> itemCategoryEdgeList = new ArrayList<Edge<ItemCategory>>();
				for (ItemCategory itemCategory : itemCategoryPage.getContent()) {
					Edge<ItemCategory> itemCategoryEdge = new Edge<ItemCategory>() {

						@Override
						public ItemCategory getNode() {
							return itemCategory;
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					itemCategoryEdgeList.add(itemCategoryEdge);
				}
				return itemCategoryEdgeList;
			}

		};
		return con;
	}

	public Connection<ItemCategory> getItemCategoryContentById(Integer first, String after, ObjectId categoryId) {
		Integer afterInt = Integer.parseInt(after);
		if (first == 0) {
			return null;
		}
		Page<ItemCategory> itemCategoryPage = itemCategoryService.getItemCategory(first, afterInt);
		Connection<ItemCategory> con = new Connection<ItemCategory>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return itemCategoryPage.hasPrevious();
					}

					@Override
					public boolean isHasNextPage() {
						return itemCategoryPage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						// TODO Auto-generated method stub
						return new DefaultConnectionCursor(String.valueOf(itemCategoryPage.getNumber() + 1));
					}
				};
				return page;
			}

			@Override
			public List<Edge<ItemCategory>> getEdges() {
				List<Edge<ItemCategory>> itemCategoryEdgeList = new ArrayList<Edge<ItemCategory>>();
				for (ItemCategory itemCategory : itemCategoryPage.getContent()) {
					Edge<ItemCategory> itemCategoryEdge = new Edge<ItemCategory>() {

						@Override
						public ItemCategory getNode() {
							return itemCategory;
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					itemCategoryEdgeList.add(itemCategoryEdge);
				}
				return itemCategoryEdgeList;
			}

		};
		return con;
	}
}
