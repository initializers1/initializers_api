package com.initializers.services.apiservices.resolver.query;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.model.UserDetails;
import com.initializers.services.apiservices.service.AdminClientDetailsService;
import com.initializers.services.apiservices.service.UserDetailsService;

@Component
public class UserDetailsQueryResolver implements GraphQLQueryResolver {
	
	@Autowired
	UserDetailsService userDetailsService;
	@Autowired
	AdminClientDetailsService adminClientDetailsService;
	
	public UserDetails getUserById(String id) {
		UserDetails userDetail = userDetailsService.getUser(new ObjectId(id));
		userDetail.setEmail(adminClientDetailsService.getClientEmail(userDetail.getId()));
		return userDetail;
	}

}
