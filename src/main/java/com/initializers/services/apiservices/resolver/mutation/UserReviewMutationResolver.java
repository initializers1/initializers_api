package com.initializers.services.apiservices.resolver.mutation;

import java.time.LocalDate;

import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.initializers.services.apiservices.exception.ItemNotFoundException;
import com.initializers.services.apiservices.exception.UserNotFoundException;
import com.initializers.services.apiservices.model.UserReview;
import com.initializers.services.apiservices.model.output.UserReviewOutput;
import com.initializers.services.apiservices.repo.UserReviewRepo;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.UserDetailsService;

@Component
public class UserReviewMutationResolver implements GraphQLMutationResolver {

	@Autowired
	UserReviewRepo userReviewRepo;
	@Autowired
	UserDetailsService userDetailsService;
	@Autowired
	ItemDetailsService itemDetailsService;

	public UserReviewOutput addItemReview(ObjectId itemId, ObjectId userId, int rating, String review) {
		if (userDetailsService.getUser(userId) == null) {
			throw new UserNotFoundException("User not found");
		} else if (!itemDetailsService.checkItemDetails(itemId)) {
			throw new ItemNotFoundException("Item not found");
		} else {
			UserReviewOutput userReviewOutput = new UserReviewOutput();
			UserReview.CompositeKey key = new UserReview.CompositeKey(userId, itemId);
			UserReview userReview = new UserReview(key, rating, review, LocalDate.now(), null, null);
			userReviewRepo.save(userReview);
			BeanUtils.copyProperties(userReview, userReviewOutput);
			userReviewOutput.setId("userReview====" + userId + itemId);
			return userReviewOutput;

		}
	}

}
