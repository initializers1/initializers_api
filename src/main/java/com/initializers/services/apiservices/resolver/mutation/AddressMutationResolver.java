package com.initializers.services.apiservices.resolver.mutation;

import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.initializers.services.apiservices.model.Address;
import com.initializers.services.apiservices.model.input.AddressInput;
import com.initializers.services.apiservices.model.output.AddressOutput;
import com.initializers.services.apiservices.service.AddressService;

@Component
public class AddressMutationResolver implements GraphQLMutationResolver{

	@Autowired
	AddressService addressService;
	
	public AddressOutput upsertAddress(AddressInput addressInput) {
		Address address = new Address();
		if(addressInput.getId() != null) {
			BeanUtils.copyProperties(addressInput, address);
			address = addressService.updateAddress(address);
		} else {
			BeanUtils.copyProperties(addressInput, address);
			address = addressService.addAddress(address);
		}
		AddressOutput addressOutput = new AddressOutput();
		addressOutput.setAddress(address);
		return addressOutput;
	}
	
	public Boolean deleteAddress(ObjectId id) {
		return addressService.deleteAddressById(id);
	}
}
