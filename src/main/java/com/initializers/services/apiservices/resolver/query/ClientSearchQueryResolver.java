package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.elasticsearch.GlobalSearchFactroy;
import com.initializers.services.apiservices.model.ClientSearchResult;
import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.repo.elasticsearch.ItemDetailsElasticSearchCustomRepo;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class ClientSearchQueryResolver implements GraphQLQueryResolver {

	@Autowired
	ItemDetailsElasticSearchCustomRepo itemDetailsElasticSearchRepo;
	@Autowired
	GlobalSearchFactroy globalSearchFactory;
	
	
	public List<ClientSearchResult> searchAsList(String searchTerm) {
		List<ClientSearchResult> result = new ArrayList<ClientSearchResult>();
		List<ItemDetailsES> itemDetailsES = globalSearchFactory.searchItemDetails(searchTerm);
		if (itemDetailsES != null) {
			itemDetailsES.stream()
					.forEach(itemDetails -> result.add(new ClientSearchResult(
							itemDetails.getId().toHexString() + "_search", itemDetails.getId(), itemDetails.getName(),
							itemDetails.getDescription().getItemProperties(), "ItemDetails", null)));
			return result;
		}
		return null;
	}

	public Connection<ClientSearchResult> search(Integer first, String after, String searchTerm) {
		if(searchTerm.equals("")) {
			return null;
		}
		Integer afterInt = Integer.parseInt(after);
		Pageable page = PageRequest.of(afterInt, first);

		Page<ItemDetailsES> searchResultPage = itemDetailsElasticSearchRepo.searchItemDetailsForAlias(searchTerm, page);
		Connection<ClientSearchResult> con = new Connection<ClientSearchResult>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return searchResultPage.hasPrevious();
					}

					@Override
					public boolean isHasNextPage() {
						return searchResultPage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(String.valueOf(searchResultPage.getNumber() + 1));
					}
				};
				return page;
			}

			@Override
			public List<Edge<ClientSearchResult>> getEdges() {
				List<Edge<ClientSearchResult>> itemCategoryEdgeList = new ArrayList<Edge<ClientSearchResult>>();
				System.out.println(searchResultPage.getContent());
				Object content = searchResultPage.getContent();
				
				@SuppressWarnings({ "rawtypes", "unchecked" })
				List<SearchHit> searchHits = (List<SearchHit>) content;
				
				for (SearchHit<ItemDetailsES> searchResult : searchHits) {
					Edge<ClientSearchResult> itemCategoryEdge = new Edge<ClientSearchResult>() {

						@Override
						public ClientSearchResult getNode() {
							ItemDetailsES itemDetails = searchResult.getContent();
							return new ClientSearchResult(itemDetails.getId().toHexString()+"_search",itemDetails.getId(), itemDetails.getName(),
									itemDetails.getDescription().getItemProperties(), "ItemDetails", null);
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					itemCategoryEdgeList.add(itemCategoryEdge);
				}
				return itemCategoryEdgeList;
			}

		};
		return con;
	}

}
