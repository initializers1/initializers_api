package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.model.UserCartList;
import com.initializers.services.apiservices.repo.UserCartRepo;
import com.initializers.services.apiservices.service.UserCartService;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class UserCartQueryResolver implements GraphQLQueryResolver {
	
	@Autowired
	UserCartService userCartService;
	@Autowired
	UserCartRepo userCartRepo;
	
	public Connection<UserCartList> getUserCart(Integer first, String after, ObjectId userId) {
		Pageable pageable = PageRequest.of(Integer.parseInt(after), first);
//		ConnectionCursor connectionCur = new C
		Page<UserCartList> userCartPage = userCartService.getUserCartByuserId(userId, pageable);
		Connection<UserCartList> con = new Connection<UserCartList>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return userCartPage.hasPrevious();
					}

					@Override
					public boolean isHasNextPage() {
						return userCartPage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(
								String.valueOf(userCartPage.getNumber() + 1)
								);
					}
				};
				return page;
			}

			@Override
			public List<Edge<UserCartList>> getEdges() {
				List<Edge<UserCartList>> userCartEdgeList = new ArrayList<Edge<UserCartList>>();
				for (UserCartList userCart : userCartPage.getContent()) {
					Edge<UserCartList> userCartEdge = new Edge<UserCartList>() {

						@Override
						public UserCartList getNode() {
//							@SuppressWarnings("unchecked")
//							ArrayList<String> imageLinks = (ArrayList<String>) userCart.getImageLinks();
							if(userCart.getImageLinks().size() > 0 ) userCart.setImageLink(userCart.getImageLinks().get(0).toString());
							userCart.setDiscountPrice(userCart.getDiscountPrice() * userCart.getQuantity());
							userCart.setId("UserCartKey=="+userCart.getItemId()+userCart.getAvailabilityId());
							return userCart;
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					userCartEdgeList.add(userCartEdge);
				}
				return userCartEdgeList;
			}

		};
		return con;
	}

	public String checkOut() {
//		TODO: Check item stock
//		this should return payment metadata 
		return "Card";
	}
}
