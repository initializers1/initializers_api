package com.initializers.services.apiservices.resolver.global;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.initializers.services.apiservices.model.item.ItemDetails;

@Component
public class ItemDetailsResolver implements GraphQLResolver<ItemDetails> {

}
