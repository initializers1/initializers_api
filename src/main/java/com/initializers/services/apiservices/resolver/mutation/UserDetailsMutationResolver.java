package com.initializers.services.apiservices.resolver.mutation;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.initializers.services.apiservices.model.UserDetails;
import com.initializers.services.apiservices.model.input.UserDetailsInput;
import com.initializers.services.apiservices.model.output.UserDetailsOutput;
import com.initializers.services.apiservices.service.UserDetailsService;

@Component
public class UserDetailsMutationResolver implements GraphQLMutationResolver{

	@Autowired
	UserDetailsService userDetailsService;
	
	public UserDetailsOutput updateUser(UserDetailsInput userDetailsInput) {
		UserDetails userDetails = new UserDetails();   
		BeanUtils.copyProperties(userDetailsInput, userDetails);
		userDetails = userDetailsService.updateUserDetail(userDetails);
		UserDetailsOutput userDetailsOutput = new UserDetailsOutput();
		userDetailsOutput.setUserDetails(userDetails);
		return userDetailsOutput;
	}
}
