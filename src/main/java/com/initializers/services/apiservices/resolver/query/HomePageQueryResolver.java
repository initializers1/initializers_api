package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.model.HomePage;
import com.initializers.services.apiservices.model.HomePageContent;
import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.model.item.ItemSubCategory;
import com.initializers.services.apiservices.service.HomePageService;
import com.initializers.services.apiservices.service.ItemCategoryService;
import com.initializers.services.apiservices.service.ItemDetailsService;
import com.initializers.services.apiservices.service.ItemSubCategoryService;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class HomePageQueryResolver implements GraphQLQueryResolver {

	@Autowired
	HomePageService homePageService;
	@Autowired
	ItemCategoryService itemCategoryService;
	@Autowired
	ItemSubCategoryService itemSubCategoryService;
	@Autowired
	ItemDetailsService itemDetailsService;

	public Connection<HomePage> getHomePage(Integer first, String after) {
		Integer afterInt = Integer.parseInt(after);
//		ConnectionCursor connectionCur = new C
		Page<HomePage> homePage = homePageService.getHomePage(first, afterInt);
		Connection<HomePage> con = new Connection<HomePage>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return homePage.hasPrevious();
					}

					@Override
					public boolean isHasNextPage() {
						return homePage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(String.valueOf(homePage.getNumber() + 1));
					}
				};
				return page;
			}

			@Override
			public List<Edge<HomePage>> getEdges() {
				List<Edge<HomePage>> homePageEdgeList = new ArrayList<Edge<HomePage>>();
				for (HomePage homepage : homePage.getContent()) {
					Edge<HomePage> homePageEdge = new Edge<HomePage>() {

						@Override
						public HomePage getNode() {
							List<HomePageContent> homePageContentList = new ArrayList<>();
							if (homepage.getItemType().equals("ItemSubCategory")) {
								List<ItemSubCategory> itemSubCategory = itemSubCategoryService
										.getItemSubCategoryByCategoryId(homepage.getTypeId());
								itemSubCategory.stream().forEach(subCategory -> {
									homePageContentList.add(new HomePageContent(subCategory.getId(),
											subCategory.getName(), subCategory.getDescription(), subCategory.getOffer(),
											subCategory.getImageLink()));
								});
							} else if (homepage.getItemType().equals("ItemDetails")) {
								List<ItemDetails> itemDetails = itemDetailsService
										.getItemDetailsBySubCategory(homepage.getTypeId());
								itemDetails.stream().forEach(details -> {
									HomePageContent homePageContent = new HomePageContent();
									homePageContent.setId(details.getId());
									homePageContent.setName(details.getName());
									if (details.getImageLinks() != null && details.getImageLinks().size() > 0) {
										homePageContent.setImageLink(details.getImageLinks().get(0));
									}
									homePageContentList.add(homePageContent);
								});
							} else if (homepage.getItemType().equals("ItemCategory")) {
								List<ItemCategory> itemCategory = itemCategoryService.getItemCategory();
								itemCategory.stream().forEach(category -> {
									homePageContentList.add(new HomePageContent(category.getId(), category.getName(),
											category.getDescription(), category.getOffer(), category.getImageLink()));
								});
							}
							homepage.setContent(homePageContentList);
							return homepage;
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					homePageEdgeList.add(homePageEdge);
				}
				return homePageEdgeList;
			}

		};
		return con;
	}

}
