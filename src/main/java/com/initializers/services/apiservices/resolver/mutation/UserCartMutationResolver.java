package com.initializers.services.apiservices.resolver.mutation;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.initializers.services.apiservices.constant.PaymentConstants;
import com.initializers.services.apiservices.model.OrderStatus;
import com.initializers.services.apiservices.model.UserCart;
import com.initializers.services.apiservices.model.UserCartList;
import com.initializers.services.apiservices.model.UserOrderSet;
import com.initializers.services.apiservices.model.input.CreateOrderInput;
import com.initializers.services.apiservices.model.input.UserCartInput;
import com.initializers.services.apiservices.model.output.CreateOrderOutput;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;
import com.initializers.services.apiservices.payment.PaymentGatewayFactory;
import com.initializers.services.apiservices.repo.UserOrderSetRepo;
import com.initializers.services.apiservices.service.UserCartService;

@Component
public class UserCartMutationResolver implements GraphQLMutationResolver {

	@Autowired
	UserCartService userCartService;
	@Autowired
	private PaymentGatewayFactory paymentGatewayFactory;
	@Autowired
	private UserOrderSetRepo userOrderSetRepo;

	public UserCartList addUserCart(UserCartInput userCartInput) {
		UserCart userCart = new UserCart();
		BeanUtils.copyProperties(userCartInput, userCart);
		userCart = userCartService.addUserCart(userCart);
		UserCartList userCartList = new UserCartList();
		BeanUtils.copyProperties(userCart, userCartList);
		userCartList.setId("UserCartKey==" + userCart.getId().getItemId() + userCart.getId().getAvailabilityId());
		userCartList.setUserId(userCart.getId().getUserId());
		userCartList.setItemId(userCart.getId().getItemId());
		userCartList.setAvailabilityId(userCart.getId().getAvailabilityId());
		return userCartList;
	}

//	
//	public CreateOrderOutput createOrderFromCart(CreateOrderInput input) {
//		return userCartService.createOrder(input);
//	}
//	
	public CreateOrderOutput createOrderFromCart(CreateOrderInput createOrderInput) {

		PaymentMetadata paymentMetadata = new PaymentMetadata();
		CreateOrderOutput createOrderOutput = new CreateOrderOutput();

		if(createOrderInput.getOrderId() == null) {
			createOrderOutput = userCartService.createOrder(createOrderInput);			
		} else {
			UserOrderSet userOrderSet = userOrderSetRepo.getMinimalUserOrderSetById(createOrderInput.getOrderId());
			BeanUtils.copyProperties(userOrderSet, createOrderOutput);
			createOrderOutput.setOrderId(userOrderSet.getId());
		}
		OrderStatus orderStatus = new OrderStatus();
		orderStatus.setPayment(PaymentConstants.InitStatus);
		createOrderOutput.setStatus(orderStatus);
		createOrderOutput.setPaymentMode(createOrderInput.getPaymentMode());

		try {
			paymentMetadata = paymentGatewayFactory.paymentGatewayFactoryProcess(createOrderOutput,
					createOrderInput.getPaymentMode(), createOrderInput.getOrderId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		createOrderOutput.setPaymentMetadata(paymentMetadata);

		return createOrderOutput;
	}
}