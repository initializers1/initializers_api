package com.initializers.services.apiservices.resolver.query;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.repo.UserWishlistRepo;
import com.initializers.services.apiservices.service.UserWishlistService;

import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.Edge;
import graphql.relay.PageInfo;

@Component
public class UserWishListQueryResolver implements GraphQLQueryResolver {

	@Autowired
	UserWishlistService userWishlistService;
	@Autowired
	UserWishlistRepo userWishlistRepo;

	public Connection<ItemDetails> getUserWishList(Integer first, String after, ObjectId userId) {
		Integer afterInt = Integer.parseInt(after);
		Pageable page = PageRequest.of(afterInt, first);
		Page<ItemDetails> itemDetailsPage = userWishlistRepo.getUserWishList(userId, page);
		Connection<ItemDetails> con = new Connection<ItemDetails>() {

			@Override
			public PageInfo getPageInfo() {
				PageInfo page = new PageInfo() {

					@Override
					public boolean isHasPreviousPage() {
						return false;
					}

					@Override
					public boolean isHasNextPage() {
						return itemDetailsPage.hasNext();
					}

					@Override
					public ConnectionCursor getStartCursor() {
						return null;
					}

					@Override
					public ConnectionCursor getEndCursor() {
						return new DefaultConnectionCursor(String.valueOf(itemDetailsPage.getNumber() + 1));
					}
				};
				return page;
			}

			@Override
			public List<Edge<ItemDetails>> getEdges() {
				List<Edge<ItemDetails>> itemDetailsEdgeList = new ArrayList<Edge<ItemDetails>>();
				for (ItemDetails itemDetails : itemDetailsPage.getContent()) {
					Edge<ItemDetails> itemDetailsEdge = new Edge<ItemDetails>() {

						@Override
						public ItemDetails getNode() {
							if(itemDetails.getImageLinks()!= null && itemDetails.getImageLinks().size() > 0) {
								itemDetails.setImageLink(itemDetails.getImageLinks().get(0));
							}
							itemDetails.setIsWishlist(true);
							return itemDetails;
						}

						@Override
						public ConnectionCursor getCursor() {
							ConnectionCursor cur = new ConnectionCursor() {

								@Override
								public String getValue() {
									return "kar";
								}
							};
							return cur;
						}
					};
					itemDetailsEdgeList.add(itemDetailsEdge);
				}
				return itemDetailsEdgeList;
			}

		};
		return con;
	}
}
