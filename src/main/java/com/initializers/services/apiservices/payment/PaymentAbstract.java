package com.initializers.services.apiservices.payment;

import com.initializers.services.apiservices.model.output.CreateOrderOutput;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;
import com.initializers.services.apiservices.model.payment.PaymentProcess;
import com.initializers.services.apiservices.model.payment.UserPayment;
import com.initializers.services.apiservices.service.AdminClientExternalIdRefSevice;

public abstract class PaymentAbstract {

	private PaymentProcess paymentProcess;
	private CreateOrderOutput createOrderOutput;
	private AdminClientExternalIdRefSevice adminClientExternalIdRefSevice;

	public PaymentAbstract(PaymentProcess paymentProcess, CreateOrderOutput createOrderOutput, 
			AdminClientExternalIdRefSevice adminClientExternalIdRefSevice) {

		this.paymentProcess = paymentProcess;
		this.createOrderOutput = createOrderOutput;
		this.adminClientExternalIdRefSevice = adminClientExternalIdRefSevice;
	}

	public PaymentProcess getPaymentProcess() {
		return paymentProcess;
	}
	
	public CreateOrderOutput getCreateOrderOutput() {
		return createOrderOutput;
	}
	
	public AdminClientExternalIdRefSevice getAdminClientExternalIdRefSevice() {
		return adminClientExternalIdRefSevice;
	}
	
	public abstract PaymentMetadata pushPaymentTransaction(UserPayment userPayment);
	public abstract void configureWebHook();
	public abstract void cleanupExternalOrderId(String transactionId);
}
