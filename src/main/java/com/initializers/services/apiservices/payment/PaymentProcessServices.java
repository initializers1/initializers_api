package com.initializers.services.apiservices.payment;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.input.CreateOrderInput;
import com.initializers.services.apiservices.model.payment.PaymentGateway;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;
import com.initializers.services.apiservices.model.payment.PaymentProcess;

@Service
public interface PaymentProcessServices {
	
	PaymentProcess getPaymentRequestSecretKey(char paymentMode);
	
	PaymentMetadata getPaymentMetadata(CreateOrderInput createOrderInput);
	
	List<PaymentProcess> getPaymentSupportTypes();
	
}
