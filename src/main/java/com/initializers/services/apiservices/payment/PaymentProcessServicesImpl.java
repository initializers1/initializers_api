package com.initializers.services.apiservices.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.exception.PayementGatewayNotFoundException;
import com.initializers.services.apiservices.model.input.CreateOrderInput;
import com.initializers.services.apiservices.model.output.CreateOrderOutput;
import com.initializers.services.apiservices.model.payment.PaymentGateway;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;
import com.initializers.services.apiservices.model.payment.PaymentProcess;
import com.initializers.services.apiservices.service.PaymentGatewayService;
import com.initializers.services.apiservices.service.impl.UserCartServiceImpl;

@Service
public class PaymentProcessServicesImpl implements PaymentProcessServices {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private PaymentGatewayFactory paymentGatewayFactory;
	@Autowired
	private UserCartServiceImpl userCartServiceImpl;
	@Autowired
	private PaymentGatewayService paymentGatewayService;

	@Override
	public PaymentProcess getPaymentRequestSecretKey(char paymentMode) {
		
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		PaymentProcess paymentProcess = new PaymentProcess();

		list.add(Aggregation.match(Criteria.where("status").is("Active")));
		//Card	: C
		//UPI	: U
		//COD	: M
		list.add(Aggregation.match(Criteria.where("type").is(paymentMode)));

		list.add(Aggregation.project(Fields.fields("secretKey", "publicKey", "status", "gatewayID")));

		Aggregation aggregation = Aggregation.newAggregation(list);
		List<PaymentProcess> results = mongoTemplate
				.aggregate(aggregation, "AdminPayementGatewayKey", PaymentProcess.class).getMappedResults();

		if (results.size() <= 0) {
			throw new PayementGatewayNotFoundException();
		} else {
			
			paymentProcess.setGatewayID(results.get(0).getGatewayID());
			paymentProcess.setName(results.get(0).getName());
			paymentProcess.setSecretKey(results.get(0).getSecretKey());
			paymentProcess.setPublicKey(results.get(0).getPublicKey());
			paymentProcess.setStatus(results.get(0).getStatus());
			paymentProcess.setType(paymentMode);
			PaymentGateway paymentGateway = paymentGatewayService.getPaymentGatewayById(results.get(0).getGatewayID());
			if(paymentGateway == null) {
				throw new PayementGatewayNotFoundException();
			}
			paymentProcess.setClassName(paymentGateway.getClassName());
			paymentProcess.setName(paymentGateway.getName());

			return paymentProcess;
		}

	}


	@Override
	public List<PaymentProcess> getPaymentSupportTypes() {
		
		List<PaymentProcess> supportPaymentTypes = new ArrayList<PaymentProcess>();
		
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(LookupOperation.newLookup().from("PaymentGateway").localField("gatewayID").foreignField("_id")
				.as("join_PaymentGateway"));

		list.add(Aggregation.match(Criteria.where("status").is("Active")));

		list.add(Aggregation.project(Fields.fields("type").and(Fields.field("name", "join_PaymentGateway.name"))));

		Aggregation aggregation = Aggregation.newAggregation(list);
		List<PaymentProcess> results = mongoTemplate
				.aggregate(aggregation, "AdminPayementGatewayKey", PaymentProcess.class).getMappedResults();

		if (results.size() <= 0) {
			throw new PayementGatewayNotFoundException();
		} else {
			supportPaymentTypes = results;
		}

		return supportPaymentTypes;
	}
	
	@Override
	public PaymentMetadata getPaymentMetadata(CreateOrderInput createOrderInput) {

		PaymentMetadata paymentMetadata = new PaymentMetadata();
		CreateOrderOutput createOrderOutput =new CreateOrderOutput();

		createOrderOutput = userCartServiceImpl.createOrder(createOrderInput);

		try {
//			paymentMetadata = paymentGatewayFactory.paymentGatewayFactoryProcess(createOrderOutput,createOrderInput.getPaymentMode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return paymentMetadata;
	}

	

}
