package com.initializers.services.apiservices.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.scheduling.annotation.Async;

import com.initializers.services.apiservices.constant.PaymentConstants;
import com.initializers.services.apiservices.exception.WebHookConfigException;
import com.initializers.services.apiservices.model.output.CreateOrderOutput;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;
import com.initializers.services.apiservices.model.payment.PaymentProcess;
import com.initializers.services.apiservices.model.payment.UserPayment;
import com.initializers.services.apiservices.service.AdminClientExternalIdRefSevice;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.model.WebhookEndpoint;

public class StripeController extends PaymentAbstract {

	public StripeController(PaymentProcess paymentProcess, CreateOrderOutput createOrderOutput,
			AdminClientExternalIdRefSevice adminClientExternalIdRefSevice) {

		super(paymentProcess, createOrderOutput, adminClientExternalIdRefSevice);

	}

	@Override
	public PaymentMetadata pushPaymentTransaction(UserPayment userPayment) {

		PaymentMetadata paymentMetadata = new PaymentMetadata();

//		THIS IS MUST IN ALL THE IMPLEMENTATION: don't create new intent if it's update operation, 
		if (userPayment != null && userPayment.getType() == PaymentConstants.Card) {
			paymentMetadata.setExternalOrderId(userPayment.getExternalOrderId());
			paymentMetadata.setTransactionId(userPayment.getTransactionId());
		} else {
			// Transaction Authentication
			Stripe.apiKey = super.getPaymentProcess().getSecretKey();

			List<Object> paymentMethodTypes = new ArrayList<>();
			paymentMethodTypes.add("card");

			Map<String, Object> params = new HashMap<>();

			params.put("amount", (int) (super.getCreateOrderOutput().getTotalAmount() * 100));
			params.put("currency", "INR");
			params.put("payment_method_types", paymentMethodTypes);

			PaymentIntent paymentIntent = null;

			try {
				paymentIntent = PaymentIntent.create(params);
				super.getAdminClientExternalIdRefSevice().addExternalIdRef(paymentIntent.getId());
			} catch (StripeException e) {
				e.printStackTrace();
			}

			paymentMetadata.setExternalOrderId(paymentIntent.getClientSecret());
			paymentMetadata.setTransactionId(paymentIntent.getId());
		}
		paymentMetadata.setGatewayName(super.getPaymentProcess().getName());
		paymentMetadata.setPublicKey(super.getPaymentProcess().getPublicKey());
		paymentMetadata.setSecretKey(null); // Not Applicable to StripeMetadata
		paymentMetadata.setOrderId(super.getCreateOrderOutput().getOrderId());

		return paymentMetadata;
	}

	@Override
	public void configureWebHook() {
		// Transaction Authentication
		Stripe.apiKey = super.getPaymentProcess().getSecretKey();
		List<Object> enabledEvents = new ArrayList<>();
		enabledEvents.add("payment_intent.succeeded");
		enabledEvents.add("payment_intent.payment_failed");
		Map<String, Object> params = new HashMap<>();
		params.put("url", "https://example.com/my/webhook/endpoint");
		params.put("enabled_events", enabledEvents);

		try {
			WebhookEndpoint webhookEndpoint = WebhookEndpoint.create(params);
		} catch (StripeException e) {
			throw new WebHookConfigException();
		}

	}

	@Override
	@Async
	public void cleanupExternalOrderId(String transactionId) {
		try {
			Stripe.apiKey = super.getPaymentProcess().getSecretKey();
			PaymentIntent paymentIntent =  PaymentIntent.retrieve(transactionId);
			Map<String, Object> cancelReason = new HashMap<>();
			cancelReason.put("cancellation_reason", "abandoned");
			paymentIntent.cancel(cancelReason);
		} catch (StripeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}