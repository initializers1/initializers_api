package com.initializers.services.apiservices.payment;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.constant.PaymentConstants;
import com.initializers.services.apiservices.exception.OrderNotFoundException;
import com.initializers.services.apiservices.exception.PaymentConfigurationException;
import com.initializers.services.apiservices.model.output.CreateOrderOutput;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;
import com.initializers.services.apiservices.model.payment.PaymentProcess;
import com.initializers.services.apiservices.model.payment.UserPayment;
import com.initializers.services.apiservices.repo.UserPaymentRepo;
import com.initializers.services.apiservices.service.AdminClientExternalIdRefSevice;

@Component
public class PaymentGatewayFactory {

	@Autowired
	private PaymentProcessServices paymentProcessServices;
	@Autowired
	UserPaymentRepo userPaymentRepo;
	@Autowired
	AdminClientExternalIdRefSevice adminClientExternalIdRefService;

	public PaymentMetadata paymentGatewayFactoryProcess(CreateOrderOutput createOrderOutput, char paymentMode,
			ObjectId orderId) {

		UserPayment userPayment = null;
		PaymentProcess paymentProcess;

		paymentProcess = paymentProcessServices.getPaymentRequestSecretKey(paymentMode);

		if (orderId != null) {
			userPayment = userPaymentRepo.findById(orderId);
			if (userPayment == null) {
				throw new OrderNotFoundException();
			}
			if (userPayment.getStatus() == PaymentConstants.FinalStatus) {
				throw new OrderNotFoundException();
			}
			createOrderOutput.setTotalAmount(userPayment.getAmount());
			if (userPayment.getType() != paymentMode) {
//	Clean up external id in case of change of payment
				PaymentProcess paymentProcessOld = paymentProcessServices
						.getPaymentRequestSecretKey(userPayment.getType());
				PaymentAbstract object = getPaymentAbstractInstance(paymentProcessOld.getClassName(), paymentProcessOld,
						createOrderOutput);
				object.cleanupExternalOrderId(userPayment.getTransactionId());
			}
		}

		PaymentAbstract object = getPaymentAbstractInstance(paymentProcess.getClassName(), paymentProcess,
				createOrderOutput);

// Payment Transaction Module
		PaymentMetadata paymentMetadata = object.pushPaymentTransaction(userPayment);

// For new orders
		if (userPayment == null)
			userPayment = new UserPayment();
		userPayment.setId(paymentMetadata.getOrderId());
		userPayment.setAmount(createOrderOutput.getTotalAmount());
// Initial Status
		userPayment.setStatus(PaymentConstants.InitStatus);
		userPayment.setType(paymentMode);
		userPayment.setExternalOrderId(paymentMetadata.getExternalOrderId());
		userPayment.setTransactionId(paymentMetadata.getTransactionId());
		userPaymentRepo.save(userPayment);

		return paymentMetadata;
	}

	public PaymentAbstract getPaymentAbstractInstance(String className, PaymentProcess paymentProcess,
			CreateOrderOutput createOrderOutput) {
		Class<?> oldInstance;
		try {
			oldInstance = Class.forName(PaymentConstants.ClassPath + className);
			Constructor<?> oldInstanceConstructor = oldInstance.getConstructor(PaymentProcess.class,
					CreateOrderOutput.class, AdminClientExternalIdRefSevice.class);
			return (PaymentAbstract) oldInstanceConstructor
					.newInstance(new Object[] { paymentProcess, createOrderOutput, adminClientExternalIdRefService });
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PaymentConfigurationException();
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			throw new PaymentConfigurationException();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new PaymentConfigurationException();
		}
	}

}
