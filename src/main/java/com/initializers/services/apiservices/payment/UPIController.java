package com.initializers.services.apiservices.payment;

import com.initializers.services.apiservices.model.output.CreateOrderOutput;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;
import com.initializers.services.apiservices.model.payment.PaymentProcess;
import com.initializers.services.apiservices.model.payment.UserPayment;
import com.initializers.services.apiservices.service.AdminClientExternalIdRefSevice;

public abstract class UPIController extends PaymentAbstract {

	public UPIController(PaymentProcess paymentProcess, CreateOrderOutput createOrderOutput,
			AdminClientExternalIdRefSevice adminClientExternalIdRefSevice) {

		super(paymentProcess, createOrderOutput, adminClientExternalIdRefSevice);

	}

	@Override
	public PaymentMetadata pushPaymentTransaction(UserPayment userPayment) {
		
		PaymentMetadata paymentMetadata = new PaymentMetadata();
		
		paymentMetadata.setGatewayName(super.getPaymentProcess().getName());
		paymentMetadata.setOrderId(super.getCreateOrderOutput().getOrderId());
		
		return paymentMetadata;

	}

	@Override
	public void configureWebHook() {
		// TODO Auto-generated method stub
		
	}

}