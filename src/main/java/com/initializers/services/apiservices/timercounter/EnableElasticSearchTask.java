package com.initializers.services.apiservices.timercounter;

import java.util.TimerTask;

import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.others.ApplicationProperties;

@Component
public class EnableElasticSearchTask extends TimerTask {
	
	@Override
	public void run() {
		ApplicationProperties.enable_elasticSearch = true;
	}

}
