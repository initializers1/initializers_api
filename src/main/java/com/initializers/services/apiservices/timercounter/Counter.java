package com.initializers.services.apiservices.timercounter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.others.ApplicationProperties;
import com.initializers.services.apiservices.service.MailService;

@Component
public class Counter {

	private long counter;
	private String message;

	Timer timer;

	public Counter() {
		this.timer = new Timer();
	}

	@Autowired
	EnableElasticSearchTask enableElasticSearchTask;
	@Autowired
	MailService mailService;

	public long getCounter() {
		return counter;
	}

	public String getMessage() {
		return this.message;
	}

	@Async
	public void incrementCounter(String... message) {
		this.counter++;
		if (this.counter > ApplicationProperties.max_count_elasticSearch) {
			ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
			ApplicationProperties.enable_elasticSearch = false;
			scheduler.schedule(enableElasticSearchTask, ApplicationProperties.tryout_timer_elasticSearch,
					TimeUnit.SECONDS);
			mailService.notifyAdmin("ElasticSearch is down", this.message);
			resetCounter();
		}
		if (message.length > 0) {
			this.message += Arrays.toString(message);
		}
	}

	@Async
	public void resetCounter() {
		this.counter = 0;
		this.message = "";
	}
}
