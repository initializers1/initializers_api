package com.initializers.services.apiservices.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.initializers.services.apiservices.exception.TenantValueMissingException;
import com.initializers.services.apiservices.multitenancy.TenantContext;

@Component
public class TenantFilter implements Filter{
	
	private static final String X_TENANT_ID = "X-Tenant";
//	TODO: Clear TenantContext at end of request
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;
//      skip preflight request, superadmin, graphql Schema
//        if(!servletRequest.getMethod().equals("OPTIONS") && !servletRequest.getRequestURI().startsWith("/superadmin") 
//        		&& !(servletRequest.getMethod().equals("GET") && servletRequest.getRequestURI().equals("/graphql/schema.json"))) {
//        	if (servletRequest.getHeader(X_TENANT_ID) != null) {
//        		TenantContext.setTenantId(TenantContext.getTenantIdForAppId(servletRequest.getHeader(X_TENANT_ID)));
//        		TenantContext.setAppId(servletRequest.getHeader(X_TENANT_ID));
//        	} else {
////        		throw new TenantValueMissingException();
//        	}        	
//        }
        chain.doFilter(servletRequest, servletResponse);
	}

}
