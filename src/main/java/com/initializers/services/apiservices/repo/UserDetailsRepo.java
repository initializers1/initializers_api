package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.UserDetails;

public interface UserDetailsRepo extends MongoRepository<UserDetails, ObjectId>{

	@Query(value = "{'id' : ?0, 'status' : '"+Status.Active+"'}")
	UserDetails findFirstById(ObjectId userId);
	
	@Query(value = "{'id' : ?0, 'status' : '"+Status.Active+"'}", fields = "{'firstName' : 1, 'lastName' : 1}")
	UserDetails findNameById(ObjectId userId);
	
	UserDetails findFirstIdByEmail(String email);
	 
	List<UserDetails> findByEmail(String email);
}
