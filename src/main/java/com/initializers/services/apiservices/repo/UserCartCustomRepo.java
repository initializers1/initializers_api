package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.initializers.services.apiservices.model.UserCart;
import com.initializers.services.apiservices.model.UserCartList;

public interface UserCartCustomRepo {
	Page<UserCartList> getUserCart(ObjectId userId, Pageable pageable);
	List<UserCartList> getUserCart(ObjectId userId);
	void findAndRemove(List<UserCart.CompositeKeyCart> keys);
}
