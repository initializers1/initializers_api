package com.initializers.services.apiservices.repo.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.model.elasticsearch.ItemCategoryES;
import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.model.elasticsearch.ItemSubCategoryES;
import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.model.item.ItemSubCategory;
import com.initializers.services.apiservices.repo.ItemCategoryRepo;
import com.initializers.services.apiservices.repo.ItemDetailsRepo;
import com.initializers.services.apiservices.repo.ItemSubCategoryRepo;
import com.initializers.services.apiservices.repo.SearchRepo;

@Repository
@Qualifier("withoutES")
public class SearchRepoWithoutESImpl implements SearchRepo {

	@Autowired
	ItemDetailsRepo itemDetailsRepo;
	@Autowired
	ItemCategoryRepo itemCategoryRepo;
	@Autowired
	ItemSubCategoryRepo itemSubCategoryRepo;

	@Override
	public List<ItemDetailsES> searchItemDetails(String searchTerm) {
		List<ItemDetails> itemDetails = itemDetailsRepo.findByName(searchTerm);
		List<ItemDetailsES> itemDetailsESList = new ArrayList<>();
		itemDetails.stream().forEach(item -> {
			ItemDetailsES itemDetailsES = new ItemDetailsES(item.getId(), item.getCategoryId(), item.getSubCategoryId(),
					item.getName(), item.getDescription(), item.getImageLinks(), item.getStatus(), null);
			itemDetailsESList.add(itemDetailsES);
		});
		return itemDetailsESList;
	}

	@Override
	public List<ItemCategoryES> searchItemCategory(String searchTerm) {
		List<ItemCategory> itemCategory = itemCategoryRepo.findByName(searchTerm);
		List<ItemCategoryES> itemCategoryESESList = new ArrayList<>();
		itemCategory.stream().forEach(catgory -> {
			ItemCategoryES itemCategoryES = new ItemCategoryES(catgory.getId(), catgory.getName(),
					catgory.getDescription(), null, catgory.getImageLink(), null);
			itemCategoryESESList.add(itemCategoryES);
		});
		return itemCategoryESESList;
	}

	@Override
	public List<ItemSubCategoryES> searchItemSubCategory(String searchTerm) {
		List<ItemSubCategory> itemSubCategory = itemSubCategoryRepo.findByName(searchTerm);
		List<ItemSubCategoryES> itemSubCategoryESESList = new ArrayList<>();
		itemSubCategory.stream().forEach(subcategory -> {
			ItemSubCategoryES itemSubCategoryES = new ItemSubCategoryES(subcategory.getId(),
					subcategory.getCategoryId(), subcategory.getName(), subcategory.getDescription(), null,
					subcategory.getImageLink(), null);
			itemSubCategoryESESList.add(itemSubCategoryES);
		});
		return itemSubCategoryESESList;
	}

}
