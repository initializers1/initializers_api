package com.initializers.services.apiservices.repo.Impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.initializers.services.apiservices.model.UserOrderSet;
import com.initializers.services.apiservices.repo.UserOrderSetCustomRepo;

public class UserOrderSetCustomRepoImpl implements UserOrderSetCustomRepo {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public UserOrderSet getUserOrderSetById(ObjectId id) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(Aggregation.match(Criteria.where("_id").is(id)));
		list.add(LookupOperation.newLookup().from("itemDetails").localField("orderList.itemId").foreignField("_id")
				.as("itemDetails"));
		list.add(LookupOperation.newLookup().from("UserPayment").localField("_id").foreignField("_id")
				.as("UserPayment"));
		list.add(LookupOperation.newLookup().from("userAddress").localField("addressId").foreignField("_id")
				.as("addressDetails"));
		list.add(LookupOperation.newLookup().from("itemAvailability").localField("orderList.availabilityId")
				.foreignField("_id").as("itemAvailability"));
		list.add(Aggregation.unwind("addressDetails", true));
		list.add(Aggregation.unwind("UserPayment", true));
		list.add(Aggregation.project(Fields
				.fields("_id", "orderList", "userId", "addressDetails", "status", "orderAt", "deliveredBy",
						"totalAmount", "deliveryCharge", "coupenCode", "coupenDiscount", "itemDetails",
						"itemAvailability")
				.and(Fields.field("paymentStatus", "UserPayment.status"))
				.and(Fields.field("paymentMode", "UserPayment.type"))));
		Aggregation aggregation = Aggregation.newAggregation(list);
		return mongoTemplate.aggregate(aggregation, "userOrder", UserOrderSet.class).getUniqueMappedResult();
	}

	@Override
	public Page<UserOrderSet> getUserOrderSetByUserId(ObjectId userId, Pageable pageable) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(Aggregation.unwind("orderList"));
		list.add(Aggregation.match(Criteria.where("userId").is(userId)));
		list.add(LookupOperation.newLookup().from("itemDetails").localField("orderList.itemId").foreignField("_id")
				.as("itemDetails"));
		list.add(LookupOperation.newLookup().from("UserPayment").localField("_id").foreignField("_id")
				.as("UserPayment"));
		list.add(Aggregation.unwind("UserPayment"));
		list.add(Aggregation.unwind("itemDetails"));
		list.add(Aggregation.group("_id").push("itemDetails.name").as("itemNames").first("status").as("status")
				.first("userId").as("userId").first("UserPayment.status").as("paymentStatus").first("UserPayment.type")
				.as("paymentMode").first("orderAt").as("orderAt").first("deliveredBy").as("deliveredBy")
				.first("totalAmount").as("totalAmount"));
//		list.add(Aggregation.unwind("itemNames"));
		list.add(Aggregation.sort(Sort.by(Direction.DESC, "orderAt")));
		list.add(Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize()));
		list.add(Aggregation.limit(pageable.getPageSize()));
		Aggregation aggregation = Aggregation.newAggregation(list);
		List<UserOrderSet> results = mongoTemplate.aggregate(aggregation, "userOrder", UserOrderSet.class)
				.getMappedResults();
//		count is calculated in separate query -> check for better approach 
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		Long totalSize = mongoTemplate.count(query, UserOrderSet.class);
		return new PageImpl<UserOrderSet>(results, pageable, totalSize);
	}

	@Override
	public UserOrderSet getMinimalUserOrderSetById(ObjectId id) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(Aggregation.match(Criteria.where("_id").is(id)));
//		list.add(LookupOperation.newLookup().from("UserPayment").localField("_id")
//				.foreignField("_id").as("UserPayment"));
		list.add(Aggregation.project(Fields.fields("_id", "deliveredBy", "totalAmount")
//				.and(Fields.field("paymentStatus", "UserPayment.status")).
//				and(Fields.field("paymentMode", "UserPayment.type"))
		));
		Aggregation aggregation = Aggregation.newAggregation(list);
		return mongoTemplate.aggregate(aggregation, "userOrder", UserOrderSet.class).getUniqueMappedResult();
	}

}
