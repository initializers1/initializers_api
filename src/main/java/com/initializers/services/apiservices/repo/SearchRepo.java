package com.initializers.services.apiservices.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.model.elasticsearch.ItemCategoryES;
import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.model.elasticsearch.ItemSubCategoryES;

@Repository
public interface SearchRepo {

	public List<ItemDetailsES> searchItemDetails(String searchTerm);
	
	public List<ItemCategoryES> searchItemCategory(String searchTerm);
	
	public List<ItemSubCategoryES> searchItemSubCategory(String searchTerm);
}
