package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.initializers.services.apiservices.model.item.ItemDetails;

public interface ItemDetailsCustomRepo {
	Page<ItemDetails> getItemDetails(ObjectId subCategoryID, Pageable pageable, String findBy);
	
	ItemDetails getItemDetailsById(ObjectId itemId);
}
