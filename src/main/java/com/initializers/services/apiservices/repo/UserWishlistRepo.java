package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.initializers.services.apiservices.model.UserWishlist;

public interface UserWishlistRepo extends MongoRepository<UserWishlist, ObjectId>, UserWishlistCustomRepo{

	UserWishlist findFirstByUserID(ObjectId userID);
	
}
