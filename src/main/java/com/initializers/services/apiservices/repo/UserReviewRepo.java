package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.initializers.services.apiservices.model.UserReview;

public interface UserReviewRepo extends MongoRepository<UserReview, ObjectId>, UserReviewCustomRepo{
	
	Page<UserReview> findByIdItemId(ObjectId itemId, Pageable pageable);
	
	@Aggregation(pipeline = { "{ $match: { '_id.itemId': ?0 } }"
			 ,"{ $group: { _id: '_id.itemId', rating: { $avg: $rating } } }"})
	Float findByIdItemIdAvgRating(ObjectId itemId);
}
