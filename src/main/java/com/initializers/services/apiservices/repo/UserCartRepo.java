package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.initializers.services.apiservices.model.UserCart;

public interface UserCartRepo extends MongoRepository<UserCart, UserCart.CompositeKeyCart>, UserCartCustomRepo{

	List<UserCart> findByIdUserId(ObjectId userId, Pageable pageable);
	
	List<UserCart> findByIdUserId(ObjectId userId);
	
	void deleteById(UserCart.CompositeKeyCart id);
	
}
