package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.initializers.services.apiservices.model.item.ItemCategory;
import com.initializers.services.apiservices.model.item.ItemDetails;

public interface ItemCategoryRepo extends MongoRepository<ItemCategory, ObjectId>, ItemCategoryCustomRepo{
	
	@Query(value = "{'id' : ?0}")
	ItemCategory findFirstById(ObjectId id);
	
	@Query(value = "{'id' : ?0}", fields = "{ 'name' : ?0 }")
	ItemCategory findFirstNameById(ObjectId id);
	
	Page<ItemCategory> findAll(Pageable pageable);
	
	List<ItemCategory> findByName(String name);
}
