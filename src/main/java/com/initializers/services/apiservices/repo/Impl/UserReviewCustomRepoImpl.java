package com.initializers.services.apiservices.repo.Impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.output.UserReviewOutput;
import com.initializers.services.apiservices.repo.UserReviewCustomRepo;

public class UserReviewCustomRepoImpl implements UserReviewCustomRepo {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Page<UserReviewOutput> getItemReview(ObjectId itemId, Pageable pageable) {

		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(LookupOperation.newLookup().from("UserDetails").localField("_id.userId").foreignField("_id")
				.as("join_user"));
		list.add(Aggregation.match(Criteria.where("_id.itemId").is(itemId)));
		list.add(Aggregation.match(Criteria.where("join_user.status").is(Status.Active)));
		list.add(Aggregation.unwind("join_user", true));
		list.add(Aggregation
				.project(Fields.fields("rating", "review", "averageRating", "changedAt")
						.and(Fields.field("itemId", "_id.itemId")).and(Fields.field("userId", "_id.userId")))
				.andExpression("concat(join_user.firstName,' ',join_user.lastName)").as("userName"));
		Aggregation aggregation = Aggregation.newAggregation(list);
		List<UserReviewOutput> results = mongoTemplate.aggregate(aggregation, "userReview", UserReviewOutput.class)
				.getMappedResults();
		int start = (int) pageable.getPageNumber() * pageable.getPageSize();
		int end = (start + pageable.getPageSize()) > results.size() ? results.size() : (start + pageable.getPageSize());
		return new PageImpl<>(results.subList(start, end), pageable, results.size());
	}

}
