package com.initializers.services.apiservices.repo.Impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.repo.UserWishlistCustomRepo;

public class UserWishlistCustomRepoImpl implements UserWishlistCustomRepo {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public Page<ItemDetails> getUserWishList(ObjectId userId, Pageable pageable) {
		
//		TODO: Pagination needs to be improved, use skip and limit at beginning of query, find a way to get total count

		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(Aggregation.unwind("itemsId"));
		list.add(LookupOperation.newLookup().from("itemDetails").localField("itemsId").foreignField("_id")
				.as("itemDetails"));
		list.add(Aggregation.match(Criteria.where("itemDetails.status").is(Status.Active)));
		list.add(LookupOperation.newLookup().from("itemAvailability").localField("itemsId").foreignField("itemId")
				.as("itemAvailability"));
		list.add(Aggregation.match(Criteria.where("itemAvailability.available").is(Status.Active)));
		list.add(Aggregation.unwind("itemDetails"));
		list.add(Aggregation.project(Fields.fields()
				.
				and(Fields.field("_id", "itemDetails._id")).
				and(Fields.field("name", "itemDetails.name")).
//				and(Fields.field("description", "itemDetails.description")).
				and(Fields.field("imageLinks", "itemDetails.imageLinks")).
				and(Fields.field("itemAvailability", "itemAvailability"))
				.and(Fields.field("count", "count"))
				));
		Aggregation aggregation = Aggregation.newAggregation(list);
		List<ItemDetails> results = mongoTemplate.aggregate(aggregation, "user_wishlist", ItemDetails.class)
				.getMappedResults();
		int start = (int) pageable.getPageNumber() * pageable.getPageSize();
		int end = (start + pageable.getPageSize()) > results.size() ? results.size() : (start + pageable.getPageSize());
		return new PageImpl<>(results.subList(start, end), pageable, results.size());
	}

}
