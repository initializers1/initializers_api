package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.item.ItemAvailability;

public interface ItemAvailabilityRepo extends MongoRepository<ItemAvailability, ObjectId>{
	
	@Query(value = "{'id' : ?0, 'available' : '"+Status.Active+"'}")
	ItemAvailability findFisrtById(ObjectId id);
	
	@Query(value = "{'id' : ?0 }")
	ItemAvailability findFirstId(ObjectId id);
	
	@Query(value = "{'id' : ?0, 'available' : '"+Status.Active+"'}", fields = "{'discountPrice' : 1}")
	ItemAvailability findFirstDiscountPriceById(ObjectId id);
	
	@Query(value = "{'id' : ?0}", fields = "{'value' : 1, 'unit' : 1}")
	ItemAvailability findFirstValueUnitId(ObjectId id);
	
	@Query(value = "{'itemId' : ?0, 'available' : '"+Status.Active+"'}")
	List<ItemAvailability> findByItem(ObjectId itemId);
	
	List<ItemAvailability> findByItemId(ObjectId itemId);
	
}
