package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.Address;


public interface AddressRepo extends MongoRepository<Address, ObjectId>{
	
	List<Address> findByUserId(ObjectId userId);
	
	@Query(value = "{'userId' : ?0, 'status' : '"+ Status.Active +"'}")
	Page<Address> findByUserId(ObjectId userId, Pageable pageable);
	
	Address findFirstById(ObjectId id);
	
	@Query(value = "{'id' : ?0, 'userId' : ?0}", fields = "{'name' : ?0}")
	Address findByIdUserID(ObjectId id, ObjectId userId);
	
}
