package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.initializers.services.apiservices.model.item.ItemDetails;

public interface UserWishlistCustomRepo {
	
	Page<ItemDetails> getUserWishList(ObjectId userId, Pageable pageable);
}
