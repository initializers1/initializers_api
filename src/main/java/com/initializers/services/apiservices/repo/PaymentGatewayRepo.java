package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.model.payment.PaymentGateway;

@MasterData
@Repository
public interface PaymentGatewayRepo extends MongoRepository<PaymentGateway, ObjectId> {
	
	List<PaymentGateway> findAll();
	
	PaymentGateway findFirstById(ObjectId id);
	
}
