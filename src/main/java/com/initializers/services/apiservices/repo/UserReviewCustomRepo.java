package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.initializers.services.apiservices.model.output.UserReviewOutput;

public interface UserReviewCustomRepo {
	Page<UserReviewOutput> getItemReview(ObjectId itemId, Pageable pageable);
}
