package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.initializers.services.apiservices.model.HomePage;

public interface HomePageRepo extends MongoRepository<HomePage, ObjectId>{
	Page<HomePage> findAll(Pageable pageable);
	
	HomePage findFirstById(ObjectId id);
}
