package com.initializers.services.apiservices.repo.Impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.UserCart;
import com.initializers.services.apiservices.model.UserCart.CompositeKeyCart;
import com.initializers.services.apiservices.model.UserCartList;
import com.initializers.services.apiservices.repo.UserCartCustomRepo;

public class UserCartCustomRepoImpl implements UserCartCustomRepo {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Page<UserCartList> getUserCart(ObjectId userId, Pageable pageable) {

//		TODO: Pagination needs to be improved, use skip and limit at beginning of query, find a way to get total count

		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(LookupOperation.newLookup().from("itemDetails").localField("_id.itemId").foreignField("_id")
				.as("join_item"));
		list.add(Aggregation.match(Criteria.where("join_item.status").is(Status.Active)));
		list.add(LookupOperation.newLookup().from("itemAvailability").localField("_id.availabilityId")
				.foreignField("_id").as("join_availability"));
		list.add(Aggregation.unwind("join_item", true));
		list.add(Aggregation.unwind("join_availability", true));
		list.add(Aggregation.match(Criteria.where("join_availability.available").is(Status.Active)));
		list.add(Aggregation.match(Criteria.where("_id.userId").is(userId)));
		list.add(Aggregation.project(Fields.fields("_id", "quantity", "unit", "value")
				.and(Fields.field("itemId", "_id.itemId")).and(Fields.field("availabilityId", "_id.availabilityId"))
				.and(Fields.field("userId", "_id.userId")).and(Fields.field("itemName", "join_item.name"))
				.and(Fields.field("imageLinks", "join_item.imageLinks"))
				.and(Fields.field("discountPrice", "join_availability.discountPrice"))
				.and(Fields.field("unit", "join_availability.unit"))
				.and(Fields.field("value", "join_availability.value"))));
		Aggregation aggregation = Aggregation.newAggregation(list);
		List<UserCartList> results = mongoTemplate.aggregate(aggregation, "userCart", UserCartList.class)
				.getMappedResults();
		int start = (int) pageable.getPageNumber() * pageable.getPageSize();
		int end = (start + pageable.getPageSize()) > results.size() ? results.size() : (start + pageable.getPageSize());
		return new PageImpl<>(results.subList(start, end), pageable, results.size());

	}

	@Override
	public List<UserCartList> getUserCart(ObjectId userId) {
//		TODO: Pagination needs to be improved, use skip and limit at beginning of query, find a way to get total count

		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(LookupOperation.newLookup().from("itemDetails").localField("_id.itemId").foreignField("_id")
				.as("join_item"));
		list.add(Aggregation.match(Criteria.where("join_item.status").is(Status.Active)));
		list.add(LookupOperation.newLookup().from("itemAvailability").localField("_id.availabilityId")
				.foreignField("_id").as("join_availability"));
		list.add(Aggregation.match(Criteria.where("join_availability.available").is(Status.Active)));
		list.add(Aggregation.match(Criteria.where("_id.userId").is(userId)));
		list.add(Aggregation.unwind("join_item", true));
		list.add(Aggregation.unwind("join_availability", true));
		list.add(Aggregation.project(Fields.fields("_id", "quantity", "unit", "value")
				.and(Fields.field("itemId", "join_item._id")).and(Fields.field("userId", "_id.userId"))
				.and(Fields.field("availabilityId", "join_availability._id"))
				.and(Fields.field("discountPrice", "join_availability.discountPrice"))
				.and(Fields.field("unit", "join_availability.unit"))
				.and(Fields.field("value", "join_availability.value"))));
		Aggregation aggregation = Aggregation.newAggregation(list);
		List<UserCartList> results = mongoTemplate.aggregate(aggregation, "userCart", UserCartList.class)
				.getMappedResults();
		return results;
	}

	@Override
	public void findAndRemove(List<CompositeKeyCart> keys) {
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(keys));
		mongoTemplate.findAllAndRemove(query, UserCart.class);
	}

}
