package com.initializers.services.apiservices.repo.elasticsearch;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.elasticsearch.ItemCategoryES;

@Service
public interface ItemCategoryElasticSearchRepo {
	public void createIndexAlias(String alias);
	public Page<ItemCategoryES> searchItemCategoryForAlias(String searchTerm, Pageable pageable);
	public List<SearchHit<ItemCategoryES>> searchItemCategoryForAliasAsList(String searchTerm);
	public void insertItemCategoryForAlias(ItemCategoryES itemCategory, String alias);
}
