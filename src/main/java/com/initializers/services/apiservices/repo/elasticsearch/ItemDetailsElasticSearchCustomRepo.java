package com.initializers.services.apiservices.repo.elasticsearch;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;

@Service
public interface ItemDetailsElasticSearchCustomRepo {
	public void createIndexAlias(String alias);
	public Page<ItemDetailsES> searchItemDetailsForAlias(String searchTerm, Pageable pageable);
	public List<SearchHit<ItemDetailsES>> searchItemDetailsForAliasAsList(String searchTerm);
	public void insertItemDetailsForAlias(ItemDetailsES itemDetails, String alias);
}