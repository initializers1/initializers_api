package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.model.AdminDetails;

@MasterData
public interface AdminDetailsRepo extends MongoRepository<AdminDetails, ObjectId> {
	
}
