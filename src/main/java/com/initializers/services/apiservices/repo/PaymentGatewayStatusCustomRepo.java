package com.initializers.services.apiservices.repo;

import com.initializers.services.apiservices.annotation.MasterData;

@MasterData
public interface PaymentGatewayStatusCustomRepo {
	
	char getInternalStatusByGatewayName(String gatewayName, String status);
}
