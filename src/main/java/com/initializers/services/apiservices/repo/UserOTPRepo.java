package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.model.UserOTP;

@MasterData
@Repository
public interface UserOTPRepo extends MongoRepository<UserOTP, ObjectId> {
	
	UserOTP findFirstById(ObjectId id);
	
	void deleteById(ObjectId id);
}
