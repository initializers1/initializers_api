package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.initializers.services.apiservices.model.payment.UserPayment;

public interface UserPaymentRepo extends MongoRepository<UserPayment, String> {
	
	UserPayment findById(ObjectId id);
	
	List<UserPayment> findAll();
	
	List<UserPayment> findListByexternalOrderId(String externalOrderId);

}
