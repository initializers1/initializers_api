package com.initializers.services.apiservices.repo.elasticsearch;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

//@Configuration
@Service
public interface ItemDetailsElasticSearchRepo 
extends 
//ElasticsearchRepository<ItemDetailsES, ObjectId>, 
ItemDetailsElasticSearchCustomRepo
{
	
}