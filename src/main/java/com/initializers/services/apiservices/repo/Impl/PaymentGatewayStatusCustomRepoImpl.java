package com.initializers.services.apiservices.repo.Impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.model.payment.PaymentGatewayStatus;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.repo.PaymentGatewayStatusCustomRepo;

@MasterData
public class PaymentGatewayStatusCustomRepoImpl implements PaymentGatewayStatusCustomRepo {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public char getInternalStatusByGatewayName(String gatewayName, String status) {
		String appId = TenantContext.getTenantId();
		TenantContext.clearTenant();
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(LookupOperation.newLookup().from("PaymentGateway").localField("gatewayID")
				.foreignField("_id").as("join_paymentGateway"));
		list.add(Aggregation.match( Criteria.where("gatewayStatus").
				is(status)));
		list.add(Aggregation.project(Fields.fields("_id", "status")
				.	
				and(Fields.field("name", "join_paymentGateway.name"))
				));
		list.add(Aggregation.match( Criteria.where("name").
				in(gatewayName)));
		Aggregation aggregation = Aggregation.newAggregation(list);
		Document doc = mongoTemplate.aggregate(aggregation, "PaymentGatewayStatus", PaymentGatewayStatus.class).getRawResults();
		PaymentGatewayStatus results = mongoTemplate.aggregate(aggregation, "PaymentGatewayStatus", PaymentGatewayStatus.class)
				.getUniqueMappedResult();
		TenantContext.setTenantId(appId);
		return results.getStatus();
	}

}
