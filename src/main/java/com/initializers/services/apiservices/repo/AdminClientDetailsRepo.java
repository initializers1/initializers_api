package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.constant.Scope;
import com.initializers.services.apiservices.model.AdminClientDetails;
import com.initializers.services.apiservices.model.AdminClientDetails.CompositeKeyAdminClient;

@MasterData
@Repository
public interface AdminClientDetailsRepo extends MongoRepository<AdminClientDetails, CompositeKeyAdminClient> {
	
	AdminClientDetails findFirstByIdAppId(String appId);
	AdminClientDetails findFirstById(CompositeKeyAdminClient id);
	AdminClientDetails findFirstByIdAndScope(CompositeKeyAdminClient id, Scope scope);
	AdminClientDetails findFirstByIdEmailAndScope(String email, Scope scope);
	@Query(value = "{'clientId' : ?0}", fields = "{'id' : 1 }")
	List<AdminClientDetails> findIdByClientId(ObjectId clientId);
}
