package com.initializers.services.apiservices.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.model.AdminClientExternalIdRef;
import com.initializers.services.apiservices.model.AdminClientExternalIdRef.CompositeKeyExtrnalId;

@MasterData
@Repository
public interface AdminClientExternalIdRefRepo extends MongoRepository<AdminClientExternalIdRef, CompositeKeyExtrnalId> {
	List<AdminClientExternalIdRef> findByIdExternalId(String externalId);
}
