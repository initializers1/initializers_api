package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.initializers.services.apiservices.model.UserOrderSet;

public interface UserOrderSetCustomRepo {
	
	UserOrderSet getUserOrderSetById(ObjectId id);
	
	Page<UserOrderSet> getUserOrderSetByUserId(ObjectId userId, Pageable pageable);
	
	UserOrderSet getMinimalUserOrderSetById(ObjectId id);
}
