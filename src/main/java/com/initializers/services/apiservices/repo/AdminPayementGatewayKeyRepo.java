package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.initializers.services.apiservices.model.payment.AdminPayementGatewayKey;

public interface AdminPayementGatewayKeyRepo extends MongoRepository<AdminPayementGatewayKey, ObjectId> {
		
	List<AdminPayementGatewayKey> findAll();
	
	AdminPayementGatewayKey findFirstById(ObjectId id);
	
}
