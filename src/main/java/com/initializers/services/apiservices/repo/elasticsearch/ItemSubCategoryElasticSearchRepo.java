package com.initializers.services.apiservices.repo.elasticsearch;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.elasticsearch.ItemSubCategoryES;

@Service
public interface ItemSubCategoryElasticSearchRepo {
	public void createIndexAlias(String alias);
	public Page<ItemSubCategoryES> searchItemSubCategoryForAlias(String searchTerm, Pageable pageable);
	public List<SearchHit<ItemSubCategoryES>> searchItemSubCategoryForAliasAsList(String searchTerm);
	public void insertItemSubCategoryForAlias(ItemSubCategoryES itemSubCategory, String alias);
}
