package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.model.TransientUserDetails;

@MasterData
@Repository
public interface TransientUserDetailsRepo extends MongoRepository<TransientUserDetails, ObjectId>{

	TransientUserDetails findFirstById(ObjectId userId);
	
	void deleteById(ObjectId userId);
	
	TransientUserDetails findFirstIdByAppId(String appId);
	
	@Query("{'$and':[ {'appId': ?0}, {'email': ?1} ]}")
	List<TransientUserDetails> findByAppIdAndEmail(String appId, String email);
	
}
