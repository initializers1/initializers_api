package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.item.ItemDetails;

public interface ItemDetailsRepo extends MongoRepository<ItemDetails, ObjectId>, ItemDetailsCustomRepo{
		
	@Query(value = "{'id' : ?0}")
	ItemDetails findFirstId(ObjectId id);
	
	@Query(value = "{'id' : ?0}", fields = "{'id' : 1, 'name' : 1,"
			+ "'imageLinks' : 1 }")
	ItemDetails findByIdForAllStatus(ObjectId id);
	
	@Query(value = "{'id' : ?0, 'status': '"+Status.Active+"'}", fields = "{'name' : 1 }")
	ItemDetails findFirstNameById(ObjectId id);
	
	@Query(value = "{'id' : ?0}", fields = "{'name' : 1 }")
	ItemDetails findFirstNameByIdAdmin(ObjectId id);
	
	@Query(value = "{'id' : ?0, 'status' : 'Active'}", fields = "{'id' : 1, 'name' : 1,"
			+ "'imageLinks' : 1 }")
	ItemDetails findListByIdByStatus(ObjectId id, String status);
	
	List<ItemDetails> findByName(String name);
	
	List<ItemDetails> findBySubCategoryId(ObjectId subCategoryID);
	
//	Client
	Page<ItemDetails> findByCategoryId(ObjectId categoryID, Pageable pageable);
	
	Page<ItemDetails> findBySubCategoryId(ObjectId subCategoryID, Pageable pageable);
	
	@Query(value = "{'id' : ?0}", fields = "{'name' : 1, 'imageLinks' : 1 }")
	ItemDetails findFirstNameImageLinksById(ObjectId id);
}
