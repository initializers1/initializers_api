package com.initializers.services.apiservices.repo.elasticsearch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest.AliasActions;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.NoSuchIndexException;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHitSupport;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.multitenancy.TenantContext;
import com.initializers.services.apiservices.timercounter.Counter;

@Service
public class ItemDetailsElasticSearchCustomRepoImpl implements ItemDetailsElasticSearchCustomRepo {

	@Autowired
	ElasticsearchOperations elasticSearchOperation;
	@Autowired
	RestHighLevelClient client;
	@Autowired
	Counter counter;

	@Override
	@Async
	public void createIndexAlias(String alias) {
		IndicesAliasesRequest request = new IndicesAliasesRequest();
		AliasActions aliasAction = new AliasActions(AliasActions.Type.ADD).index(ItemDetailsES.indexName)
				.alias(ItemDetailsES.indexName + "" + alias).filter("{\"term\":{\"appId\":\"" + alias + "\"}}");
		request.addAliasAction(aliasAction);
		try {
			client.indices().updateAliases(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Page<ItemDetailsES> searchItemDetailsForAlias(String searchTerm, Pageable pageable) {
		Criteria criteria = new Criteria("name").is(searchTerm);
		Query searchQuery = new CriteriaQuery(criteria, pageable);
		SearchHits<ItemDetailsES> itemHits = elasticSearchOperation.search(searchQuery, ItemDetailsES.class,
				IndexCoordinates.of(TenantContext.getTenantId()));
		return (Page) SearchHitSupport.searchPageFor(itemHits, searchQuery.getPageable());
//	    	     (Page) productHits;
	}

	@Override
	@Async
	public void insertItemDetailsForAlias(ItemDetailsES itemDetails, String alias) {
		itemDetails.setAppId(alias);
		IndexQuery indexQuery = new IndexQueryBuilder().withId(itemDetails.getId().toString()).withObject(itemDetails)
				.build();
		elasticSearchOperation.index(indexQuery, IndexCoordinates.of(ItemDetailsES.indexName+""+alias));
	}

	@Override
	public List<SearchHit<ItemDetailsES>> searchItemDetailsForAliasAsList(String searchTerm) {
		Criteria criteria = new Criteria("name").is(searchTerm);
		List<SearchHit<ItemDetailsES>> itemHits = new ArrayList<>();
		Query searchQuery = new CriteriaQuery(criteria);
		try {
			itemHits = elasticSearchOperation.search(searchQuery, ItemDetailsES.class,
					IndexCoordinates.of(ItemDetailsES.indexName + "" + TenantContext.getTenantId())).toList();
			counter.resetCounter();
			return itemHits;
		} catch (DataAccessResourceFailureException e) {
			counter.incrementCounter(e.toString());
		} catch (NoSuchIndexException e) {
			counter.incrementCounter(e.toString());
		}
		return itemHits;
	}

}
