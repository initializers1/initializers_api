package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.model.payment.PaymentGatewayStatus;

@MasterData
@Repository
public interface PaymentGatewayStatusRepo extends MongoRepository<PaymentGatewayStatus,ObjectId>, PaymentGatewayStatusCustomRepo{
	
	PaymentGatewayStatus findByGatewayIDAndGatewayStatus(ObjectId gatewayID, String gatewayStatus);
	
}
