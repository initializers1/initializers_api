package com.initializers.services.apiservices.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.annotation.MasterData;
import com.initializers.services.apiservices.model.AdminAppDetails;

@MasterData
@Repository
public interface AdminAppDetailsRepo extends MongoRepository<AdminAppDetails, String> {

	AdminAppDetails findFirstById(String appId);

	AdminAppDetails findFirstAppNameById(String appId);

	AdminAppDetails findFirstAppNameAppIconById(String appId);

	AdminAppDetails findFirstAppIconPipelineStatusById(String appId);

	AdminAppDetails findFirstByPipelineId(Integer pipelineId);

}
