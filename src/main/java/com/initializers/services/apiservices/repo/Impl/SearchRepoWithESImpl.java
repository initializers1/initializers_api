package com.initializers.services.apiservices.repo.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.stereotype.Repository;

import com.initializers.services.apiservices.model.elasticsearch.ItemCategoryES;
import com.initializers.services.apiservices.model.elasticsearch.ItemDetailsES;
import com.initializers.services.apiservices.model.elasticsearch.ItemSubCategoryES;
import com.initializers.services.apiservices.repo.SearchRepo;
import com.initializers.services.apiservices.repo.elasticsearch.ItemCategoryElasticSearchRepo;
import com.initializers.services.apiservices.repo.elasticsearch.ItemDetailsElasticSearchCustomRepo;
import com.initializers.services.apiservices.repo.elasticsearch.ItemSubCategoryElasticSearchRepo;

@Repository
@Qualifier("withES")
public class SearchRepoWithESImpl implements SearchRepo {

	@Autowired
	ItemDetailsElasticSearchCustomRepo itemDetailsElasticSearchCustomRepo;
	@Autowired
	ItemCategoryElasticSearchRepo itemCategoryElasticSearchRepo;
	@Autowired
	ItemSubCategoryElasticSearchRepo itemSubCategoryElasticSearchRepo;

	@Override
	public List<ItemDetailsES> searchItemDetails(String searchTerm) {
		List<SearchHit<ItemDetailsES>> searchHit = itemDetailsElasticSearchCustomRepo
				.searchItemDetailsForAliasAsList(searchTerm);
		List<ItemDetailsES> itemDetailsES = new ArrayList<>();
		searchHit.stream().forEach(hit -> itemDetailsES.add(hit.getContent()) );
		return itemDetailsES;
	}

	@Override
	public List<ItemCategoryES> searchItemCategory(String searchTerm) {
		List<SearchHit<ItemCategoryES>> searchHit = itemCategoryElasticSearchRepo.searchItemCategoryForAliasAsList(searchTerm);
		List<ItemCategoryES> itemCategoryES = new ArrayList<>();
		searchHit.stream().forEach(hit -> itemCategoryES.add(hit.getContent()));
		return itemCategoryES;
	}

	@Override
	public List<ItemSubCategoryES> searchItemSubCategory(String searchTerm) {
		List<SearchHit<ItemSubCategoryES>> searchHit = itemSubCategoryElasticSearchRepo.searchItemSubCategoryForAliasAsList(searchTerm);
		List<ItemSubCategoryES> itemSubCategoryES = new ArrayList<>();
		searchHit.stream().forEach(hit -> itemSubCategoryES.add(hit.getContent()));
		return itemSubCategoryES;
	}

}
