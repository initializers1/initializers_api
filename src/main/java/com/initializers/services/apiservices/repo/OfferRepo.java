package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.initializers.services.apiservices.model.Offer;

public interface OfferRepo extends MongoRepository<Offer, ObjectId>{

}
