package com.initializers.services.apiservices.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.initializers.services.apiservices.model.item.ItemSubCategory;

public interface ItemSubCategoryRepo extends MongoRepository<ItemSubCategory, ObjectId>{

	void deleteByCategoryId(ObjectId categoryId);
	
	ItemSubCategory findFirstById(ObjectId id);
	
	@Query(value = "{'id' : ?0}", fields = "{ 'name' : ?0 }")
	ItemSubCategory findFirstNameById(ObjectId id);
	
	List<ItemSubCategory> findByCategoryId(ObjectId categoryId);
	
	Page<ItemSubCategory> findByCategoryId(ObjectId categoryId, Pageable pageable);
	
	List<ItemSubCategory> findByName(String name);
}
