package com.initializers.services.apiservices.repo.Impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.repo.ItemDetailsCustomRepo;

public class ItemDetailsCustomRepoImpl implements ItemDetailsCustomRepo {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Page<ItemDetails> getItemDetails(ObjectId id, Pageable pageable, String findBy) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(LookupOperation.newLookup().from("itemAvailability").localField("_id").foreignField("itemId")
				.as("itemAvailability"));
		list.add(Aggregation.match(Criteria.where("status").is(Status.Active)));
		if (findBy.equals("ItemCategory")) {
			list.add(Aggregation.match(Criteria.where("categoryId").is(id)));
		} else {
			list.add(Aggregation.match(Criteria.where("subCategoryId").is(id)));
		}
		list.add(Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize()));
		list.add(Aggregation.limit(pageable.getPageSize()));
		Aggregation aggregation = Aggregation.newAggregation(list);
		Document test = mongoTemplate.aggregate(aggregation, "itemDetails", ItemDetails.class).getRawResults();
		List<ItemDetails> results = mongoTemplate.aggregate(aggregation, "itemDetails", ItemDetails.class)
				.getMappedResults();

//		count is calculated in separate query -> check for better approach 
		Query query = new Query();
		query.addCriteria(Criteria.where("status").is(Status.Active));
		if (findBy.equals("ItemCategory")) {
			query.addCriteria(Criteria.where("categoryId").is(id));
		} else {
			query.addCriteria(Criteria.where("subCategoryId").in(id));
		}
		Long totalSize = mongoTemplate.count(query, ItemDetails.class);
		return new PageImpl<>(results, pageable, totalSize);
	}

	@Override
	public ItemDetails getItemDetailsById(ObjectId itemId) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(Aggregation.match(Criteria.where("_id").is(itemId)));
		list.add(Aggregation.match(Criteria.where("status").is(Status.Active)));
		list.add(LookupOperation.newLookup().from("itemAvailability").localField("_id").foreignField("itemId")
				.as("itemAvailability"));
		list.add(LookupOperation.newLookup().from("userReview").localField("_id").foreignField("_id.itemId")
				.as("userReview"));
		list.add(Aggregation.unwind("userReview", true));
		list.add(Aggregation.group("_id").avg("userReview.rating").as("averageRating").first("_id").as("id")
				.first("name").as("name").first("itemAvailability").as("itemAvailability").first("description")
				.as("description").first("imageLinks").as("imageLinks"));
		Aggregation aggregation = Aggregation.newAggregation(list);
		ItemDetails itemDetails = mongoTemplate.aggregate(aggregation, "itemDetails", ItemDetails.class)
				.getUniqueMappedResult();
		return itemDetails;
	}
}
