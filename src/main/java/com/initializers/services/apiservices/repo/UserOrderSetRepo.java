package com.initializers.services.apiservices.repo;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.initializers.services.apiservices.model.UserOrderSet;

public interface UserOrderSetRepo extends MongoRepository<UserOrderSet, ObjectId>, UserOrderSetCustomRepo{
	
	UserOrderSet findFirstById(ObjectId id);
	
	Page<UserOrderSet> findByUserId(ObjectId userId, Pageable pageable);
	
}