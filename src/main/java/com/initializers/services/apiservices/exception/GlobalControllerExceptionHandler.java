package com.initializers.services.apiservices.exception;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "no such item")  // 409
    @ExceptionHandler(DataIntegrityViolationException.class)
    public void handleConflict() {
        // Nothing to do
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Item doesn't exist")
    @ExceptionHandler(ItemNotFoundException.class)
    public void itemNotFound() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Category doesn't exist")
    @ExceptionHandler(CategoryNotFoundException.class)
    public void categoryNotFound() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Sub Category doesn't exist")
    @ExceptionHandler(SubCategoryNotFoundException.class)
    public void subCategoryNotFound() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "UserName and Passowrd doesn't match, Please try agian")
    @ExceptionHandler(UserNotFoundException.class)
    public void userNotFound() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Mandatory value is missing")
    @ExceptionHandler(RequiredValueMissingException.class)
    public void requiredValueMissing() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Availabilty doesn't exist or unavailable")
    @ExceptionHandler(ItemAvailabilityException.class)
    public void itemAvailabilityNotFound() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Availabilty exist with same value and unit")
    @ExceptionHandler(ItemAvailabilityExistException.class)
    public void itemAvailabilityExist() {
    	
    }
    
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Address not found")
    @ExceptionHandler(AddressNotFoundException.class)
    public void addressNotFound() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Error in Order Configuration database")
    @ExceptionHandler(OrderConfigurationException.class)
    public void orderConfiguration() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "cart is empty")
    @ExceptionHandler(CartItemNotFoundException.class)
    public void CartItemNotFound() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Account already exist for this user, Please try to login")
    @ExceptionHandler(EmailExistException.class)
    public void emailExist() {
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Faulty EmailID")
    @ExceptionHandler(EmailIDException.class)
    public void emailIdFault() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.GONE, reason = "your current OTP is expired, we have sent a new one. Use can new OTP to continue")
    @ExceptionHandler(OTPExpiredException.class)
    public void otpExpired() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.BAD_GATEWAY, reason = "internal issue")
    @ExceptionHandler(InternalIssueException.class)
    public void internalIssue() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "OTP is not valid, please try againd")
    @ExceptionHandler(OTPNotValidException.class)
    public void otpNotValid() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Unable to upload Image")
    @ExceptionHandler(ImageUploadException.class)
    public void imageUploadException() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Please confirm order before marking delivered")
    @ExceptionHandler(UserOrderSetNotConfirmed.class)
    public void userOrderSetNotConfirmed() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Delivery Date should greater that or equal to todays date")
    @ExceptionHandler(UserOrderDeliveryDateException.class)
    public void userOrderDeliveryDateException() {
    	
    }
    	
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Item is already delivered, you cannot change delivery date")
    @ExceptionHandler(UserOrderItemDeliveredException.class)
    public void userOrderItemDeliveredException() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "Tenant not found")
    @ExceptionHandler(TenantNotFoundException.class)
    public void tenantNotFoundException() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Missing tenant info in the request")
    @ExceptionHandler(TenantValueMissingException.class)
    public void tenantValueMissingException() {
    	
    }
    
    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "No Access to change value")
    @ExceptionHandler(NoAccessToChangeValueException.class)
    public void noAccessToChangeValueException() {
    	
    }
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "There is already an app exist with this name, please try to use different name")
    @ExceptionHandler(DuplicateAppException.class)
    public void duplicateAppException() {
    	
    }
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Password should contain atlease 8 character and must include atlease one digit and a letter")
    @ExceptionHandler(PasswordShouldMatchCrieteriaException.class)
    public void passwordShouldMatchCrieteriaException() {
    	
    }
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Special characters are not allowed")
    @ExceptionHandler(SpecialCharacterNotAllowedException.class)
    public void pecialCharacterNotAllowedException() {
    	
    }
    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "Unexpected error while configuring your webhook, please contact your system administrator")
    @ExceptionHandler(WebHookConfigException.class)
    public void webHookConfigException() {
    	
    }
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Order does not exist")
    @ExceptionHandler(OrderNotFoundException.class)
    public void orderNotFoundException() {
    	
    }
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Payment Configuration missing, please maintain proper configuration")
    @ExceptionHandler(PaymentConfigurationException.class)
    public void paymentConfigurationMissingException() {
    	
    }
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "OTP cannot be regenerated")
    @ExceptionHandler(OTPNotExpiredException.class)
    public void otpNotExpiredException() {
    	
    }
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "There is already an app being generated, you cannot create another one now")
    @ExceptionHandler(ActiveBuildExistException.class)
    public void pipelineStillRunning() {
    	
    }
}
