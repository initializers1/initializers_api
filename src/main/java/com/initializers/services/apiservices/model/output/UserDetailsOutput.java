package com.initializers.services.apiservices.model.output;

import com.initializers.services.apiservices.model.UserDetails;

public class UserDetailsOutput {
	private UserDetails userDetails;

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
	
}
