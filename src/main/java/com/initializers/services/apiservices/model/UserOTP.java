package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Getter;
import lombok.Setter;


@Document("UserOTP")
@Getter
@Setter
public class UserOTP {
	
	@Id
	private ObjectId id;
	
	private Integer otp;
	
	@DateTimeFormat(iso=ISO.DATE_TIME)
	private java.util.Date expiryTime;
	
}
