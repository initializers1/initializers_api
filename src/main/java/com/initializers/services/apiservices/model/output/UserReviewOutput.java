package com.initializers.services.apiservices.model.output;

import java.time.LocalDate;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserReviewOutput {
	
    private String id;
    private ObjectId userId;
	private ObjectId itemId;
	private int rating;
	private String review;
	private LocalDate changedAt;
	private Float averageRating;
	private String userName;
		
}
