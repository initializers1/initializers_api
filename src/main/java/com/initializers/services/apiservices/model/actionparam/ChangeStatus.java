package com.initializers.services.apiservices.model.actionparam;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangeStatus {
	
	private ObjectId[] id;
	private String status;
}
