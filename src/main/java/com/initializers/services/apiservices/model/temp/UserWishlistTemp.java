package com.initializers.services.apiservices.model.temp;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserWishlistTemp {
	@Id
	private ObjectId userID;
	private ObjectId itemsId;
}
