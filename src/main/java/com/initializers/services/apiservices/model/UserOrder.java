package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserOrder {

	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId itemId;
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId availabilityId;
//	@Transient
	private String itemName;
//	@Transient
	private String imageLink;
//	@Transient 
	private String unit;
	private Float quantity;
	private Float value;
	private Float amount;
}
