package com.initializers.services.apiservices.model.temp;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoggedInUserResponse {
	@JsonSerialize(using = ObjectIdSerializer.class)
	ObjectId id;
	String userName;
	String accessToken;
	String appName;
	String appIcon;
}
