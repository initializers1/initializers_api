package com.initializers.services.apiservices.model.output;

import com.initializers.services.apiservices.model.Address;

public class AddressOutput {
	private Address address;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
