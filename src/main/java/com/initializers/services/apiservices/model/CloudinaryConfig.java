package com.initializers.services.apiservices.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.api.ApiResponse;
import com.cloudinary.utils.ObjectUtils;

@Component
public class CloudinaryConfig {

	private Cloudinary cloudinary;

	CloudinaryConfig() {
		cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name", "dsywyhhdl", "api_key", "128818145556257",
				"api_secret", "AXceLRI5c5B0QilhHoQzhAjSQ30"));
//		try {
//			usageLimits();
//			deleteImage();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	public Cloudinary getCloudinary() {
		return cloudinary;
	}

	public void setCloudinary(Cloudinary cloudinary) {
		this.cloudinary = cloudinary;
	}

	public String addImage(MultipartFile image, String extension, boolean isSquare) throws IOException {
		if (image != null && !image.isEmpty()) {
			File imageFile;
			Map mapping;
			Map uploadResult;
			imageFile = Files.createTempFile("temp", image.getOriginalFilename()).toFile();
			if (extension == null) {
				mapping = ObjectUtils.emptyMap();
			} else {
				Map<String, String> options = new HashMap<>();
				options.put("format", "png");
				options.put("height", "150");
				options.put("width", "150");
				mapping = options;

			}
			image.transferTo(imageFile);
			uploadResult = cloudinary.uploader().upload(imageFile, mapping);
//			return url;
			if (uploadResult != null) {
				String url = uploadResult.get("url").toString();
				if (isSquare) {
					url = url.replace("/upload/", "/upload/w_150,h_150/");
				}
				return url;
			}
		}

		return null;
	}

	public void usageLimits() throws Exception {
		ApiResponse result = this.cloudinary.api().resources(ObjectUtils.emptyMap());
		System.out.println(result.apiRateLimit().getLimit());
		System.out.println(result.apiRateLimit().getRemaining());
		System.out.println(result.apiRateLimit().getReset());
	}

	@Async
	public void deleteImage(String image) throws Exception {
		if (image != null) {
			String[] splitBySlash = image.split("/");
			if (splitBySlash.length > 0) {
				String splitByPeriod = splitBySlash[splitBySlash.length - 1];
				String[] publicId = splitByPeriod.split("\\.");
				if (publicId.length > 0) {
					ApiResponse result = this.cloudinary.api().deleteResources(Arrays.asList(publicId[0]),
							ObjectUtils.emptyMap());
					System.out.println("delete image" + result.toString());
				}
			}
		}
	}

}
