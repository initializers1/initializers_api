package com.initializers.services.apiservices.model.actionparam;

import com.initializers.services.apiservices.model.SupportedPlatform;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenerateApp {
	String appName;
	String appPackage;
	String appIcon;
	String appCategory;
	SupportedPlatform supportedPlatform;
	String selectedTheme;
}
