package com.initializers.services.apiservices.model.input;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateOrderInput {
	private ObjectId userId;
	private ObjectId addressId;
	private String coupenCode;
	private char paymentMode;
	private ObjectId orderId;
}
