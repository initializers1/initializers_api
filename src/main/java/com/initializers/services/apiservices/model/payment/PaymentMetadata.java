package com.initializers.services.apiservices.model.payment;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentMetadata {
	
	String gatewayName;
	String secretKey;
	String publicKey;
	@JsonSerialize(using = ObjectIdSerializer.class)
	ObjectId orderId;
	String externalOrderId; //clientsecret --> stripe
	String transactionId; //intent id --> stripe

}
