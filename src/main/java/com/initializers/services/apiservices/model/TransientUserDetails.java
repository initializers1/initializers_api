package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.initializers.services.apiservices.constant.Scope;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "TransientUserDetails")
@Getter
@Setter
public class TransientUserDetails {
	
	@Id
	private ObjectId id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String appId;
	private String appName;
	private Scope scope;
	
}