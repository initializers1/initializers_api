package com.initializers.services.apiservices.model.temp;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.initializers.services.apiservices.model.item.ItemAvailability;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemListTemp {
	@Id
	private ObjectId id;
	private String name;
	private String imageLinks;
	private List<ItemAvailability> availablity;
}
