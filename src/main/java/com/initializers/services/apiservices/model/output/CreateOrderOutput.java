package com.initializers.services.apiservices.model.output;

import java.time.LocalDate;

import org.bson.types.ObjectId;

import com.initializers.services.apiservices.model.OrderStatus;
import com.initializers.services.apiservices.model.payment.PaymentMetadata;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateOrderOutput {
	private ObjectId orderId;
	private LocalDate deliveredBy;
	private Float totalAmount;
	private char paymentMode;
	private OrderStatus status;
	private PaymentMetadata paymentMetadata;
}
