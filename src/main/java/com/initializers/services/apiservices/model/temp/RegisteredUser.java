package com.initializers.services.apiservices.model.temp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisteredUser {
	private String email;
	private String password;
	private String appId;
	private String firstName;
	private String lastName;
	private String personalEmail;
	private String companyEmail;
}
