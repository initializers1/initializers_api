package com.initializers.services.apiservices.model.input;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressInput {
	
	private ObjectId id;
	private ObjectId userId;
	private String name;
	private String phoneNumber;
	private String firstLine;
	private String secondLine;
	private String landMark;
	private String pinCode;
}
