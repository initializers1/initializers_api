package com.initializers.services.apiservices.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.initializers.services.apiservices.constant.Scope;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
	

@Document(collection = "AdminClientDetails")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdminClientDetails implements UserDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4838842773631778030L;
	@Id
    private CompositeKeyAdminClient id;
//	private ObjectId adminId;
	private String password;
//	private ObjectId clientId;
	private ObjectId userId;
	private Scope scope;
	private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private boolean accountExpired;
    private boolean accountLocked;
    private boolean credentialsExpired;
    private boolean enabled;
    private Set<Role> authorities = new HashSet<>();
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class CompositeKeyAdminClient implements Serializable {
		
		private static final long serialVersionUID = -1935529049201208834L;
		private String email;
		private String appId;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getUsername() {
		return getId().getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return !isAccountExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return !isAccountLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return !isCredentialsExpired();
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
}
