package com.initializers.services.apiservices.model.payment;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Getter;
import lombok.Setter;

@Document("PaymentGatewayStatus")
@Getter
@Setter
public class PaymentGatewayStatus {
	
	@Id
	private ObjectId id;
	
	private ObjectId gatewayID;
	private String gatewayStatus;
	private char status;
	
}
