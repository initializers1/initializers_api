package com.initializers.services.apiservices.model.elasticsearch;

import java.util.List;

import org.bson.types.ObjectId;
import org.checkerframework.checker.signature.qual.FullyQualifiedName;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;

import com.initializers.services.apiservices.model.item.ItemDescription;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(indexName = "item-details")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Mapping(mappingPath = "ElasticSearch/mapping/item-details.json")
@Setting(settingPath = "ElasticSearch/settings/item-details.json")
public class ItemDetailsES {
	@Transient 
	public static final String indexName = "item-details";
	
	
	@Id
	private ObjectId id;
	private ObjectId categoryId;
	private ObjectId subCategoryId;
	@Field(type = FieldType.Text, analyzer = "pattern")
	private String name;
	private ItemDescription description;
	private List<String> imageLinks;
	private String status;
	private String appId;
}
