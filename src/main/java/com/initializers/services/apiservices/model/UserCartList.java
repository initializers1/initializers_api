package com.initializers.services.apiservices.model;

import java.util.ArrayList;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCartList {
	private String id;
	private ObjectId userId;
	private ObjectId itemId;
	private ObjectId availabilityId;
	private Float quantity;
	private Float discountPrice;
	private Float value; 
	private String unit;
	private String itemName;
	private ArrayList<Object> imageLinks;
	private String imageLink;
}
