package com.initializers.services.apiservices.model;

import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "Theme")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Theme {
	@Id
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	private String name;
	// TODO: add structure to include description and features of theme
	private Set<String> images;
}
