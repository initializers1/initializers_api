package com.initializers.services.apiservices.model.payment;


import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;


@Document("PaymentGateway")
@Getter
@Setter
public class PaymentGateway {
	
	@Id
	private ObjectId id;
	
	private String name;
	private String serviceURL; 
	private String className;
			
}
