package com.initializers.services.apiservices.model;

import java.util.HashSet;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("user_wishlist")
public class UserWishlist {
	
	@Id
	private ObjectId userID;
	private Set<ObjectId> itemsId;
	public ObjectId getUserID() {
		return userID;
	}
	public void setUserID(ObjectId userID) {
		this.userID = userID;
	}
	public Set<ObjectId> getItemsId() {
		return itemsId;
	}
	public void setItemsId(ObjectId itemsId) {
		if(this.itemsId == null) {
			this.itemsId = new HashSet<ObjectId>();
		}
		this.itemsId.add(itemsId);
	}

}
