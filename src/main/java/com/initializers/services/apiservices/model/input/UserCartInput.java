package com.initializers.services.apiservices.model.input;

import com.initializers.services.apiservices.model.UserCart;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCartInput {
	
	private UserCart.CompositeKeyCart id;
	
	private Float quantity;
}
