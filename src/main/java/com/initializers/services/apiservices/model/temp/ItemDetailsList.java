package com.initializers.services.apiservices.model.temp;

import java.util.List;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.model.item.ItemDescription;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemDetailsList {
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
//	@JsonSerialize(using = ObjectIdSerializer.class)
	private String categoryId;
//	@JsonSerialize(using = ObjectIdSerializer.class)
	private String subCategoryId;
	private String name;
	private ItemDescription description;
	private List<String> imageLinks;
	private String status;
	private String categoryName;
	private String subCategoryName;
}
