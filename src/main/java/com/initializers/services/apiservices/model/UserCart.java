package com.initializers.services.apiservices.model;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "userCart")
@Getter
@Setter
public class UserCart {

	@Id
    private CompositeKeyCart id;
	
	private Float quantity;
	
	@Getter
	@Setter
    public static class CompositeKeyCart implements Serializable {
		private ObjectId userId;
    	private ObjectId itemId;
    	private ObjectId availabilityId;
    }
}
