package com.initializers.services.apiservices.model.payment;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminClientPaymentIntent {
	
	@Id
	ObjectId id;
	
	String paymentIntentId;
	String appId;

}
