package com.initializers.services.apiservices.model.temp;

import java.time.LocalDate;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.initializers.services.apiservices.model.OrderStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserOrderSetList {
	@Id
	private ObjectId id;
	private String name;
	private String firstLine;
	private String secondLine;
	private String pinCode;
	private String phoneNumber;
	private OrderStatus status;
	private String statusString;
	private String state;
	private LocalDate orderAt;
	private LocalDate deliveredBy;
	private Float totalAmount;
}
