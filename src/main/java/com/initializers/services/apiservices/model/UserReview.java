package com.initializers.services.apiservices.model;

import java.io.Serializable;
import java.time.LocalDate;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document("userReview")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserReview {

	@Id
    private CompositeKey id;
	
	private int rating;
	private String review;
	private LocalDate changedAt;
	@Transient
	private Float averageRating;
	@Transient 
	private String userName;
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
    public static class CompositeKey implements Serializable {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 4834201631748703823L;
		private ObjectId userId;
    	private ObjectId itemId;
    }		
}
