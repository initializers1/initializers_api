package com.initializers.services.apiservices.model.elasticsearch;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(indexName = "item-category")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Mapping(mappingPath = "ElasticSearch/mapping/item-category.json")
@Setting(settingPath = "ElasticSearch/settings/item-category.json")
public class ItemCategoryES {

	@Transient 
	public static final String indexName = "item-category";
	
	@Id
	private ObjectId id;
	private String name;
	private String description;
	private Long offer;
	private String imageLink;
	private String appId;
}
