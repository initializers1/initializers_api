package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

/**
 * @author karthik
 *
 */
@Document("topDeals")
@Getter
@Setter
public class TopDeals {
	@Id
	private ObjectId id;
	private String type;
	private Long typeID;
	private String resultType;
	private String image;	
}
