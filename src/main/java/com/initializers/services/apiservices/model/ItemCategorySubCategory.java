package com.initializers.services.apiservices.model;

import java.util.List;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemCategorySubCategory {
	
	private ObjectId id;
	private String name;
	private String description;
	private Long offer;
	private String imageLink;
	private List<ObjectId> subCategoryID;
}
