package com.initializers.services.apiservices.model.temp;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemSubCategoryList {

	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	private String categoryId;
	private String name;
	private String description;
	private Long offer;
	private String imageLink;
}
