package com.initializers.services.apiservices.model;

import java.time.LocalDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "AdminAppDetails")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdminAppDetails {
	@Id
	String id;
	String appName;
	String appPackage;
	String appIcon;
	String category;
	SupportedPlatform supportedPlatform;
	ObjectId theme;
	int pipelineId;
	String pipelineStatus;
	String expoBuildUrl;
	LocalDateTime triggeredTime;
	LocalDateTime refreshTime;
}
