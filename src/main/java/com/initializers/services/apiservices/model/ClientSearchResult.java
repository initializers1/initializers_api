package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientSearchResult {
	private String id;
	private ObjectId typeId;
	private String name;
	private String description;
	private String type;
	private String image;
}
