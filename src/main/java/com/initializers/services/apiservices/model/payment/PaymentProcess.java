package com.initializers.services.apiservices.model.payment;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Transient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentProcess {
	
	private ObjectId gatewayID;
	private String publicKey;
	private String secretKey;
	private String status;
	private String name;
	private char type;
	private String serviceURL;
	private String className;
	
}
