package com.initializers.services.apiservices.model.temp;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserReviewTemp {
	
	private ObjectId userId;
	private ObjectId itemId;
	private int rating;
	private String review;
}
