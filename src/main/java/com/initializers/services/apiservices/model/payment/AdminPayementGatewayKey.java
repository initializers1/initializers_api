package com.initializers.services.apiservices.model.payment;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.constant.Status;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Document("AdminPayementGatewayKey")
@Getter
@Setter
public class AdminPayementGatewayKey {

	@Id
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId gatewayID;
	private String publicKey;
	private String secretKey;
	private String status;
	private char type;
	private Date createdAt;
	private Date updatedAt;
	
}
