package com.initializers.services.apiservices.model.item;


import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "itemCategory")
@Getter
@Setter
public class ItemCategory {

	@Id
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	private String name;
	private String description;
	private Long offer;
	private String imageLink;
}
