package com.initializers.services.apiservices.model;

import org.springframework.data.annotation.Transient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderStatus {

	public boolean confirmed = false;
	public boolean delivered = false;
	@Transient
	public char payment;
}
