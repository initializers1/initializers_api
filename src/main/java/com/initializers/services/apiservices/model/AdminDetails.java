package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.initializers.services.apiservices.constant.Status;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "AdminDetails")
@Getter
@Setter
@NoArgsConstructor
public class AdminDetails {
	
	@Id
	private ObjectId id;
	private String firstName;
	private String lastName;
	private String status;
		
	public AdminDetails(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.status = Status.Active;
	}
}
