package com.initializers.services.apiservices.model.elasticsearch;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(indexName = "item-subcategory")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Mapping(mappingPath = "ElasticSearch/mapping/item-subcategory.json")
@Setting(settingPath = "ElasticSearch/settings/item-subcategory.json")
public class ItemSubCategoryES {
	@Transient 
	public static final String indexName = "item-subcategory";
	
	@Id
	private ObjectId id;
	private ObjectId categoryId;
	private String name;
	private String description;
	private Long offer;
	private String imageLink;
	private String appId;
}
