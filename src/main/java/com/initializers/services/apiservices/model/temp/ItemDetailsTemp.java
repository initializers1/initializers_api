package com.initializers.services.apiservices.model.temp;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.initializers.services.apiservices.model.item.ItemAvailability;
import com.initializers.services.apiservices.model.item.ItemDescription;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemDetailsTemp {

	@Id
	private ObjectId id;
	private ObjectId categoryId;
	private ObjectId subCategoryId;
	private String name;
	private List<ItemAvailability> itemAvailabilities;
	private ItemDescription description;
	private List<String> imageLinks;
}
