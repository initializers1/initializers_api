package com.initializers.services.apiservices.model.input;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserWishListInput {
	private ObjectId userId;
	private ObjectId itemId;
	private String type;

}
