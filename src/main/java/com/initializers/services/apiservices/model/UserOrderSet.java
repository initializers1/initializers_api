package com.initializers.services.apiservices.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.model.item.ItemAvailability;
import com.initializers.services.apiservices.model.item.ItemDetails;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "userOrder")
@Getter
@Setter
public class UserOrderSet { 

	@Id
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	private List<UserOrder> orderList;
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId userId;
	@Transient
	private String orderDetails;
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId addressId;
//	@Transient
	private Address addressDetails;
	private OrderStatus status;
	private LocalDateTime orderAt;
	private LocalDate deliveredBy;
	@Transient
	private Float netAmount;
	private Float totalAmount;
	private Float deliveryCharge;
	private String coupenCode;
	private Float coupenDiscount;
	
//	Transient but used in aggregation
	private char paymentStatus;
	private char paymentMode;
	private ArrayList<String> itemNames;
	private List<ItemDetails> itemDetails;
	private List<ItemAvailability> itemAvailability;
}
