package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;

/**
 * @author karthik
 *
 */
@Getter
@Setter
public class ItemDetailsSearch {

	@Id
	private ObjectId id;
	private String name;	
}
