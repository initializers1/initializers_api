package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document("userAddress")
@Getter
@Setter
public class Address {
	
	@Id
	private ObjectId id;

	private ObjectId userId;
	private String name;
	private String phoneNumber;
	private String firstLine;
	private String secondLine;
	private String landMark;
	private String pinCode;
	private String status;
}
