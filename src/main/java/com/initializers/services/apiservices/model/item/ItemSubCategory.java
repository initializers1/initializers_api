package com.initializers.services.apiservices.model.item;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "itemSubCategory")
@Getter
@Setter
public class ItemSubCategory {

	@Id
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId categoryId;
	private String name;
	private String description;
	private Long offer;
	private String imageLink;
}
