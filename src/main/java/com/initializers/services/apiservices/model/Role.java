package com.initializers.services.apiservices.model;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role implements GrantedAuthority {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4325844048545849968L;
	public static final String ADMIN = "ADMIN";
	public static final String CLIENT = "CLIENT";
	public static final String CREATE_ITEM = "CREATE_ITEM";
	public static final String READ_ITEM = "READ_ITEM";
	public static final String MODIFY_ITEM = "MODIFY_ITEM";
	public static final String DELETE_ITEM = "DELETE_ITEM";
	public static final String CONFIG_ORDER = "CONFIG_ORDER";
	public static final String CONFIG_HOMEPAGE = "CONFIG_HOMEPAGE";
	public static final String CONFIG_PAYMENT = "CONFIG_PAYMENT";
	public static final String READ_USERORDER = "READ_USERORDER";
	public static final String[] ADMIN_ROLES = { CREATE_ITEM, READ_ITEM, MODIFY_ITEM, DELETE_ITEM, CONFIG_ORDER,
			CONFIG_HOMEPAGE, CONFIG_PAYMENT, READ_USERORDER, CLIENT };

	private String authority;
}
