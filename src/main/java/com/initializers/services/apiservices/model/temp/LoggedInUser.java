package com.initializers.services.apiservices.model.temp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoggedInUser {
	private String username;
	private String password;
	private String appId;

}
