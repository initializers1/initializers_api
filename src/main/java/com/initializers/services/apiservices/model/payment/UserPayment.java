package com.initializers.services.apiservices.model.payment;

import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.initializers.services.apiservices.constant.Status;

import lombok.Getter;
import lombok.Setter;

@Document("UserPayment")
@Getter
@Setter
public class UserPayment {
	
	@Id
	private ObjectId id;
	
	private Float amount;
	private char status;
	private char type;
	private String gatewayName;
	private String externalOrderId;
	private String transactionId;
	private String failureCode;
	private String failureMessage;
	private Date changedOn;
}
