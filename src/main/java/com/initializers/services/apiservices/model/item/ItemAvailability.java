package com.initializers.services.apiservices.model.item;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "itemAvailability")
@Getter
@Setter
public class ItemAvailability {
	
	@Id
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId itemId;
	private Float actualPrice;
	private Long discount;
	private Float discountPrice;
	private Float value;
	private String unit;
	private String available;
	private String state;	
}
