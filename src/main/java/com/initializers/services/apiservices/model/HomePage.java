package com.initializers.services.apiservices.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "homepage")
@Getter
@Setter
public class HomePage {
	@Id
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	private String itemType; // ItemCategory or ItemSubCategory
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId typeId;
	private String widget;
	private String name;
	@Transient
	private List<HomePageContent> content;

}
