package com.initializers.services.apiservices.model;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomePageContent {
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	private String name;
	private String description;
	private Long offer;
	private String imageLink;
}
