package com.initializers.services.apiservices.model.input;

import org.bson.types.ObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDetailsInput {
	
	private ObjectId id;
	private String firstName;
	private String lastName;
}
