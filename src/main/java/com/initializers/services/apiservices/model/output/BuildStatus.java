package com.initializers.services.apiservices.model.output;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuildStatus {
	String status;
	LocalDateTime generatedAt;
	String expoBuildUrl;
}
