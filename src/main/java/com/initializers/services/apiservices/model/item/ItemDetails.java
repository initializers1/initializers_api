package com.initializers.services.apiservices.model.item;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.initializers.services.apiservices.others.ObjectIdSerializer;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "itemDetails")
@Getter
@Setter
public class ItemDetails {

	@Id
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId id;
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId categoryId;
	@JsonSerialize(using = ObjectIdSerializer.class)
	private ObjectId subCategoryId;
	private String name;
	private ItemDescription description;
	private List<String> imageLinks;
	private String status;
//	@Transient
	private List<ItemAvailability> itemAvailability;
	@Transient
	private String imageLink;
	@Transient
	private Boolean isWishlist;
	private Float averageRating;
	
}
