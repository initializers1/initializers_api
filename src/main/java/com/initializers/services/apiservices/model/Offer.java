package com.initializers.services.apiservices.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "offers")
@Getter
@Setter
public class Offer {
	
	@Id
	private ObjectId id;
	private Long itemId;
	private Long discount;
	private Long discountPrice;
	private Date timeStamp;	
}
