package com.initializers.services.apiservices.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter

@Document(collection = "AdminClientExternalIdRef")
public class AdminClientExternalIdRef {
	@Id
	private CompositeKeyExtrnalId id;
	@Getter
	@Setter
    public static class CompositeKeyExtrnalId implements Serializable {
		private static final long serialVersionUID = 6954487836539955268L;
		private String externalId;
		private String appId;
    }
}
