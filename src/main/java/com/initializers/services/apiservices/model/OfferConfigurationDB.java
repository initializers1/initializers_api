package com.initializers.services.apiservices.model;

import java.util.ArrayList;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "configurationDB")
@Getter
@Setter
public class OfferConfigurationDB {

	private ObjectId id;
	private ArrayList<String> fieldIn;
	private String fieldOut;
	private String symbol;
	private String type;
	private String formula;
	private String flag;
	private Boolean round;
}
