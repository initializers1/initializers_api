package com.initializers.services.apiservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLScalarType;

@SpringBootApplication
@ComponentScan({"com.initializers.services.apiservices"})
@EnableAsync
public class ApiservicesApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ApiservicesApplication.class, args);
	}
	
//	@Bean(name = "restcorsConfigurer")
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/admin/**").allowedOrigins("*").allowedMethods("GET", "POST", "PUT", "DELETE");
				registry.addMapping("/graphql").allowedOrigins("*").allowedMethods("POST");
				registry.addMapping("/user/**").allowedOrigins("*").allowedMethods("GET","POST");
			}
		};
	}
	@Bean
	public GraphQLScalarType dateType() {
		return ExtendedScalars.Date;
	} 
    @Bean
    GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults(""); // Remove the ROLE_ prefix
    }
//	@Bean(name = "tenantContext")
//	public String getTenantId() {
//		return TenantContext.getTenantId();
//	}
}
