package com.initializers.services.apiservices.convertor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.initializers.services.apiservices.model.actionparam.GenerateApp;

public class GenerateAppConvertor implements Converter<String, GenerateApp>{

	@Autowired
    private ObjectMapper objectMapper;
	
	@Override
	public GenerateApp convert(String source) {
		GenerateApp generateApp = new GenerateApp();
		try {
			generateApp = objectMapper.readValue(source, GenerateApp.class);
		} catch(JsonProcessingException e) {
			//log error
		}
		return generateApp;
	}

}
