package com.initializers.services.apiservices.others;

public class AppIdGenerator {

	public static String getBias(String userName) {
		int sum = userName.chars().sum();
		int radix = 16;
		String bias = "";
		while (sum > 0) {
			int mod = sum % radix;
			sum = sum / radix;
			bias += Character.forDigit(mod, radix);
		}
		return bias;
	}
}
