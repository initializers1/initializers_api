package com.initializers.services.apiservices.others;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProperties {
	
	Logger logger = LoggerFactory.getLogger(ApplicationProperties.class);

	public static String cloudname;
	public static String api_key;
	public static String api_secret;
	public static String databaseName;
	public static String mongoDbURI;
	public static String[] orderConfigType = { "DLRCRG", "DLRBY", "COUPEN", "AUTOCNFRM" };
	public static String tenant_Identifier = ", 'admin_Id' : 1";
	public static String app_name;
	public static String app_url;
	public static int otp_expireTime;
	public static String secret;
	public static int max_count_elasticSearch;
	public static int tryout_timer_elasticSearch;
	public static boolean enable_elasticSearch;

	public static Map<String, String> orderFieldMap = new HashMap<String, String>() {

		private static final long serialVersionUID = -9026411095742084952L;

		{
			put("ItemDetails", "UserOrderSet.orderList.itemId");
			put("UserDetails", "UserOrderSet.orderList.userId");
			put("Address", "UserOrderSet.addressId");
		}
	};

	public ApplicationProperties(@Value("${cloudinary.cloud_name}") String cloudname,
			@Value("${cloudinary.api_key}") String api_key, @Value("${cloudinary.api_secret}") String api_secret,
			@Value("${app.env}") String env, @Value("${spring.data.mongodb.database}") String databaseName,
			@Value("${spring.data.mongodb.uri}") String mongoDbURI, @Value("${initializers.app.name}") String app_name,
			@Value("${initializers.app.icon.url}") String app_url,
			@Value("${initializers.app.otp.expiretime}") int otp_expireTime,
			@Value("${intializers.secret.password}") String secret,
			@Value("${initializers.elasticsearch.enable}") Boolean enable_elasticSearch,
			@Value("${initializers.circuitbreaker.elasticsearch.tryouttimer}") int tryout_timer,
			@Value("${initializers.circuitbreaker.elaticsearch.maxcount}") int max_count) {
		ApplicationProperties.cloudname = cloudname;
		ApplicationProperties.api_key = api_key;
		ApplicationProperties.api_secret = api_secret;
		ApplicationProperties.databaseName = databaseName;
		ApplicationProperties.mongoDbURI = mongoDbURI;
		ApplicationProperties.app_name = app_name;
		ApplicationProperties.app_url = app_url;
		ApplicationProperties.otp_expireTime = otp_expireTime;
		ApplicationProperties.secret = secret;
		ApplicationProperties.enable_elasticSearch = enable_elasticSearch;
		ApplicationProperties.tryout_timer_elasticSearch = tryout_timer;
		ApplicationProperties.max_count_elasticSearch = max_count;
	}
}