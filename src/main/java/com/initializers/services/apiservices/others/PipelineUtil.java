package com.initializers.services.apiservices.others;

public class PipelineUtil {

	public static String extractExpoURL(String jobLogs, String expoBaseURL, String expoBaseURLMobile) {
		String expoAppURL = "";
		String[] splitJobLogs = jobLogs.split("\\s");
		for (int i = 0; i < splitJobLogs.length; i++) {
			if (splitJobLogs[i].contains(expoBaseURL)) {
				expoAppURL = splitJobLogs[i];
				expoAppURL = expoAppURL.replaceFirst(expoBaseURL, expoBaseURLMobile);
				break;
			}
		}
		return expoAppURL;
	}
}
