package com.initializers.services.apiservices.others;

import java.io.IOException;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@Component
public class ObjectIdSerializer extends JsonSerializer<ObjectId> {

	@Override
	public void serialize(ObjectId value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		// TODO Auto-generated method stub
		if(value == null) {
			gen.writeNull();
		}else {
			gen.writeString(value.toHexString());
		}
	}
	
}
