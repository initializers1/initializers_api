package com.initializers.services.apiservices.others;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryString {
	String[] filter;
	String search;
}
